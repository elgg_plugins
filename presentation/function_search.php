<?php
/**
 * Elgg Presentation tool
 * 
 * @author Sven Edge
 * @package mod_presentation
 */

global $CFG, $USER, $db, $PAGE;
global $search_exclusions;

$weblogPosted = __gettext("Presentations by"); // gettext variable
$inCategory = __gettext("in category"); // gettext variable
$rssForBlog = __gettext("RSS feed for presentations by"); // gettext variable

$handle = 1;

// wtf? - Sven
if (!empty($PAGE->returned_items) && $PAGE->returned_items == "accounts") {
    $handle = 0;
}

if ((isset($parameter) && $parameter[0] == "presentation") && $handle) {
    
    $owner = optional_param('owner',0,PARAM_INT);
    
    if ($owner) {
        // search for matching tagged presentations belonging to userid $owner
        $searchline = "tagtype = 'presentation' AND owner = $owner AND tag = " . $db->qstr($parameter[1]) . "";
        $searchline = "(" . run("users:access_level_sql_where",$_SESSION['userid']) . ") AND " . $searchline;
        $searchline = str_replace("owner","tags.owner",$searchline);
        if ($refs = get_records_select('tags',$searchline)) {
            $searchline = "";
            foreach($refs as $ref) {
                if ($searchline != "") {
                    $searchline .= ",";
                }
                $searchline .= $ref->ref;
            }
            $searchline = " p.ident in (" . $searchline . ")";
            if (!empty($PAGE->search_type_unformatted)) {
                $searchline .= " AND u.user_type = " . $PAGE->search_type;
            }
            
            // get presentations for id list
            if ($posts = get_records_sql('SELECT p.*,u.username,u.ident as userid
                                     FROM '.$CFG->prefix.'presentations p JOIN '.$CFG->prefix.'users u ON u.ident = p.owner
                                     WHERE ('.$searchline.') ORDER BY p.name ASC')) {
                
                // get username/nicename for h2 header
                $name = '';
                $username = '';
                if (count($posts)) {
                    $keys = array_keys($posts);
                    $p = $posts[$keys[0]];
                    if (!empty($p)) {
                        $name = run("profile:display:name",$p->userid);
                        $username = $p->username;
                    }
                }
                $run_result .= "<h2>$weblogPosted " . $name . " $inCategory '".htmlspecialchars($parameter[1])."'</h2>\n<ul>";
                foreach($posts as $post) {
                    $run_result .= "<li>";
                    $run_result .= "<a href=\"".url . $username . "/presentations/" . $post->ident . "\">" . htmlspecialchars($post->name) . "</a>\n";
                    $run_result .= "</li>";
                }
                $run_result .= "</ul>";
            }
        }
    }
    
    // search for users other than $owner with matching tagged presentations
    
    $searchline = "tagtype = 'presentation' AND tag = " . $db->qstr($parameter[1]) . "";
    $searchline = "(" . run("users:access_level_sql_where",$_SESSION['userid']) . ") AND " . $searchline;
    $searchline = str_replace("owner","t.owner",$searchline);
    if (!empty($PAGE->search_type_unformatted)) {
        $searchline .= " AND u.user_type = " . $PAGE->search_type;
    }
    $sql = "SELECT DISTINCT u.* FROM ".$CFG->prefix.'tags t JOIN '.$CFG->prefix.'users u ON u.ident = t.owner WHERE ('.$searchline.')';
    if ($owner) {
        $sql .= " AND u.ident != " . $owner;
        $otherUsers = __gettext("Other users with presentations in category"); // gettext variable
    } else {
        $otherUsers = __gettext("Users with presentations in category"); // gettext variable
    }
    if ($users = get_records_sql($sql)) {
        
        $run_result .= "<h2>$otherUsers '".htmlspecialchars($parameter[1])."'</h2>\n";
        $body = "<table><tr>";
        $i = 1;
        $w = 100;
        if (sizeof($users) > 4) {
            $w = 50;
        }
        foreach($users as $key => $info) {
            $friends_userid = $info->ident;
            $friends_name = run("profile:display:name",$info->ident);
            $info->icon = run("icons:get",$info->ident);
            $friends_menu = run("users:infobox:menu",array($info->ident));
            $link_keyword = urlencode($parameter[1]);
            $body .= <<< END
        <td align="center">
            <p>
            <a href="{$CFG->wwwroot}search/index.php?presentation={$link_keyword}&amp;owner={$friends_userid}">
            <img src="{$CFG->wwwroot}_icon/user/{$info->icon}/w/{$w}" alt="{$friends_name}" border="0" /></a><br />
            <span class="userdetails">
                {$friends_name}
                {$friends_menu}
            </span>
            </p>
        </td>
END;
            if ($i % 5 == 0) {
                $body .= "\n</tr><tr>\n";
            }
            $i++;
        }
        $body .= "</tr></table>";
        $run_result .= $body;
    }
    
}

?>