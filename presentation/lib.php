<?php
/**
 * Elgg Presentation tool
 * 
 * @author Sven Edge
 * @package mod_presentation
 */

/*
	TODO?:
		export presentation to zip?
		The ability to search for blog posts and files via tag
		The ability to choose files uploaded to a community
		Auto table of contents

*/




/**
 * Standard page setup function
 * 
 * @param string $varname 
 * @param int $options 
 * @return mixed
 */
function presentation_pagesetup() {

	global $PAGE, $CFG, $USER, $page_owner, $function, $metatags, $template;
	
	require_once($CFG->dirroot.'lib/filelib.php'); // to ensure file_get_contents()
	$css = file_get_contents($CFG->dirroot . "mod/presentation/presentation.css");
	$css = str_replace("{{url}}", $CFG->wwwroot, $css);
	$metatags .= '<style type="text/css">' . $css . '</style>';
	$metatags .= '<script type="text/javascript" src="' . $CFG->wwwroot . 'mod/presentation/presentation.js"></script>' . "\n";
	
	$page_username = user_info("username",$page_owner);
	
	if (isloggedin() && user_info("user_type", $USER->ident) != "external") {
		if (defined("context") && context == "presentation" && $page_owner == $USER->ident) {
			$PAGE->menu[] = array(
				'name' => 'presentation',
				'html' => '<li><a href="' . $CFG->wwwroot . $USER->username . '/presentations/" class="selected">' . __gettext("Your Presentations") . '</a></li>'
			);
		} else {
			$PAGE->menu[] = array(
				'name' => 'presentation',
				'html' => '<li><a href="' . $CFG->wwwroot . $USER->username . '/presentations/">' . __gettext("Your Presentations") . '</a></li>'
			);
		}
	}
	
	if (defined("context") && context == "presentation") {
			
			$PAGE->menu_sub[] = array(
				'name' => 'presentation:index',
				'html' => a_href(
					$CFG->wwwroot . $page_username . '/presentations/',
					__gettext("Index")
				)
			);
			
			if (isloggedin() && run("permissions:check", "profile") && $page_owner == $USER->ident) {
				$PAGE->menu_sub[] = array(
					'name' => 'presentation:create',
					'html' => a_href(
						$CFG->wwwroot . 'mod/presentation/new.php?owner=' . $USER->ident, 
						__gettext("New")
					)
				);
			}
			
			
	}
	
	$function['display:sidebar'][] = $CFG->dirroot . "mod/presentation/user_info_menu.php";
	
	$postedby = '';
	$keywords = __gettext("Keywords: ");
	$template['presentation_weblogpost'] = <<< END
		<div class="weblog-post"><!-- Holds all aspects of a blog post -->
			<div class="user"><!-- Open class user -->
				<img alt="" src="{{url}}_icon/user/{{usericon}}"/><br />{{fullname}}
			</div><!-- Close class user -->
			<div class="weblog-title"><h3>{{title}}</h3></div>
			<div class="post"><!-- Open class post -->
				{{body}}
			</div><!-- Close class post -->
			<div class="info"><!-- Open class info -->
				<p>
					$postedby {{fullname}}
					{{links}}
					{{commentslink}}
				</p>
			</div><!-- Close class info -->
			<p>
			$keywords {{tags}}
			</p>
			{{comments}}
		</div><!-- Close weblog_post -->
		<div class="clearing"></div>
END;
	
	$comments = __gettext("Comments (+/-)");
	$template['presentation_weblogcomments'] = <<< END
		<div class="presentation-importedcomments"><!-- start comments div -->
			<h4><a href="javascript:toggleVisibility('{{blockid}}')">{{numcomments}} $comments</a></h4>
			<ol id="{{blockid}}">
				{{comments}}
			</ol>
			<script type="text/javascript">
			<!--
				toggleVisibility('{{blockid}}');
			// -->
			</script>
		</div><!-- end comments div -->
END;
	
	$template['presentation_weblogcomment'] = <<< END
		<li>
			{{body}}
			<div class="comment-owner">
			<p>
				{{usericon}}{{postedname}} on {{posted}}
			</p>
			</div>
		</li>
END;

	$template['presentation_comment'] = <<< END
		<li>
			{{body}}
			<div class="comment-owner">
			<p>
				{{usericon}}{{postedname}} on {{posted}} | {{links}}
			</p>
			</div>
		</li>
END;
	
	$comments = __gettext("Comments");
	//template for overall presentation comments
	$template['presentation_comments'] = <<< END
		<div class="presentation-comments"><!-- start comments div -->
			<ol id="{{blockid}}">
				{{comments}}
			</ol>
		</div><!-- end comments div -->
END;
	//template for per-section comments, with integrated comment adding form
	$template['presentation_sectioncomments'] = <<< END
		<div class="presentation-sectioncomments" id="{{blockid}}">
			<ol>
				{{comments}}
			</ol>
			{{addform}}
		</div>
		<script type="text/javascript">
		<!--
			toggleVisibility('{{blockid}}');
		// -->
		</script>
		<!-- -->
END;




	
} // end function




/**
 * Initialisation function
 *
 */
function presentation_init() {
	
	global $CFG, $function, $db, $METATABLES;
	
	$CFG->presentation->types = array('text', 'blogpost', 'file');
	
	if (!array_intersect(array(
		$CFG->prefix . "presentations", 
		$CFG->prefix . "presentation_sections", 
		$CFG->prefix . "presentation_sections_data",
		$CFG->prefix . "presentation_comments"
	), $METATABLES)) {
		if (file_exists($CFG->dirroot . "mod/presentation/$CFG->dbtype.sql")) {
			modify_database($CFG->dirroot . "mod/presentation/$CFG->dbtype.sql");
		} else {
			error("Error: Your database ($CFG->dbtype) is not yet fully supported by the Elgg Presentation tool. See the mod/presentation directory.");
		}
		print_continue("index.php");
		exit;
	}
	
	$function['search:all:tagtypes'][] = $CFG->dirroot . "mod/presentation/function_search_all_tagtypes.php";
	$function['search:display_results'][] = $CFG->dirroot . "mod/presentation/function_search.php";
	$function['groups:delete'][] = $CFG->dirroot . "mod/presentation/groups_delete.php";
	
	$action = trim(optional_param('action'));
	if ($action) {
		presentation_actions($action);
	}
	
	listen_for_event("user", "delete", "presentation_user_delete"); 
	
} // end function




/**
 * Get presentations of a particular user visible to the logged on user
 * 
 * @param int $user_id userid to get presentations for
 * @return array
 */
function presentation_get_presentations_visible($owner_id) {
	
	global $CFG, $USER;
	
	// Get access list for this user
	$where = run("users:access_level_sql_where",$USER->ident);
	
	// Get a list of presentations
	$sql = "SELECT * FROM ".$CFG->prefix."presentations WHERE ($where) AND owner = $owner_id ORDER BY name";
	$presentations = get_records_sql($sql);
	
	// Return them
	return $presentations;
	
} // end function




/**
 * Get presentations sidebar content
 * 
 * @return string html to add on to sidebar
 */
function presentation_sidebar() {

	global $page_owner, $CFG;
	$run_result = '';
	
	if ($page_owner > 0) { // TODO: a better way to do this - Sven
		$title = __gettext("Presentations");
		$body = "<ul><li>"; 
		$ownername = user_info("username",$page_owner);
		$body .= '<a href="' . $CFG->wwwroot . $ownername . '/presentations/">' . __gettext("Presentations List") . '</a></li></ul>';
		
		$run_result .= "<li id=\"sidebar_presentations\">";
		$run_result .= templates_draw(array(
											'context' => 'sidebarholder',
											'title' => $title,
											'body' => $body,
											)
									  );
		$run_result .= "</li>";
	}
	return $run_result;
} // end function




/**
 * Do updatey things for presentations
 * 
 * @param string $action the action to trigger
 * 
 */
function presentation_actions($action) {
	
	global $CFG, $USER, $page_owner, $messages;
	
	// logged on-only actions
	if (logged_on) {
		switch ($action) {
			
			case "presentation:create":
				// this always creates as the logged in user, not the page owner
				$new_presentation = new StdClass;
				$new_presentation->name = trim(optional_param('new_presentation_title'));
				$new_presentation->access = trim(optional_param('new_presentation_access'));
				$new_presentation->owner = $USER->ident;
				$new_presentation_keywords = trim(optional_param('new_presentation_keywords'));
					
				// If we've been asked to add an item, do that
				if (!empty($new_presentation->name)) {
					
					$new_presentation = plugin_hook("presentation","create",$new_presentation);
					if (!empty($new_presentation)) {
						$insert_id = insert_record('presentations',$new_presentation);
						$new_presentation->ident = $insert_id;
						
						insert_tags_from_string ($new_presentation_keywords, 'presentation', $insert_id, $new_presentation->access, $new_presentation->owner);
						
						$new_presentation = plugin_hook("presentation","publish",$new_presentation);
						$messages[] = __gettext("Your presentation has been created.");
						define('redirect_url',$CFG->wwwroot . user_info("username",$USER->ident) . "/presentations/");
					}
				} else {
					$messages[] = __gettext("Your presentation has to have a name.");
				}
			break;
			
			
			
			case "presentation:editsave":
				
				$edit_presentation = new StdClass;
				$edit_presentation->ident = optional_param('edit_presentation_ident',0,PARAM_INT);
				$edit_presentation->name = trim(optional_param('edit_presentation_title'));
				$edit_presentation->access = trim(optional_param('edit_presentation_access'));
				$edit_presentation_keywords = trim(optional_param('edit_presentation_keywords'));
				
				$presrec = get_record("presentations", "ident", $edit_presentation->ident);
				
				// presentation exists? right owner?
				if ($presrec && presentation_permissions_check($presrec->ident, $USER->ident)) {
					
					if ($edit_presentation->name) {
						
						$edit_presentation = plugin_hook("presentation","update",$edit_presentation);
						if (!empty($edit_presentation)) {
							$insert_id = update_record('presentations',$edit_presentation);
							
							delete_records('tags','tagtype','presentation','ref',$edit_presentation->ident);
							insert_tags_from_string ($edit_presentation_keywords, 'presentation', $edit_presentation->ident, $edit_presentation->access, $USER->ident);
							
							$edit_presentation = plugin_hook("presentation","republish",$edit_presentation);
							$messages[] = __gettext("Your changes have been saved.");
							$page_username = user_info("username",$USER->ident);
							define('redirect_url',$CFG->wwwroot . $page_username . "/presentations/" . $edit_presentation->ident . '/edit');
							//define('redirect_url',$CFG->wwwroot . "mod/presentation/edit.php?id=" . $edit_presentation->ident);
						}
					} else {
						$messages[] = __gettext("Your changes could not be saved.");
					}
				}
			break;
			
			
			
			case "presentation:delete":
				
				$deleteident = optional_param('delete_presentation_ident',0,PARAM_INT);
				
				$presrec = get_record("presentations", "ident", $deleteident);
				
				// presentation exists? right owner?
				if ($presrec && presentation_permissions_check($presrec->ident, $USER->ident)) {
					
					//$edit_presentation = plugin_hook("presentation","delete",$edit_presentation);
					
					presentation_delete($deleteident);
					
					//$edit_presentation = plugin_hook("presentation","republish",$edit_presentation);
					$messages[] = __gettext("Your presentation was deleted.");
					
					$page_username = user_info("username",$USER->ident);
					define('redirect_url',$CFG->wwwroot . $page_username . "/presentations/");
				} else {
					$messages[] = __gettext("Your presentation could not be deleted.");
				}
			break;
			
			
			
			case "presentation:movesec":
				$presentation = optional_param('id',0,PARAM_INT);
				$section = optional_param('sec',0,PARAM_INT);
				$direction = optional_param('dir');
				
				$presrec = get_record("presentations", "ident", $presentation);
				
				// presentation exists? right owner?
				if ($presrec && presentation_permissions_check($presrec->ident, $USER->ident)) {
					if ($secrec = get_record("presentation_sections", "ident", $section, "presentation", $presentation)) {
						if ($direction == "up") {
							presentation_section_moveup($secrec);
						} elseif ($direction == "down") {
							presentation_section_movedown($secrec);
						}
					}
					$page_username = user_info("username",$USER->ident);
					define('redirect_url',$CFG->wwwroot . $page_username . "/presentations/" . $presentation . '/edit');
					//define('redirect_url',$CFG->wwwroot . "mod/presentation/edit.php?id=" . $presentation);
				}
				
			break;
			
			
			
			case "presentation:delsec":
				$presentation = optional_param('id',0,PARAM_INT);
				$section = optional_param('sec',0,PARAM_INT);
				
				$presrec = get_record("presentations", "ident", $presentation);
				
				// presentation exists? right owner?
				if ($presrec && presentation_permissions_check($presrec->ident, $USER->ident)) {
					if ($secrec = get_record("presentation_sections", "ident", $section, "presentation", $presentation)) {
						presentation_section_delete($presentation, $section);
						$messages[] = __gettext("Section deleted.");
					}
					$page_username = user_info("username",$USER->ident);
					define('redirect_url',$CFG->wwwroot . $page_username . "/presentations/" . $presentation . '/edit');
					//define('redirect_url',$CFG->wwwroot . "mod/presentation/edit.php?id=" . $presentation);
				}
				
			break;
			
			
			
			case "presentation:savesec":
				$presentation = optional_param('id',0,PARAM_INT);
				$section = optional_param('sec',0,PARAM_INT);
				$sectiontype = optional_param('sectype');
				$dataarray = optional_param('pres_sec_data');
				
				$presrec = get_record("presentations", "ident", $presentation);
				
				$time = time();
				
				// presentation exists? right owner?
				if ($presrec && presentation_permissions_check($presrec->ident, $USER->ident)) {
					
					if ($section == -1) {
						//new section
						$newsec = new stdClass;
						$newsec->presentation = $presrec->ident;
						$newsec->section_type = $sectiontype;
						$newsec->updated = $newsec->created = $time;
						$newsec->display_order = 10001; //yeah, i know. i can't be arsed. - sven
						
						if ($insert = insert_record('presentation_sections', $newsec)) {
							$func = "presentation_section_save_" . $sectiontype;
							if (is_callable($func)) {
								$func($presrec->ident, $insert, $dataarray);
							}
							presentation_sections_reorder($presrec->ident);
							$messages[] = __gettext("Added section.");
							
						} else {
							$messages[] = __gettext("Error: Could not create section.");
						}
						
					} else {
						//existing section
						
						if ($secrec = get_record("presentation_sections", "ident", $section, "presentation", $presrec->ident)) {
							$func = "presentation_section_save_" . $sectiontype;
							if (is_callable($func)) {
								$func($presrec->ident, $section, $dataarray);
							}
							set_field('presentation_sections', 'updated', $time, 'ident', $section);
						} else {
							$messages[] = __gettext("Error: Could not find section to update.");
						}
						
						
						
					}
					$page_username = user_info("username",$USER->ident);
					define('redirect_url',$CFG->wwwroot . $page_username . "/presentations/" . $presentation . '/edit');
					//define('redirect_url',$CFG->wwwroot . "mod/presentation/edit.php?id=" . $presentation);
				}
				
			break;
			
			case "presentation:delcomment":
				
				$presentation = optional_param('id',0,PARAM_INT);
				$section = optional_param('sec',0,PARAM_INT);
				$deleteident = optional_param('commentid',0,PARAM_INT);
				
				$presrec = get_record("presentations", "ident", $presentation);
				$commentrec = get_record("presentation_comments", "ident", $deleteident);
				
				// presentation + comment exist? presentation or comment owner?
				if (
					$presrec && 
					$commentrec && 
					(
						presentation_permissions_check($presrec->ident, $USER->ident)
						|| $commentrec->owner == $USER->ident
					)
				) {
					
					delete_records('presentation_comments', 'ident', $deleteident, 'presentation', $presentation, 'section', $section);
					
					$messages[] = __gettext("Comment deleted.");
					
					$page_username = user_info("username",$USER->ident);
					define('redirect_url',$CFG->wwwroot . $page_username . "/presentations/" . $presentation);
				} else {
					$messages[] = __gettext("Comment could not be deleted.");
				}
			break;
			
			
			
		} // end switch
		
	} // end if logged_on
	
	// logged-out actions
	switch ($action) {
		
		case "presentation:addcomment":
			
			$presentation = optional_param('id',0,PARAM_INT);
			$section = optional_param('sec',0,PARAM_INT);
			$presrec = get_record("presentations", "ident", $presentation);
			
			// presentation exists? comment poster is logged on or public comments allowed?
			if ($presrec && (logged_on || (!$CFG->disable_publiccomments && user_flag_get("publiccomments",$presrec->owner)) )) {
				
				if ($section && !record_exists("presentation_sections", "ident", $section, "presentation", $presentation)) {
					$section = 0;
				}
				
				$newcomment = new stdClass;
				$newcomment->presentation = $presrec->ident;
				$newcomment->section = $section;
				$newcomment->created = time();
				if (logged_on) {
					$newcomment->owner = $USER->ident;
				} else {
					$newcomment->owner = -1;
				}
				$newcomment->postedname = trim(optional_param('postedname'));
				$newcomment->body = trim(optional_param('commentbody'));
				
				if ($newcomment->body) {
					if ($insert = insert_record('presentation_comments', $newcomment)) {
						$messages[] = __gettext("Comment added.");
					} else {
						$messages[] = __gettext("Error: Failed to add comment.");
					}
				} else {
					$messages[] = __gettext("Error: Not adding blank comment.");
				}
				
				//stay on the same page
				define('redirect_url', $_SERVER['REQUEST_URI']);
			}
			
		break;
		
	} // end switch
	
	
	
	
	
	if (defined('redirect_url')) {
		$_SESSION['messages'] = $messages;
		header("Location: " . redirect_url);
		exit;
	}
	
} // end function




/**
 * Reorders sections for a particular presentation
 * 
 * @param int $presentationid ID of the presentation to reorder
 * 
 */
function presentation_sections_reorder($presentationid) {
	
	$sections = presentation_get_sections($presentationid);
	if (!empty($sections) && is_array($sections)) {
		$order = array();
		$i = 1;
		foreach($sections as $section) {
			$order[$section->ident] = $i * 10;
			$i++;
		}
		foreach($order as $ident => $display_order) {
			// no point in updating non-changing values
			if ($sections[$ident]->display_order != $display_order) {
				$section = new StdClass;
				$section->display_order = $display_order;
				$section->ident = $ident;
				update_record('presentation_sections',$section);
			}
		}
	}
	
} // end function

/**
 * Move a section up
 * 
 * @param object $section section row to move
 * 
 */
function presentation_section_moveup($section) {
	$section->display_order = $section->display_order - 11;
	update_record('presentation_sections',$section);
	presentation_sections_reorder($section->presentation);
} // end function

/**
 * Move a section down
 * 
 * @param object $section section row to move
 * 
 */
function presentation_section_movedown($section) {
	$section->display_order = $section->display_order + 11;
	update_record('presentation_sections',$section);
	presentation_sections_reorder($section->presentation);
} // end function



/**
 * Get section rows for a presentation
 * 
 * @param int $presentation presentation id
 * @return mixed false, or an array of sections
 */
function presentation_get_sections($presentation) {
	
	return get_records('presentation_sections', 'presentation', $presentation, 'display_order');
	
} // end function


/**
 * Purge a section from a presentation
 * 
 * @param int $presentation presentation id
 * @param int $section section id
 */
function presentation_section_delete($presentation, $sectionid) {
	
	$secrec = get_record("presentation_sections", "ident", $sectionid, "presentation", $presentation);
	$func = "presentation_section_delete_" . $secrec->section_type;
	if (is_callable($func)) {
		$func($presentation, $sectionid);
	}
	
	delete_records('presentation_comments', 		'section', $sectionid, 'presentation', $presentation);
	delete_records('presentation_sections_data',	'section', $sectionid, 'presentation', $presentation);
	delete_records('presentation_sections', 		'ident', $sectionid, 'presentation', $presentation);
	
} // end function



/**
 * Purge a presentation
 * 
 * @param int $presentation ID of presentation to delete
 */
function presentation_delete($presentation) {
	
	if ($secrecs = get_records("presentation_sections", "presentation", $presentation)) {
		foreach ($secrecs as $secrec) {
			presentation_section_delete($presentation, $secrec->ident);
		}
	}
	
	delete_records('tags','tagtype','presentation','ref',$presentation);
	delete_records('presentations', 'ident', $presentation);
	
} // end function



/**
 * Check whether a user id has write permission for a presentation
 * Currently just checks owner, but could be so much more!
 * 
 * @param int $presentation presentation id
 * @param int $userid user id
 * @return boolean true for has write permission
 */
function presentation_permissions_check($presentation, $userid) {
	
	$presrec = get_record("presentations", "ident", $presentation);
	
	// presentation exists? right owner?
	if ($presrec && $presrec->owner == $userid) {
		return true;
	} else {
		return false;
	}
} // end function




/**
 * Returns HTML for a particular section
 * 
 * @param object $section section row
 * @return string HTML output
 */
function presentation_section_display($section, $menuedit = false, $menucomments = true, $forcenoaddcomments = false) {
	
	global $CFG, $PAGE, $USER;
	
	$body = "";
	if (is_object($section)) {
		
		// If the handler for displaying this particular section type exists,
		// run it - otherwise display nothing
		$func = "presentation_section_display_" . $section->section_type;
		if (is_callable($func)) {
			$body = $func($section);
		}
		$body = "<div class=\"presentation-widget-content\">$body</div>\n";
		
		$menu = array();
		if ($menuedit && isloggedin() && presentation_permissions_check($section->presentation, $USER->ident)) {
			$menu[] = '<a href="' . $CFG->wwwroot . 'mod/presentation/editsection.php?id=' . $section->presentation . '&amp;sec=' . $section->ident . '">' . __gettext("Edit section") . "</a>";
			$menu[] = '<a href="' . $CFG->wwwroot . 'mod/presentation/edit.php?action=presentation:delsec&amp;id=' . $section->presentation . '&amp;sec=' . $section->ident . '" onclick="return confirm(\'' . __gettext("Are you sure you want to delete this section?") . '\')">' . __gettext("Delete section") . "</a>";
			$menu[] = '<a href="' . $CFG->wwwroot . 'mod/presentation/edit.php?action=presentation:movesec&amp;id=' . $section->presentation . '&amp;sec=' . $section->ident . '&amp;dir=up">' . __gettext("Move up") . "</a>";
			$menu[] = '<a href="' . $CFG->wwwroot . 'mod/presentation/edit.php?action=presentation:movesec&amp;id=' . $section->presentation . '&amp;sec=' . $section->ident . '&amp;dir=down">' . __gettext("Move down") . "</a>";
		}
		
		//
			
			//TODO comment link/code here?
			
		//}
		$commentstext = '';
		if ($menucomments) {
			$numcomments = 0;
			if ($comments = presentation_get_comments($section->presentation, $section->ident)) {
				$numcomments = count($comments);
			}
			
			$blockid = "cmtp" . $section->presentation . "s" . $section->ident;
			
			$presrec = get_record("presentations", "ident", $section->presentation);
			$addform = false;
			if (!$forcenoaddcomments) {
				if (logged_on || (!$CFG->disable_publiccomments && user_flag_get("publiccomments",$presrec->owner)) ) {
					$addform = true;
				}
			}
			
			$menutext = '';
			if ($numcomments || $addform) {
				$menutext .= '<a class="presentation-seclinkcom" href="javascript:toggleVisibility(\'' . $blockid . '\')">';
				$menutext .= $numcomments . __gettext(' Section Comments');
				$menutext .= '</a>';
			}
			if ($menutext) {
				$menu[] = $menutext;
			}
			
			$commentstext = presentation_comments_display($section->presentation, $section->ident, $comments, $addform);
		}
		
		if ($menuedit || $menucomments) {
			$menu[] = '<a class="presentation-seclinktop" href="#container" title="' . __gettext('Jump to the top of the page') . '">' . __gettext('Top') . '</a>';
			$body .= '<p class="presentation-widget-menu">' . implode(' | ', $menu) . '</p>';
			$body .= $commentstext;
		}
		
		$body = "<div class=\"presentation-widget\">$body</div>\n";
		
	}
	
	// Return HTML
	return $body;
	
} // end function


/**
 * Return title for section
 * 
 * @param object $section section row
 * @return string HTML output
 */
function presentation_section_gettitle($section) {
	
	$body = '';
	if (is_object($section)) {
		
		// If the handler for displaying this particular section type exists,
		// run it - otherwise display nothing
		$func = "presentation_section_gettitle_" . $section->section_type;
		if (is_callable($func)) {
			$body = $func($section);
		}
		
	}
	
	// Return HTML
	return $body;
}


/**
 * Returns HTML for editing a particular section
 * 
 * @param object $section section row object
 * @return string HTML output
 */
function presentation_section_edit($section) {
	
	global $CFG, $PAGE;
	
	$body = "something went wrong";
	$showform = true;
	
	if (is_object($section)) {
		// If the handler for displaying this particular section type exists,
		// run it - otherwise display nothing
		$func = "presentation_section_edit_" . $section->section_type;
		if (is_callable($func)) {
			$obj = $func($section);
			$body = $obj->body;
			$showform = $obj->showform;
		}
		
		if ($section->ident > 0) {
			$submitstring = __gettext("Update section");
		} else {
			$submitstring = __gettext("Add section");
		}
		
		if ($showform) {
			// Stick it in an appropriate form for saving
			$body = "<form action=\"" . $CFG->wwwroot . "mod/presentation/edit.php\" method=\"post\">\n" . $body;
			
			$body .= '
			<p>
				<input type="hidden" name="action" value="presentation:savesec" />
				<input type="hidden" name="id" value="' . $section->presentation . '" />
				<input type="hidden" name="sec" value="' . $section->ident . '" />
				<input type="hidden" name="sectype" value="' . $section->section_type . '" />
				<input type="submit" value="' . $submitstring . '" />
			</p>
			</form>
			';
		}
		
	}
	
	return $body;
	
} // end function







/**
 * Display plain text section
 * 
 * @param object $section section row
 * @return string HTML output
 */
function presentation_section_display_text($section) {
	
	global $CFG;
	
	$title = presentation_section_get_data($section->ident,"title");
	$body = presentation_section_get_data($section->ident,"body");
	$return = '';
	
	if ($title->value) {
		$return .= "<h2>" . $title->value . "</h2>\n";
	}
	if ($body->value) {
		$return .= "<div>" . nl2br($body->value) . "</div>\n";
	}
	
	return $return;
	
} // end function


/**
 * Return title for plain text section
 * 
 * @param object $section section row
 * @return string HTML output
 */
function presentation_section_gettitle_text($section) {
	
	global $CFG;
	
	$title = presentation_section_get_data($section->ident,"title");
	if (empty($title->value)) {
		$ret = 'Untitled';
	} else {
		$ret = htmlspecialchars($title->value);
	}
	
	return $ret;
	
} // end function


/**
 * Display edit form for plain text section
 * 
 * @param object $section section row
 * @return object body=HTML output, showform=whether to surround output with a form
 */
function presentation_section_edit_text($section) {
	
	global $CFG, $page_owner;
	$ret = new stdClass;
	$ret->body = "";
	$ret->showform = true;
	
	if (is_object($section)) {
		if ($section->ident > 0) {
			$sectitle = presentation_section_get_data($section->ident,"title");
			$sectitle = $sectitle->value;
			$secbody = presentation_section_get_data($section->ident,"body");
			$secbody = $secbody->value;
		} else {
			$sectitle = '';
			$secbody = '';
		}
		
		$ret->body .= "<h2>" . __gettext("Text box") . "</h2>\n";
		$ret->body .= "<p>" . __gettext("This displays the text content of your choice. All you need to do is enter the title and body below:") . "</p>\n";
		
		if ($section->ident > 0) {
			// presentation edit page runs its own
			run('tinymce:include', array(array("pres_sec_data__body__")) );
		}
		
		$ret->body .= "<p>" . display_input_field(array("pres_sec_data[title]",$sectitle,"text")) . "</p>\n";
		$ret->body .= "<p>" . display_input_field(array("pres_sec_data[body]",$secbody,"longtext")) . "</p>\n";
		
	}
	
	return $ret;
	
} // end function


/**
 * Handle saving data for a plain text section
 * 
 * @param int $presentation presentation id
 * @param int $section section id
 * @param array $dataarray extra data
 * @return boolean
 */
function presentation_section_save_text($presentation, $section, $dataarray) {
	
	global $CFG, $page_owner;
	$return = true;
	
	if (is_array($dataarray)) {
		foreach ($dataarray as $name => $value) {
			presentation_section_set_data($presentation, $section, $name, $value);
		}
	}
	
	return $return;
} // end function







/**
 * Display blog post section
 * 
 * @param object $section section row
 * @return string HTML output
 */
function presentation_section_display_blogpost($section) {
	
	global $CFG, $template;
	$body = "";
	
	if (is_object($section)) {
		
		$data = presentation_section_get_all_data($section->ident);
		//var_dump($data);
		$sectitle = isset($data['title']) ? $data['title']->value : 'Untitled';
		$secbody = isset($data['body']) ? $data['body']->value : '';
		$secposted = isset($data['posted']) ? strftime("%d %B %Y", $data['posted']->value) : '';
		$secicon = isset($data['icon']) ? $data['icon']->value : -1;
		$secblogid = isset($data['weblog']) ? $data['weblog']->value : '';
		$secowner = isset($data['owner']) ? $data['owner']->value : '';
		$commentsbody = isset($data['comments']) ? $data['comments']->value : '';
		$numcomments = isset($data['numcomments']) ? $data['numcomments']->value : 0;
		$tags = isset($data['tags']) ? $data['tags']->value : '';
		$narrative = isset($data['narrative']) ? $data['narrative']->value : '';
		
		$sourceid = presentation_section_get_data($section->ident, "sourceid");
		
		$body .= '<h2>Blog post: ' . $secposted . '</h2>';
		//$body .= '<div>' . $secbody . '</div>';
		if ($narrative) {
			$body .= '<div class="presentation-narrative">' . $narrative . '</div>';
		}
		
		if ($numcomments && $commentsbody) {
			$blockid = "cmtp" . $section->presentation . "s" . $section->ident . "content";
			$commentsbody = templates_draw(
				array(
					'context' => 'presentation_weblogcomments',
					'comments' => $commentsbody,
					'numcomments' => $numcomments,
					'blockid' => $blockid,
				)
			);
		}
		
		if ($secicon == -1) {
			if ($newicon = user_info("icon", $secowner)) {
				$secicon = $newicon;
			}
		}
		
		$fullname = '';
		$username = '';
		$links = '';
		
		$body .= templates_draw(
			array(
				'context' => 'presentation_weblogpost',
				'date' => $secposted,
				'username' => $username,
				'usericon' => $secicon,
				'body' => $secbody,
				'fullname' => $fullname,
				'title' => $sectitle,
				'comments' => $commentsbody,
				'tags' => $tags,
				'links' => $links
			)
		);
		
		if (!empty($sourceid)) {
			$body .= '<div class="presentation-imported-guff">';
			if ($sourceid->created) {
				$body .= __gettext('Imported at: ') . strftime("%d/%m/%Y %H:%M %Z", $sourceid->created) . '<br />';
			}
			
			if ($sourcepost = get_record("weblog_posts", "ident", $sourceid->value)) {
				$ownerusername = user_info("username", $sourcepost->weblog);
				$url = $CFG->wwwroot . $ownerusername . '/weblog/' . $sourceid->value . '.html';
				$body .= sprintf(__gettext('The original blog post this was imported from is <a href="%s">here</a>.'), $url) . '<br />';
			}
			$body .= '</div>';
		}
		
	}
	
	return $body;
}


/**
 * Return title for blog post section
 * 
 * @param object $section section row
 * @return string HTML output
 */
function presentation_section_gettitle_blogpost($section) {
	
	global $CFG;
	
	$title = presentation_section_get_data($section->ident,"title");
	$posted = presentation_section_get_data($section->ident,"posted");
	
	$ret = 'Blog post: ' . strftime("%d %B %Y", $posted->value) . ' -- ';
	
	if (empty($title->value)) {
		$ret .= 'Untitled';
	} else {
		$ret .= htmlspecialchars($title->value);
	}
	
	return $ret;
	
} // end function


/**
 * Display edit form for blog post section
 * 
 * @param object $section section row
 * @return object body=HTML output, showform=whether to surround output with a form
 */
function presentation_section_edit_blogpost($section) {
	
	global $CFG, $USER, $page_owner;
	$ret = new stdClass;
	$ret->body = "";
	$ret->showform = true;
	
	if (is_object($section)) {
	
		$presrec = get_record("presentations", "ident", $section->presentation);
		
		$ret->body .= "<h2>" . __gettext("Blog post") . "</h2>\n";
		$url = $CFG->wwwroot . $USER->username . '/weblog/';;
		
		$secnarrative = presentation_section_get_data($section->ident,"narrative");
		$secnarrative = $secnarrative->value;
		if ($section->ident > 0) {
			// presentation edit page runs its own
			run('tinymce:include', array(array("pres_sec_data__blognarrative__")) );
		}
		$ret->body .= "<p>" . __gettext('Description:') . "</p>\n";
		$ret->body .= "<p>" . display_input_field(array("pres_sec_data[blognarrative]",$secnarrative,"longtext")) . "</p>\n";
		
		if ($section->ident > 0) {
			//editing
			
			$ownerusername = user_info("username", $presrec->owner);
			$url = $CFG->wwwroot . $ownerusername . '/weblog/';
			
			$sourceid = presentation_section_get_data($section->ident, "sourceid");
			if ($sourcepost = get_record("weblog_posts", "ident", $sourceid->value)) {
				
				$url .= $sourceid->value . '.html';
				$ret->body .= "<p>" . sprintf(__gettext('You can reimport <a href="%s">this post</a> from your blog, optionally with the comments it currently has:'), $url) . "</p>\n";
				
				$ret->body .= '<p>'
				. '<label for="pres_sec_data__postid__"><input type="checkbox" id="pres_sec_data__postid__" name="pres_sec_data[postid]" value="' . $sourcepost->ident . '" /> ' . __gettext('Reimport blog post.') . '</label>'
				. '</p>'
				. "\n";
				$ret->body .= '<p>'
				. '<label for="pres_sec_data__withcomments__"><input type="checkbox" id="pres_sec_data__withcomments__" name="pres_sec_data[withcomments]" value="yes" /> ' . __gettext('Import comments too.') . '</label>'
				. '</p>'
				. "\n";
				
			} else {
				
				$ret->body .= "<p>" . sprintf(__gettext('You can not reimport this post, as it no longer exists on <a href="%s">your blog</a>.'), $url) . "</p>\n";
				$ret->showform = false;
			}
			
		} else {
			//adding
			
			$ret->body .= "<p>" . sprintf(__gettext('You can import a post from <a href="%s">your blog</a> into a new section, optionally with the comments it currently has:'), $url) . "</p>\n";
			
			if ($posts = get_records_select('weblog_posts', 'owner = '.$presrec->owner, null, 'posted ASC')) {
				$ret->body .= '<p><select name="pres_sec_data[postid]">' . "\n";
				$lasttime = '';
				foreach ($posts as $apost) {
					$time = strftime("%d %B %Y", $apost->posted);
					if ($time != $lasttime) {
						if ($lasttime) {
							$ret->body .= '</optgroup>' . "\n";
						}
						$ret->body .= '<optgroup label="' . $time . '">' . "\n";
					}
					$title = strip_tags(get_access_description($apost->access)) . (($apost->title) ? htmlspecialchars($apost->title) : "Untitled");
					if ($apost->weblog != $apost->owner) {
						$title = '@' . run("profile:display:name", $apost->weblog) . ': ' . $title;
					}
					$text = htmlspecialchars(substr($apost->body, 0, 50));
					$ret->body .= "\t" . '<option value="' . $apost->ident . '" title="' . $text . '">' . $title . '</option>' . "\n";
					$lasttime = $time;
				}
				if ($lasttime) {
					$ret->body .= '</optgroup>' . "\n";
				}
				$ret->body .= '</select></p>' . "\n";
				$ret->body .= '<p><label for="pres_sec_data__withcomments__"><input type="checkbox" id="pres_sec_data__withcomments__" name="pres_sec_data[withcomments]" value="yes" /> ' . __gettext('Import blog post comments too.') . '</label></p>' . "\n";
				
			} else {
				// no posts
				$ret->showform = false;
				$ret->body .= "<p>" . __gettext("You don't have any blog posts yet.") . "</p>\n";
				
			}
			
		}
		
	}
	
	return $ret;
}

/**
 * Handle saving data for a blog post section
 * 
 * @param int $presentation presentation id
 * @param int $section section id
 * @param array $dataarray extra data
 * @return boolean
 */
function presentation_section_save_blogpost($presentation, $section, $dataarray) {
	
	global $CFG, $USER, $page_owner;
	$return = true;
	
	if (!empty($dataarray['blognarrative'])) {
		presentation_section_set_data($presentation, $section, "narrative", $dataarray['blognarrative']);
	}
	
	if (!empty($dataarray['postid'])) {
		if ($post = get_record("weblog_posts", "ident", $dataarray['postid'], "owner", $USER->ident)) {
			
			presentation_section_set_data($presentation, $section, "sourceid", $post->ident);
			$postbody = run("weblogs:text:process", $post->body);
			presentation_section_set_data($presentation, $section, "body", $postbody);
			presentation_section_set_data($presentation, $section, "title", $post->title);
			presentation_section_set_data($presentation, $section, "icon", $post->icon);
			presentation_section_set_data($presentation, $section, "owner", $post->owner);
			presentation_section_set_data($presentation, $section, "weblog", $post->weblog);
			presentation_section_set_data($presentation, $section, "posted", $post->posted);
			
			$keywords = display_output_field(array("", "keywords", "weblog", "weblog", $post->ident, $post->owner));
			presentation_section_set_data($presentation, $section, "tags", $keywords);
			
			if (!empty($dataarray['withcomments'])) {
				//adding comments too
				
				$commentsbody = '';
				$numcomments = 0;
				if ($commentarray = get_records('weblog_comments','post_id',$post->ident,'posted ASC')) {
					
					$numcomments = count($commentarray);
					
					templates_page_setup();
					
					foreach ($commentarray as $comment) {
						// turn commentor name into a link if they're a registered user
						// add rel="nofollow" to comment links if they're not
						if ($comment->owner > 0) {
							$commentownerusername = user_info('username', $comment->owner);
							$comment->postedname = '<a href="' . $CFG->wwwroot . $commentownerusername . '/">' . $comment->postedname . '</a>';
							$comment->icon = '<a href="' . $CFG->wwwroot . $commentownerusername . '/"><img src="' . $CFG->wwwroot . '_icon/user/' . run("icons:get",$comment->owner) . '/w/50/h/50" border="0" align="left" alt="" /></a>';
							$comment->body = run("weblogs:text:process", array($comment->body, false));
						} else {
							$comment->icon = '<img src="' . $CFG->wwwroot . '_icons/data/default.png" width="50" height="50" align="left" alt="" />';
							$comment->body = run("weblogs:text:process", array($comment->body, true));
						}
						$commentsbody .= templates_draw(
							array(
								'context' => 'presentation_weblogcomment',
								'postedname' => $comment->postedname,
								'body' => '' . $comment->body,
								'posted' => strftime("%A, %d %B %Y, %H:%M %Z",$comment->posted),
								'usericon' => $comment->icon,
							)
						);
					}
					
				}
				
				presentation_section_set_data($presentation, $section, "comments", $commentsbody);
				presentation_section_set_data($presentation, $section, "numcomments", $numcomments);
			} else {
				presentation_section_set_data($presentation, $section, "comments", '');
				presentation_section_set_data($presentation, $section, "numcomments", 0);
			}
			
		}
	}
	
	return $return;
}










/**
 * Display file section
 * 
 * @param object $section section row
 * @return string HTML output
 */
function presentation_section_display_file($section) {
	
	global $CFG;
	$body = '';
	if (is_object($section)) {
		
		$data = presentation_section_get_all_data($section->ident);
		
		$title = isset($data['title']) ? $data['title']->value : 'Untitled';
		$description = isset($data['description']) ? $data['description']->value : '';
		$size = isset($data['size']) ? $data['size']->value : 0;
		$originalname = isset($data['originalname']) ? $data['originalname']->value : '';
		$location = isset($data['location']) ? $data['location']->value : '';
		$tags = isset($data['tags']) ? $data['tags']->value : '';
		$narrative = isset($data['narrative']) ? $data['narrative']->value : '';
		
		$sourceid = presentation_section_get_data($section->ident,"sourceid");
		
		$body .= "<h2>File: " . htmlspecialchars($title) . "</h2>\n";
		if ($narrative) {
			$body .= '<div class="presentation-narrative">' . $narrative . '</div>';
		}
		$url = $CFG->wwwroot . 'mod/presentation/file.php?pres=' . $section->presentation . '&amp;sec=' . $section->ident;
		
		$body .= '<div class="presentation-file">';
		$body .= '<a href="' . $url . '&amp;get=file"><img align="left" border="0" src="' . $url . '&amp;get=icon" alt="' . __gettext('Thumbnail') . '" /></a>';
		$body .= '<div style="margin-left: 100px;">';
		$body .= __gettext('Title: ') . htmlspecialchars($title) . '<br />';
		$body .= __gettext('Description: ') . $description . '<br />';
		$body .= __gettext('Keywords: ') . $tags . '<br />';
		$body .= __gettext('Size: ') . $size . ' ' . __gettext('bytes') . '<br />';
		$body .= __gettext('Original filename: ') . $originalname . '<br />';
		//$body .= 'location: ' . $location->value . '<br />';
		//$body .= 'Source: ' . $sourceid->value . '<br />';
		$body .= '</div>';
		
		$body .= '<div class="presentation-imported-guff">';
		$body .= __gettext('Imported at: ') . strftime("%d/%m/%Y %H:%M %Z", $sourceid->created) . '<br />';
		
		if ($sourcefile = get_record("files", "ident", $sourceid->value)) {
			$ownerusername = user_info("username", $sourcefile->files_owner);
			$url = $CFG->wwwroot . $ownerusername . '/files/';
			if ($sourcefile->folder > 0) {
				$url .= $sourcefile->folder . '/';
			}
			$body .= sprintf(__gettext('The original file this was imported from is <a href="%s">here</a>.'), $url) . '<br />';
		}
		
		$body .= '</div>';
		$body .= '</div>';
	}
	
	return $body;
}


/**
 * Return title for file section
 * 
 * @param object $section section row
 * @return string HTML output
 */
function presentation_section_gettitle_file($section) {
	
	$title = presentation_section_get_data($section->ident,"title");
	
	$ret = 'File: ';
	
	if (empty($title->value)) {
		$ret .= 'Untitled';
	} else {
		$ret .= htmlspecialchars($title->value);
	}
	
	return $ret;
	
} // end function


/**
 * Display edit form for file section
 * 
 * @param object $section section row
 * @return object body=HTML output, showform=whether to surround output with a form
 */
function presentation_section_edit_file($section) {
	
	global $CFG, $USER, $page_owner;
	$ret = new stdClass;
	$ret->body = "";
	$ret->showform = true;
	
	//TODO - better file selector
	
	if (is_object($section)) {
	
		$presrec = get_record("presentations", "ident", $section->presentation);
		
		$ret->body .= "<h2>" . __gettext("File") . "</h2>\n";
		$fileurl = $CFG->wwwroot . $USER->username . '/files/';
		
		$secnarrative = presentation_section_get_data($section->ident,"narrative");
		$secnarrative = $secnarrative->value;
		if ($section->ident > 0) {
			// presentation edit page runs its own
			run('tinymce:include', array(array("pres_sec_data__filenarrative__")) );
		}
		$ret->body .= "<p>" . __gettext('Description:') . "</p>\n";
		$ret->body .= "<p>" . display_input_field(array("pres_sec_data[filenarrative]",$secnarrative,"longtext")) . "</p>\n";
		
		if ($section->ident > 0) {
			//editing
			
			$ownerusername = user_info("username", $presrec->owner);
			
			$sourceid = presentation_section_get_data($section->ident, "sourceid");
			if ($sourcefile = get_record("files", "ident", $sourceid->value)) {
				
				$url = $CFG->wwwroot . '_files/edit_file.php?edit_file_id=' . $sourcefile->ident . '&amp;owner=' . $presrec->owner;
				$ret->body .= "<p>" . sprintf(__gettext('You can reimport this file, to update its title/description/keywords from the <a href="%s">source file</a>.'), $url) . '</p>';
				$ret->body .= '<p><label for="pres_sec_data__fileid__"><input type="checkbox" id="pres_sec_data__fileid__" name="pres_sec_data[fileid]" value="' . $sourcefile->ident . '" /> ' . __gettext('Reimport file.') . '</label></p>' . "\n";
				
			} else {
				
				$url = $CFG->wwwroot . $ownerusername . '/files/';
				$ret->body .= "<p>" . sprintf(__gettext('You can not reimport this file, as it no longer exists in <a href="%s">your files</a>.'), $url) . "</p>\n";
				$ret->showform = false;
				
			}
			
		} else {
			//adding
			
			$ret->body .= "<p>" . sprintf(__gettext('You can copy one of <a href="%s">your files</a> into a new section.'), $fileurl) . "</p>\n";
			if ($files = get_records_select('files',"files_owner = ?",array($USER->ident))) {
				
				$ret->body .= '<p><select name="pres_sec_data[fileid]">' . "\n";
				$ret->body .= presentation_viewfolder(-1, $USER->ident, 0);
				$ret->body .= "</select></p>\n";
				
			} else {
				
				// no files
				$ret->showform = false;
				$ret->body .= "<p>" . __gettext("You don't have any files yet.") . "</p>\n";
			}
			
		}
		
	}
	
	return $ret;
}

/**
 * Handle saving data for a file section
 * 
 * @param int $presentation presentation id
 * @param int $section section id
 * @param array $dataarray extra data
 * @return boolean
 */
function presentation_section_save_file($presentation, $section, $dataarray) {
	
	global $CFG, $USER, $page_owner;
	$return = false;
	
	require_once($CFG->dirroot . 'lib/filelib.php');
	
	$initial = substr($presentation, -1, 1);
	require_once($CFG->dirroot . 'lib/uploadlib.php');
	make_upload_directory('presentation');
	make_upload_directory('presentation/' . $initial);
	make_upload_directory('presentation/' . $initial . '/' . $presentation);
	
	if (!empty($dataarray['filenarrative'])) {
		presentation_section_set_data($presentation, $section, "narrative", $dataarray['filenarrative']);
	}
	
	if (!empty($dataarray['fileid'])) {
		if ($file = get_record("files", "ident", $dataarray['fileid'], "owner", $USER->ident)) {
			if ($filelocation = file_cache($file)) {
				$filename = basename($filelocation);
				$location = 'presentation/' . $initial . '/' . $presentation . '/' . time() . $filename;
				$destdir = $CFG->dataroot . $location;
//				var_dump($filelocation);
//				var_dump($destdir);
				if (copy($filelocation, $destdir)) {
					
					presentation_section_set_data($presentation, $section, "sourceid", $file->ident);
					presentation_section_set_data($presentation, $section, "originalname", $file->originalname);
					presentation_section_set_data($presentation, $section, "location", $location);
					presentation_section_set_data($presentation, $section, "title", $file->title);
					presentation_section_set_data($presentation, $section, "description", $file->description);
					presentation_section_set_data($presentation, $section, "size", $file->size);
					
					$keywords = display_output_field(array("", "keywords", "file", "file", $file->ident, $file->owner));
					presentation_section_set_data($presentation, $section, "tags", $keywords);
					
					$return = true;
				}
			}
		}
	}
	
	
	return $return;
}


/**
 * Handle deleting data for a file section
 * 
 * @param int $presentation presentation id
 * @param int $section section id
 * @return boolean
 */
function presentation_section_delete_file($presentation, $section) {
	
	global $CFG, $USER, $page_owner;
	$return = true;
	
	$location = presentation_section_get_data($section, "location");
	$fullocation = $CFG->dataroot . $location->value;
	if (is_file($fullocation)) {
		unlink($fullocation);
	}
	
	return $return;
}









/**
 * Get a data row for a section
 * 
 * @param int $sectionid section id
 * @param string $field "name"-field value to search for
 * @return object presentation_sections_data table row
 */
function presentation_section_get_data($sectionid, $field) {
	
	global $db, $CFG;
	
	$value = get_record_sql("SELECT * FROM ".$CFG->prefix."presentation_sections_data where section=? AND name=?", array($sectionid, $field));
	
	if (empty($value)) {
		$value = new stdClass;
		$value->name = $field;
		$value->value = "";
	}
	
	return $value;
	
} // end function



/**
 * Get a data array for a section
 * 
 * @param int $sectionid section id
 * @param string $field "name"-field value to search for
 * @return mixed false or an array of allpresentation_sections_data 
 */
function presentation_section_get_all_data($sectionid) {
	
	return get_records('presentation_sections_data', 'section', $sectionid, $sort='', $fields='name,value');
	
} // end function



/**
 * Sets data with a particular name for a particular section ID
 * 
 * @param int $presentation presentation id
 * @param int $section section id
 * @param string $name field name
 * @param string $value value to set
 * @return mixed something
 */
function presentation_section_set_data($presentation, $section, $name, $value) {
	$data = new stdClass;
	$data->presentation = $presentation;
	$data->section = $section;
	$data->name = $name;
	$data->value = $value;
	$data->created = time();
	$existing = presentation_section_get_data($section, $name);
	if (!empty($existing->ident)) {
		$data->ident = $existing->ident;
		return update_record('presentation_sections_data',$data);
	} else {
		return insert_record('presentation_sections_data',$data);
	}
} // end function













/**
 * copied from units/files/weblogs_posts_add_fields.php and modified
 * 
 * @param int $folderid folder id
 * @param int $userid user id whose files we're browsing
 * @param int $level how many levels down the recursion we are
 * @return string HTML of a list of <option> tags
 */

function presentation_viewfolder($folderid, $userid, $level) {

	$prefix = "";
	for ($i = 0; $i < $level; $i++) {
		$prefix .= "&gt;";
	}
	$fileprefix = $prefix . "&gt;";
	
	if ($folderid == -1) {
		$body = <<< END
			<option value="" title="Folder" disabled="disabled">ROOT</option>
END;
	} else {
		$current_folder = get_record('file_folders','files_owner',$userid,'ident',$folderid);
		$textlib = textlib_get_instance();
		$name = htmlspecialchars($textlib->strtoupper($current_folder->name));
		$body = <<< END
			<option value="" title="Folder" disabled="disabled">{$prefix} {$name}</option>
END;
	}
	if ($files = get_records_select('files',"files_owner = ? AND folder = ?",array($userid,$folderid))) {
		foreach($files as $file) {
			$filetitle = strip_tags(get_access_description($file->access)) . htmlspecialchars($file->title);
			$filename = htmlspecialchars($file->originalname);
			
			$body .= <<< END
			<option value="{$file->ident}" title="{$filename}">{$fileprefix} {$filetitle}</option>
END;
		}
	}
	
	if ($folders = get_records_select('file_folders',"files_owner = ? AND parent = ? ",array($userid,$folderid))) {
		foreach($folders as $folder) {
			$body .= presentation_viewfolder($folder->ident, $userid, $level + 1);
		}
	}
	return $body;
} // end function



/**
 * get the comments for a given presentation, overall or a section
 * 
 * @param int $presentation presentation id
 * @param int $section section id, 0 for presentation comments
 * @return mixed array list of comments, or false
 */

function presentation_get_comments($presentation, $section) {
	
	return get_records_select('presentation_comments', 'presentation = ? AND section = ?', array($presentation,$section), 'created ASC');
	
} // end function






/**
 * get the comment adding form
 * 
 * @param int $presentation presentation id
 * @param int $section section id, 0 for presentation comments
 * @param string $posturl URL the form should submit to
 * @return string HTML
 */
function presentation_comments_add_form($presentation, $section, $posturl) {
	
	global $CFG, $USER;
	$run_result = '';
	
	$posturl = htmlspecialchars($posturl);
	if ($section > 0) {
		$addComment = __gettext("Add a section comment"); // gettext variable
	} else {
		$addComment = __gettext("Add a comment"); // gettext variable
	}
	$run_result .= <<< END
		<form action="$posturl" method="post" class="presentation-commentaddform">
		<h2>$addComment</h2>
END;
	
	$id = 'commentbodyp' . $presentation . 's' . $section;
	$field = '<textarea name="commentbody" id="' . $id . '" style="width: 95%; height: 200px"></textarea>';
	
	$field .= <<< END
		<input type="hidden" name="action" value="presentation:addcomment" />
		<input type="hidden" name="id" value="{$presentation}" />
		<input type="hidden" name="sec" value="{$section}" />
END;
	
	$run_result .= templates_draw(array(
		'context' => 'databox1',
		'name' => __gettext("Your comment text"),
		'column1' => $field
		)
	);
	
	if (logged_on) {
		$comment_name = $USER->name;
	} else {
		$comment_name = __gettext("Guest");
	}
	
	$run_result .= templates_draw(array(
		'context' => 'databox1',
		'name' => __gettext("Your name"),
		'column1' => "<input type=\"text\" name=\"postedname\" value=\"".htmlspecialchars($comment_name, ENT_COMPAT, 'utf-8')."\" />"
		)
	);
	
	$run_result .= templates_draw(array(
		'context' => 'databox1',
		'name' => '&nbsp;',
		'column1' => "<input type=\"submit\" value=\"".__gettext("Add comment")."\" />"
		)
	);
	
	$run_result .= "</form>\n";
	
	return $run_result;
	
} // end function


/**
 * get HTML for a presentation or section's comments
 * 
 * @param int $presentation presentation id
 * @param int $section section id, 0 for presentation comments
 * @param array $commentarray array of comment db rows
 * @param boolean $showaddform whether to show the add comment form
 * @return string HTML
 */
function presentation_comments_display($presentation, $section, $commentarray, $showaddform = false) {
	
	global $CFG, $USER, $PAGE;
	$body = '';
	$commentsbody = '';
	
	if ($section > 0) {
		$commentcontext = 'presentation_comment';
		$commentscontext = 'presentation_sectioncomments';
		$blockid = "cmtp" . $presentation . "s" . $section;
	} else {
		$commentcontext = 'presentation_comment';
		$commentscontext = 'presentation_comments';
		$blockid = "cmtp" . $presentation . "s0";
	}
	
	$numcomments = 0;
	if (is_array($commentarray)) {
		$numcomments = count($commentarray);
		foreach ($commentarray as $comment) {
			
			// turn commentor name into a link if they're a registered user
			// add rel="nofollow" to comment links if they're not
			if ($comment->owner > 0) {
				$commentownerusername = user_info('username', $comment->owner);
				$comment->postedname = '<a href="' . $CFG->wwwroot . $commentownerusername . '/">' . $comment->postedname . '</a>';
				$comment->icon = '<a href="' . $CFG->wwwroot . $commentownerusername . '/"><img src="' . $CFG->wwwroot . '_icon/user/' . run("icons:get",$comment->owner) . '/w/50/h/50" border="0" align="left" alt="" /></a>';
				$comment->body = run("weblogs:text:process", array($comment->body, false));
			} else {
				$comment->icon = '<img src="' . $CFG->wwwroot . '_icons/data/default.png" width="50" height="50" align="left" alt="" />';
				$comment->body = run("weblogs:text:process", array($comment->body, true));
			}
			
			$commentmenu = '';
			if (logged_on && ($comment->owner == $USER->ident || presentation_permissions_check($presentation, $USER->ident) )) {
				$returnConfirm = __gettext("Are you sure you want to permanently delete this comment?");
				$url = $CFG->wwwroot . 'mod/presentation/index.php?action=presentation:delcomment&amp;id=' . $presentation . '&amp;sec=' . $section . '&amp;commentid=' . $comment->ident;
				$commentmenu = '<a href="' . $url . '" onclick="return confirm(\'' . $returnConfirm . '\')">' . __gettext("Delete") . '</a>';
			}
			
			
			$commentsbody .= templates_draw(
				array(
					'context' => $commentcontext,
					'postedname' => $comment->postedname,
					'body' => '' . $comment->body,
					'posted' => strftime("%A, %d %B %Y, %H:%M %Z",$comment->created),
					'usericon' => $comment->icon,
					'links' => $commentmenu,
				)
			);
			
		}
	}
	
	$addform = '';
	if ($showaddform) {
		$addform = presentation_comments_add_form($presentation, $section, $_SERVER['REQUEST_URI']);
	}
	
	if ($commentsbody || $addform) {
		$body .= templates_draw(
			array(
				'context' => $commentscontext,
				'comments' => $commentsbody,
				'addform' => $addform,
				'numcomments' => $numcomments,
				'blockid' => $blockid,
			)
		);
	}
	
	return $body;
} // end function


/**
 * hook into user deletion
 * 
 * @param string $object_type
 * @param string $event
 * @param object $object user to delete
 * @return object user to delete
 */
function presentation_user_delete($object_type, $event, $object) {
	
	if ($presentations = get_records('presentations', 'owner', $object->ident, $sort='', $fields='ident')) {
		foreach ($presentations as $apresentation) {
			presentation_delete($apresentation->ident);
		}
	}
	
	// You must pass $object back unless something goes wrong!
	return $object; 
} // end function


?>