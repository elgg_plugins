<?php
/**
 * Elgg Presentation tool
 * 
 * @author Sven Edge
 * @package mod_presentation
 */

	// Load Elgg framework
	@require_once("../../includes.php");

	// Define context
	define("context","presentation");
	
	// Load global variables
	global $CFG, $PAGE, $page_owner, $profile_id;
	
	protect(1);
	if (isloggedin()) {
		
		$pres = optional_param('id',0,PARAM_INT);
		$sec = optional_param('sec',0,PARAM_INT);
		$title = run("profile:display:name", $profile_id) . " :: " . __gettext("Edit Presentation Section");
		
		$presrec = get_record("presentations", "ident", $pres);
		$page_owner = $presrec->owner;
		$profile_id = $page_owner;
		
		templates_page_setup();
		if ($presrec && $presrec->owner == $USER->ident) {
			
			$PAGE->menu_sub[] = array(
				'name' => 'presentation',
				'html' => '<a href="' . $CFG->wwwroot . 'mod/presentation/edit.php?id=' . $presrec->ident . '">' . __gettext("Edit Presentation") . '</a>'
			);
			
			$body = "";
			//$body .= "<h1>" . __gettext("Editing a section") . "</h1>\n";
			
			if ($secrec = get_record("presentation_sections", "ident", $sec, "presentation", $presrec->ident)) {
				
				$body .= "<h1>" . __gettext("Current content") . "</h1>\n";
				$body .= presentation_section_display($secrec, $menuedit = false, $menucomments = false);
				
				$body .= "<h1>" . __gettext("Edit content") . "</h1>\n";
				$body .= presentation_section_edit($secrec);
				
				$body .= '<p>';
				$body .= '<a href="' . $CFG->wwwroot . 'mod/presentation/edit.php?id=' . $secrec->presentation . '">' . __gettext("Back without saving") . '</a>';
				$body .= '</p>';
				
			} else {
				$body = __gettext('Error: Section not found.');
			}
			
		} else {
			$body = __gettext('Error: Presentation not found or not owned by you.');
		}
	}
	
	// Output to the screen
	$body = templates_draw(array(
		'context' => 'contentholder',
		'title' => $title,
		'body' => $body
		)
	);
	
	echo templates_page_draw( array(
		$title, $body
		)
	);
	
?>