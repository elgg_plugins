<?php
/**
 * Elgg Presentation tool
 * 
 * @author Sven Edge
 * @package mod_presentation
 */

	// Load Elgg framework
	@require_once("../../includes.php");

	// Define context
	define("context","presentation");
	
	// Load global variables
	global $CFG, $PAGE, $page_owner, $profile_id;
	
	protect(1);
	if (isloggedin()) {
		
		$pres = optional_param('id',0,PARAM_INT);
		$title = run("profile:display:name", $profile_id) . " :: " . __gettext("Edit Presentation");
		
		$presrec = get_record("presentations", "ident", $pres);
		$page_owner = $presrec->owner;
		$profile_id = $page_owner;
		
		
		if ($presrec && $presrec->owner == $USER->ident) {
			
			if (isloggedin() && $presrec->owner == $USER->ident) {
				$PAGE->menu_sub[] = array(
					'name' => 'presentation:view',
					'html' => a_href(
						$CFG->wwwroot . $USER->username . '/presentations/' . $presrec->ident . '', 
						__gettext("View")
					)
				);
			}
			
			$PAGE->menu_sub[] = array(
				'name' => 'presentation:edit',
				'html' => a_href(
					$CFG->wwwroot . $USER->username . '/presentations/' . $presrec->ident . '/clean', 
					__gettext("Plain View")
				)
			);
			
			templates_page_setup();
			
			$body = "";
			
			$Keywords = __gettext("Keywords (Separated by commas):"); // gettext variable
			//$keywordDesc = __gettext("Keywords commonly referred to as 'Tags' are words that represent the weblog post you have just made. This will make it easier for others to search and find your posting."); // gettext variable
			$postTitle = __gettext("Title:"); // gettext variable
			$accessRes = __gettext("Access restrictions:"); // gettext variable
			
			$body .= "<h1>" . __gettext("General details") . "</h1>\n";
			$body .= "<form action=\"" . $CFG->wwwroot . "mod/presentation/index.php\" method=\"post\" >\n";
			
			$body .= templates_draw(array(
										'context' => 'databoxvertical',
										'name' => $postTitle,
										'contents' => display_input_field(array("edit_presentation_title",$presrec->name,"text"))
									)
									);
			
			$body .= templates_draw(array(
										'context' => 'databoxvertical',
										'name' => $Keywords,
										'contents' => display_input_field(array("edit_presentation_keywords","","keywords","presentation",$presrec->ident))
									)
									);
			
			$body .= templates_draw(array(
										'context' => 'databoxvertical',
										'name' => $accessRes,
										'contents' => run("display:access_level_select",array("edit_presentation_access",$presrec->access))
									)
									);
			
			$body .= "<p>";
			$body .= "<input type=\"submit\" value=\"" . __gettext("Save") . "\" />";
			$body .= "<input type=\"hidden\" name=\"action\" value=\"presentation:editsave\" />";
			$body .= "<input type=\"hidden\" name=\"owner\" value=\"$page_owner\" />";
			$body .= "<input type=\"hidden\" name=\"edit_presentation_ident\" value=\"$presrec->ident\" />";
			$body .= "</p>\n";
			
			$body .= "</form>\n";
			$body .= "<hr /><p>&nbsp;</p>\n";
			
			
			
			
			$body .= "<h1>" . __gettext("Content") . "</h1>\n";
			
			if ($sections = presentation_get_sections($presrec->ident)) {
				//var_dump($sections);
				foreach ($sections as $asection) {
					$body .= presentation_section_display($asection, $menuedit = true, $menucomments = false);
				}
			} else {
				$body .= "<p>" . __gettext("This presentation doesn't have any content yet.") . "</p>\n";
			}
			
			$body .= "<hr /><p>&nbsp;</p>\n";
			
			$metatags .= "<script type=\"text/javascript\" src=\"" . $CFG->wwwroot . "mod/profile/tabber/tabber.js\"></script>\n";
			$metatags .= "<link rel=\"stylesheet\" href=\"" . $CFG->wwwroot . "mod/profile/tabber/example.css\" type=\"text/css\" media=\"screen\" />\n";
			
			$body .= "<h1>" . __gettext("Add a section") . "</h1>\n";
			$body .= "<h2>" . __gettext("A presentation is made up of content sections, which come in various types:") . "</h2>\n";
			
			$newsection = new stdClass;
			$newsection->ident = -1;
			$newsection->presentation = $presrec->ident;
			
			$body .= "<div class=\"tabber\">\n";
			foreach($CFG->presentation->types as $atype) {
				$body .= '<div class="tabbertab" title="' . ucwords($atype) . '">';
				$newsection->section_type = $atype;
				$body .= presentation_section_edit($newsection);
				$body .= "</div>\n";
			}
			$body .= "</div><br />\n";
			
			// tinymce is annoying
			run('tinymce:include', array(array("pres_sec_data__blognarrative__", "pres_sec_data__filenarrative__", "pres_sec_data__body__")));
			
		} else {
			templates_page_setup(); // dunno if this is needed
			$body = __gettext('Error: Presentation not found or not owned by you.');
		}
		
	}
	
	// Output to the screen
	$body = templates_draw(array(
		'context' => 'contentholder',
		'title' => $title,
		'body' => $body
		)
	);
	
	echo templates_page_draw( array(
		$title, $body
		)
	);
	
?>