<?php
/**
 * Elgg Presentation tool
 * 
 * @author Sven Edge
 * @package mod_presentation
 */

	// Load Elgg framework
	@require_once("../../includes.php");

	// Define context
	define("context","presentation");
	
	// Load global variables
	global $CFG, $PAGE, $page_owner;
	
	// Get the current user
	$page_owner = optional_param('owner',$_SESSION['userid'],PARAM_INT); 
	
	// Initialise page body and title
	$body = "";
	$title = __gettext("Create presentation");
	
	protect(1);
	
	templates_page_setup();
	
	// Are we eligible to edit this?
	if (run("permissions:check","profile")) {
		
		$Keywords = __gettext("Keywords (Separated by commas):"); // gettext variable
		$keywordDesc = __gettext("Keywords commonly referred to as 'Tags' are words that represent the weblog post you have just made. This will make it easier for others to search and find your posting."); // gettext variable
		$postTitle = __gettext("Title:"); // gettext variable
		$accessRes = __gettext("Access restrictions:"); // gettext variable
		
		$contentTitle = trim(optional_param('new_presentation_title'));
		$contentKeywords = trim(optional_param('new_presentation_keywords'));
		
		$body = "<p>" . __gettext("You can create a new presentation:") . "</p>";
		
		$body .= "<form action=\"" . $CFG->wwwroot . "mod/presentation/new.php\" method=\"post\" >";
		
		$body .= templates_draw(array(
									'context' => 'databoxvertical',
									'name' => $postTitle,
									'contents' => display_input_field(array("new_presentation_title",$contentTitle,"text"))
								)
								);
								
		
		$body .= templates_draw(array(
									'context' => 'databoxvertical',
									'name' => $Keywords . "<br />" . $keywordDesc,
									'contents' => display_input_field(array("new_presentation_keywords","","keywords","presentation"))
								)
								);
				
		$body .= templates_draw(array(
									'context' => 'databoxvertical',
									'name' => $accessRes,
									'contents' => run("display:access_level_select",array("new_presentation_access",default_access))
								)
								);
		
		$body .= "<input type=\"submit\" value=\"" . __gettext("Create") . "\" />";
		$body .= "<input type=\"hidden\" name=\"action\" value=\"presentation:create\" />";
		$body .= "<input type=\"hidden\" name=\"owner\" value=\"$page_owner\" />";
		$body .= "</p>";
		
		$body .= "</form>";
		
	} else {
		$body = "<p>" . __gettext("You do not have permission to create a new presentation.") . "</p>";
	}
	
	// Output to the screen
	$body = templates_draw(array(
					'context' => 'contentholder',
					'title' => $title,
					'body' => $body
				)
				);
	
	echo templates_page_draw( array(
					$title, $body
				)
				);
	
?>