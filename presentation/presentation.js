
/* ######################################################################### */

function crossbrowser_return_documentobject (objectid) {
	
	if (document.getElementById) {
		return(document.getElementById(objectid));
	} else if (document.all) {
		return(document.all[objectid]);
	} else {
		return(null);
	}
	
}

/* ######################################################################### */

function toggleVisibility(destobjectid) {
	
	var obj = crossbrowser_return_documentobject(destobjectid);
	
	if (obj) {
		if (obj.style.display != 'none') {
			obj.style.display = 'none';
		} else {
			obj.style.display = '';
		}
	}

}

/* ######################################################################### */
