<?php
/**
 * Elgg Presentation tool
 * 
 * @author Sven Edge
 * @package mod_presentation
 */

	// Load Elgg framework
	@require_once("../../includes.php");

	// Define context
	define("context","presentation");
	
	// Load global variables
	global $CFG, $PAGE, $page_owner, $profile_id, $metatags, $template, $template_name;
	
	// Initialise page body and title
	$body = "";
	$title = __gettext("View Presentation");
	
	$view = optional_param('view');
	if ($view == "clean") {
		// non-elgg in-elgg view
		
		$template_name = '';
		templates_page_setup();
		
		$metatags = '';
		//var_dump($template_name);
		$metatags .= "\n" . '<script type="text/javascript" src="' . $CFG->wwwroot . 'mod/presentation/presentation.js"></script>' . "\n";
		
		$template['pageshell'] = file_get_contents($CFG->dirroot . "mod/presentation/plainviewpageshell.html");
		$template['css'] = file_get_contents($CFG->dirroot . "mod/presentation/presentation.css");
		$template['css'] .= file_get_contents($CFG->dirroot . "mod/presentation/plainviewpageshell.css");
		//var_dump($template);
	}
	
	$pres = optional_param('id',0,PARAM_INT);
	if ($presrec = get_record("presentations", "ident", $pres)) {
		$page_owner = $presrec->owner;
		$profile_id = $page_owner;
		$ownername = user_info("username",$page_owner);
		
		$title = run("profile:display:name", $page_owner) . " :: " . __gettext("View Presentation");
		
		if (isloggedin() && $presrec->owner == $USER->ident) {
			$PAGE->menu_sub[] = array(
				'name' => 'presentation:edit',
				'html' => a_href(
					$CFG->wwwroot . $ownername . '/presentations/' . $presrec->ident . '/edit', 
					__gettext("Edit")
				)
			);
		}
		
		$PAGE->menu_sub[] = array(
			'name' => 'presentation:edit',
			'html' => a_href(
				$CFG->wwwroot . $ownername . '/presentations/' . $presrec->ident . '/clean', 
				__gettext("Plain View")
			)
		);
		
		if ($view != "clean") {
			templates_page_setup();
		}
		
		$presentationsallowed = presentation_get_presentations_visible($page_owner);
		//var_dump($presentationsallowed);
		
		if ($presrec && $presentationsallowed && array_key_exists($presrec->ident, $presentationsallowed)) {
			
			$tinymceids = array();
			
			$body .= '<h1>';
			if ($view == "clean") {
				$body .= '<a href="' . $CFG->wwwroot . $ownername . '/profile/">' . run("profile:display:name", $presrec->owner) . '</a> :: ';
			}
			$body .= htmlspecialchars($presrec->name) . "</h1>";
			
			if ($view == "clean") {
				$menuedit = false;
				$menucomments = true;
				$disableaddcommentsform = true;
			} else {
				$menuedit = false;
				$menucomments = true;
				$disableaddcommentsform = false;
			}
			if ($sections = presentation_get_sections($presrec->ident)) {
				//var_dump($sections);
				
				$body .= '<div class="presentation-contentslist"><h2><a href="javascript:toggleVisibility(\'presentationcontentslist\')">' . __gettext('Contents') . '</a></h2><ol id="presentationcontentslist">';
				$i=1;
				foreach ($sections as $asection) {
					$atitle = presentation_section_gettitle($asection);
					$body .= '<li><a href="#sectop' . $i . '">' . $atitle . '</a></li>';
					$i++;
				}
				$body .= '</ol>
					<script type="text/javascript">
					<!--
						toggleVisibility(\'presentationcontentslist\');
					// -->
					</script><!-- --></div>
				';
				
				$i=1;
				foreach ($sections as $asection) {
					$body .= '<a name="sectop' . $i . '"></a>' . presentation_section_display($asection, $menuedit, $menucomments, $disableaddcommentsform);
					$tinymceids[] = 'commentbodyp' . $presrec->ident . 's' . $asection->ident;
					$i++;
				}
				
			} else {
				$body .= "<p>" . __gettext("This presentation doesn't have any content yet.") . "</p>\n";
			}
			
			$numcomments = 0;
			if ($comments = presentation_get_comments($presrec->ident, 0)) {
				$numcomments = count($comments);
			}
			$body .= "<h2>";
			if ($view == "clean") {
				$body .= '<a href="javascript:toggleVisibility(\'presentationoverallcomments\')">';
			}
			$body .= sprintf(__gettext('%s Presentation Comments'), $numcomments);
			if ($view == "clean") {
				$body .= '</a>';
			}
			$body .= "</h2>\n";
			$body .= '<div id="presentationoverallcomments">';
			
			if ($numcomments) {
				$body .= presentation_comments_display($presrec->ident, 0, $comments);
			}
			
			if (logged_on || (!$CFG->disable_publiccomments && user_flag_get("publiccomments",$presrec->owner)) ) {
				$tinymceids[] = 'commentbodyp' . $presrec->ident . 's0';
				$body .= presentation_comments_add_form($presrec->ident, 0, $_SERVER['REQUEST_URI']);
			} else {
				$body .= "<p>" . __gettext("You must be logged in to post a comment.") . "</p>";
			}
			
			$body .= '</div>';
			
			if ($view == "clean") {
				$body .= <<< END
				<script type="text/javascript">
				<!--
					toggleVisibility('presentationoverallcomments');
				// -->
				</script>
				<!-- -->
END;
			}
			
			run('tinymce:include', array($tinymceids) );
			
		} else {
			$body = __gettext('Error: Access denied.');
		}
	} else {
		$body = __gettext('Error: Presentation not found.');
	}
	
	
	
	
	
	
	
	
	if ($view == "clean") {
		
		// non-elgg in-elgg view
		echo templates_page_draw( array(
			$title, $body
			)
		);
		
	} else {
		
		// Output to the screen
		$body = templates_draw(array(
			'context' => 'contentholder',
			'title' => $title,
			'body' => $body
			)
		);
		
		echo templates_page_draw( array(
			$title, $body
			)
		);
	}
	
?>