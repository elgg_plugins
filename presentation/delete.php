<?php
/**
 * Elgg Presentation tool
 * 
 * @author Sven Edge
 * @package mod_presentation
 */

	// Load Elgg framework
	@require_once("../../includes.php");

	// Define context
	define("context","presentation");
	
	// Load global variables
	global $CFG, $PAGE, $USER, $page_owner, $profile_id;
	
	protect(1);
	if (isloggedin()) {
		
		$pres = optional_param('id',0,PARAM_INT);
		$title = run("profile:display:name", $profile_id) . " :: " . __gettext("Delete Presentation");
		
		$presrec = get_record("presentations", "ident", $pres);
		$page_owner = $presrec->owner;
		$profile_id = $page_owner;
		
		templates_page_setup();
		if ($presrec && $presrec->owner == $USER->ident) {
			
			$body = '<p>' . sprintf(__gettext('Click Delete if you are sure you want to permanently delete the presentation "%s".'), htmlspecialchars($presrec->name)) . '</p>';
			
			$body .= "<form action=\"" . $CFG->wwwroot . "mod/presentation/index.php\" method=\"post\" >";
			
			$body .= "<input type=\"submit\" value=\"" . __gettext("Delete") . "\" />";
			$body .= "<input type=\"hidden\" name=\"action\" value=\"presentation:delete\" />";
			$body .= "<input type=\"hidden\" name=\"owner\" value=\"$page_owner\" />";
			$body .= "<input type=\"hidden\" name=\"delete_presentation_ident\" value=\"$presrec->ident\" />";
			$body .= "</p>";
			
			$body .= "</form>";
			
		} else {
			$body = __gettext('Error: Presentation not found or not owned by you.');
		}
	}
	
	// Output to the screen
	$body = templates_draw(array(
		'context' => 'contentholder',
		'title' => $title,
		'body' => $body
		)
	);
	
	echo templates_page_draw( array(
		$title, $body
		)
	);
	
?>