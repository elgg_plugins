<?php
/**
 * Elgg Presentation tool
 * 
 * @author Sven Edge
 * @package mod_presentation
 */

	// Load Elgg framework
	@require_once("../../includes.php");

	// Define context
	define("context","presentation");
	
	// Load global variables
	global $CFG, $PAGE, $USER, $page_owner, $profile_id;
	
	// Get the current user
	$username = trim(optional_param('user',''));
	if ($username) {
		$user_id = user_info_username("ident",$username);
	} else {
		$user_id = optional_param('owner',0,PARAM_INT);
		$username = user_info("username",$user_id);
	}
	
	if (empty($username) || $user_id == false) {
		if (isloggedin()) {
			$user_id = $USER->ident;
			$username = $_SESSION['username'];
		} else {
			header("Location: " . $CFG->wwwroot);
			exit;
		}
	}
	
	$page_owner = $user_id;
	$profile_id = $page_owner;
	
	// Initialise page body and title
	$body = "";
	$title = run("profile:display:name", $user_id) . " :: " . __gettext("Presentations");
	
	// Get presentations
	$presentationslist = presentation_get_presentations_visible($user_id);
	
	templates_page_setup();
	
	if (!empty($presentationslist) && is_array($presentationslist)) {
		
		foreach($presentationslist as $apres) {
			
			$ownerusername = user_info("username",$apres->owner);
			$viewurl = $CFG->wwwroot . $ownerusername . "/presentations/" . $apres->ident;
			
			$body .= "<div class=\"presentation-widget\">\n";
			$body .= '<a href="' . $viewurl . '">' . get_access_description($apres->access) . htmlspecialchars($apres->name) . '</a>';
			$body .= '<br />' . __gettext("Keywords") . ': ';
			$body .= display_output_field(array("","keywords","presentation",null,$apres->ident,$apres->owner));
			
			if (isloggedin() && presentation_permissions_check($apres->ident, $USER->ident)) {
				$body .= "<p class=\"presentation-widget-menu\">";
				$body .= '   <a href="' . $viewurl . '">' . __gettext("View") . "</a>";
				$body .= ' | <a href="' . $viewurl . '/edit">' . __gettext("Edit") . "</a>";
				$body .= ' | <a href="' . $CFG->wwwroot . "mod/presentation/delete.php?id=" . $apres->ident . "\">" . __gettext("Delete") . "</a>";
				$body .= "</p>";
			}
			$body .= "\n</div>\n";
		}
		
	} else if ($user_id == $USER->ident) {
		$body .= "<p>" . __gettext("You haven't created any presentations yet.") . "</p>";
	} else {
		$body .= "<p>" . __gettext("This user hasn't created any presentations yet.") . "</p>";
	}
	
	// Output to the screen
	$body = templates_draw(array(
		'context' => 'contentholder',
		'title' => $title,
		'body' => $body
		)
	);
	
	echo templates_page_draw( array(
		$title, $body
		)
	);
	
?>