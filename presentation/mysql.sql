
CREATE TABLE `prefix_presentations` (
	`ident` INT(10) UNSIGNED NOT NULL auto_increment,
	`name` varchar(255) NOT NULL,
	`owner` INT(11) NOT NULL COMMENT '-> users.ident, creator',
	`access` varchar(20) NOT NULL,
	`created` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'unix timestamp',
	PRIMARY KEY (`ident`),
	KEY `owner` (`owner`), 
	KEY `access` (`access`)
);

CREATE TABLE `prefix_presentation_sections` (
	`ident` INT(10) UNSIGNED NOT NULL auto_increment,
	`presentation` INT(10) UNSIGNED NOT NULL COMMENT '-> presentations.ident',
	`section_type` varchar(20) NOT NULL,
	`display_order` INT(11) NOT NULL,
	`created` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'unix timestamp',
	`updated` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'last update from source',
	PRIMARY KEY (`ident`),
	KEY `presentation` (`presentation`),
	KEY `display_order` (`display_order`)
);

CREATE TABLE `prefix_presentation_sections_data` (
	`ident` INT(10) UNSIGNED NOT NULL auto_increment,
	`presentation` INT(10) UNSIGNED NOT NULL COMMENT '-> presentations.ident',
	`section` INT(10) UNSIGNED NOT NULL COMMENT '-> presentation_sections.ident',
	`name` varchar(128) NOT NULL,
	`value` TEXT NOT NULL,
	`created` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'unix timestamp',
	PRIMARY KEY (`ident`),
	KEY `presentation` (`presentation`),
	KEY `section` (`section`)
);

CREATE TABLE `prefix_presentation_comments` (
	`ident` INT(10) UNSIGNED NOT NULL auto_increment,
	`presentation` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '-> presentations.ident',
	`section` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '-> presentation_sections.ident, optional',
	`owner` INT(11) NOT NULL DEFAULT '0' COMMENT '-> users.ident, commenter',
	`postedname` varchar(128) NOT NULL DEFAULT '' COMMENT 'displayed name of commenter',
	`body` TEXT NOT NULL,
	`created` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'unix timestamp',
	PRIMARY KEY (`ident`),
	KEY `owner` (`owner`),
	KEY `created` (`created`),
	KEY `presentationsection` (`presentation`, `section`)
);
