<?php

// Icon script

// Run includes
@require_once("../../includes.php");

global $CFG, $USER;

// If an ID number for the file has been specified ...
$presentation = optional_param('pres',0,PARAM_INT);
$section = optional_param('sec',0,PARAM_INT);
$fileoricon = optional_param('get');

if (!empty($presentation) && !empty($section)) {
	
	$presrec = get_record("presentations", "ident", $presentation);
	
	$presentationslist = presentation_get_presentations_visible($presrec->owner);
	
	templates_page_setup();
	
	if (!empty($presentationslist) && is_array($presentationslist)) {
		
		if(array_key_exists($presentation, $presentationslist)) {
			
			if ($secrec = get_record("presentation_sections", "ident", $section, "presentation", $presentation)) {
				
				$location = presentation_section_get_data($secrec->ident,"location");
				$location = $CFG->dataroot . $location->value;
				$originalname = presentation_section_get_data($secrec->ident,"originalname");
				$originalname = $originalname->value;
				
				require_once($CFG->dirroot . 'lib/filelib.php');
				$mimetype = mimeinfo('type',$originalname);
				
				if ($presrec->access == 'PUBLIC') {
					header("Pragma: public");
					header("Cache-Control: public");
				} else {
					// "Cache-Control: private" to allow a user's browser to cache the file, but not a shared proxy
					// Also to override PHP's default "DON'T EVER CACHE THIS EVER" header
					header("Cache-Control: private");
				}
				
				if ($fileoricon == "icon") {
					
					$w = optional_param('w',90,PARAM_INT);
					$h = optional_param('h',90,PARAM_INT);
					
					require_once($CFG->dirroot . 'lib/iconslib.php');
					
					// images most likely don't want compressing, and this will kill the Vary header
					if (function_exists('apache_setenv')) { // apparently @ isn't enough to make php ignore this failing
						@apache_setenv('no-gzip', '1');
					}
					
					if ($mimetype == "image/jpeg" || $mimetype == "image/png" || $mimetype == "image/gif") {
						// file is an image
						
						$phpthumbconfig = array();
						$phpthumbconfig['w'] = $w;
						$phpthumbconfig['h'] = $h;
						//var_dump($phpthumbconfig);
						spit_phpthumb_image($location, $phpthumbconfig);
						
					} else {
						
						// file is a file
						header($_SERVER['SERVER_PROTOCOL'] . " 301 Moved Permanently"); //permanent because the user's file can't change and keep the same id
						header("Location: " . $CFG->wwwroot . '_files/file.png');
						die();
						
					}
					
				} else {
					
					// disable mod_deflate/mod_gzip for already-compressed files,
					// partly because it's pointless, but mainly because some browsers
					// are thick.
					if (preg_match('#^(application.*zip|image/(png|jpeg|gif))$#', $mimetype)) {
						if (function_exists('apache_setenv')) { // apparently @ isn't enough to make php ignore this failing
							@apache_setenv('no-gzip', '1');
						}
					}
					
					if ($mimetype == "application/octet-stream") {
						header('Content-Disposition: attachment; Filename=' . $originalname);
					} else {
						header("Content-Disposition: inline; Filename=" . $originalname);
					}
					
					spitfile_with_mtime_check($location, $mimetype, "elgg");
					
				}
				
			}
		}
	}
}

?>