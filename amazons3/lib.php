<?php

    global $CFG;

    function amazons3_pagesetup() {
    }
    
    function amazons3_init() {
        
        /*
        
            Fill in your bucket name to use S3!
            
            Also remember to fill in your key and secret key in s3.class.php.
        
        */
        if (empty($CFG->files->amazons3->bucket)) {
            $CFG->files->amazons3->bucket = "";
        }
        
        
        global $CFG;
        $CFG->files->default_handler = "amazons3";
        $CFG->files->handler["amazons3"] = "amazons3_passthru";
        
        if (empty($CFG->files->siteid)) {
            $CFG->files->siteid = $CFG->dbuser;
        }
        listen_for_event("file","create","amazons3_create");
        listen_for_event("file","delete","amazons3_delete");
    }

    function amazons3_upload($originalname, $filePath, $bucket = NULL) {
        global $CFG;
        if ($bucket == NULL) {
            $bucket = $CFG->files->amazons3->bucket;
        }
        require_once("s3.class.php");
        $s3transfer = new s3();
        $result = $s3transfer->getObjects($bucket);
        if (substr_count($result,"The specified bucket does not exist")) {
            $s3transfer->putBucket($bucket);
        }
        if ($data = file_get_contents($filePath)) {
            $output = $s3transfer->putObject($originalname, $data, $bucket);
            header("content-type: ".$s3transfer->getResponseContentType());
        }
    }

    function amazons3_passthru($filename, $bucket = NULL) {
        global $CFG;
        
        $filename = $CFG->files->siteid . "/" . str_replace($CFG->dataroot, "", $filename);
        
        if ($bucket == NULL) {
            $bucket = $CFG->files->amazons3->bucket;
        }

        require_once($CFG->dirroot . 'lib/filelib.php');
        $mimetype = mimeinfo('type',$filename);
        require_once("s3.class.php");
        $s3transfer = new s3();
        $metadata = $s3transfer->getObjectInfo($filename, $bucket, NULL, $mimetype);
        $mimetype = mimeinfo('type',$filename);
        // header("Content-type: ".$mimetype);
        return $s3transfer->getObject($filename,$bucket);
    }
    
    function amazons3_create($object_type, $event, $object) {
        global $CFG;
        amazons3_upload($CFG->files->siteid . "/" . $object->location, $CFG->dataroot . $object->location);
        @unlink($CFG->dataroot . $object->location);
        $object->handler = "amazons3";
        return $object;
    }
    
    function amazons3_delete($object_type, $event, $object) {
        global $CFG;
        require_once("s3.class.php");
        $s3transfer = new s3();
        $s3transfer->deleteObject($CFG->files->siteid . "/" . $object->location, $CFG->files->amazons3->bucket);
        return $object;
    }

?>