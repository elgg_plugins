<?php
	/* WARNING: This is a HACK!
	 * This module is a helper to the ratings pluging to ensure that the rating link is always last on 
	 * weblog menus.
	 * @author Marcus Povey <marcus@dushka.co.uk>
	 */


	function z_ratings_init()
	{
		global $function, $CFG;

		if ($function['weblog:post:links'][count($function['weblog:post:links'])-1] != $CFG->dirroot . "mod/ratings/lib/weblog_post_links.php")
		{
			foreach ($function['weblog:post:links'] as $key => $f)
			{
				if (($f == $CFG->dirroot . "mod/ratings/lib/weblog_post_links.php") && ($key!=count($function['weblog:post:links'])-1))
				{
					$tmp = $function['weblog:post:links'][count($function['weblog:post:links'])-1];
	
					$function['weblog:post:links'][count($function['weblog:post:links'])-1] = $CFG->dirroot . "mod/ratings/lib/weblog_post_links.php";
	
					$function['weblog:post:links'][$key] = $tmp;
				}
			}
		}
	}
	
	function z_ratings_pagesetup()
	{
	}


?>
