<?php
/*
 * Created on Aug 30, 2007
 *
 * @author Diego Andrés Ramírez Aragón <diego@somosmas.org>
 * @copyright Corporación Somos más - 2007
 */
require_once (dirname(dirname(__FILE__)) . "/../includes.php");

require dirname(__FILE__)."/config.php";

run("profile:init");

$context = optional_param('type');
$suggest = $suggest_queries[$context];

if(!empty($suggest)){

  define("context", $suggest['menu_contrib']);
  templates_page_setup();
  
  define('SUGGEST_FILTER',false);
  
  $title = run("profile:display:name");
  $title .= " :: " . sprintf(__gettext("Suggested %s"),$suggest['title']);

  $vars[] = "suggest";
  $vars[] = SUGGEST_SUGGESTIONS_CONTRIB;
  $vars[] = $context;
  $body = suggest_mainbody($vars,'content_',"content");
}
else{
	$body = sprintf(__gettext("There is no suggestions for the '%s' context "),$context);
  $body="<p>$body</p>";
}

$body = templates_draw(array (
  'context' => 'contentholder',
  'title' => $title,
  'body' => "<div id=\"view_own_blog\">$body</div>"
));

echo templates_page_draw(array (
  $title,
  $body
));

?>
