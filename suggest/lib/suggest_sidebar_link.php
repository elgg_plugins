<?php

/*
 * Created on Sep 3, 2007
 *
 * @author Diego Andrés Ramírez Aragón <diego@somosmas.org>
 * @copyright Corporación Somos más - 2007
 */
global $page_owner;
global $suggest_queries;

if ($page_owner != -1 && $page_owner == $_SESSION['userid']) {

	if (!isset ($suggest_queries)) {
		require_once dirname(__FILE__) . "/../config.php";
	}

	if (!empty ($suggest_queries)) {
		$body = "<ul>";
		foreach ($suggest_queries as $key => $value) {
			$body .= "<li><a href=\"" . url . user_info('username', $page_owner) . "/$key/suggestions/\">" . $value['title'] . "</a></li>";
		}
		$body.="</ul>";
		$run_result .= "<li id=\"suggestion\">";
		$run_result .= templates_draw(array (
			'context' => 'sidebarholder',
			'title' => __gettext("Suggestions"), 
      'body' => $body));
		$run_result .= "</li>";

	} else {
		$run_result .= "";
	}

}
?>
