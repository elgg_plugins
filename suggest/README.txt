--------------------------------------------------------------
| SUGGEST MODULE: Configuration options and Extension points |
--------------------------------------------------------------

Configuration Options
=====================

SUGGEST_CACHE_TIME (default value: 3600)
  To configure the cache time.
SUGGEST_SUGGESTIONS (default value: 3)
	To configure how many items would be suggested
SUGGEST_FILTER
	To configure if you want to filter the suggestion from the tracking information
SUGGEST_WIDGET_ICON_SIZE (default value: 40)
	To configure the icon size for the widgets and keyword display
SUGGEST_SUBMENU_CONTRIBUTIONS (default value: true)
	To enable/disable the menu contributions (see config.php for more detail)
SUGGEST_SUGGESTIONS_CONTRIB (default value: 10)
	to configure how many items would be suggested in the contribution pages
SUGGEST_CONTRIB_ICON_SIZE (default value: 100)
	To configure the icon size for the contributions display
 
Extension Points
================

You can extend this plugin to suggest any kind of content managed by Elgg. 
To do that, you must to add the configuration in the 'config.php' file.
