<?php
/*
	Sven Edge, Jan 2007, GPL 2.
	
	plugin to clear out the cache directory based on modifcation date.
	
	based on the assumption that the cache dir is purely for temp data.
	depends on find and rm commands, so no Windows.
	
	e.g.
	$CFG->cachecleaner_maxage_days = 28;
	
*/
	function cachecleaner_pagesetup() {
		
	} // end function cachecleaner_pagesetup
	
	function cachecleaner_init() {
		
	} // end function cachecleaner_init
	
	// called by cron.php
	function cachecleaner_cron() {
		global $CFG;
		
		if (isset($CFG->cachecleaner_maxage_days)) {
			$CFG->cachecleaner_maxage_days = (int) $CFG->cachecleaner_maxage_days;
		}
		
		if (!empty($CFG->cachecleaner_maxage_days)) {
			
			if (!empty($CFG->cachecleaner_lastcron) && (time() - (60*60*24)) < $CFG->cachecleaner_lastcron) {
				return true;
			} else {
				
				$cachedir = $CFG->dataroot . 'cache/';
				if (is_dir($cachedir)) {
					chdir($cachedir);
					
					$find = '/usr/bin/find';
					$rm = '/bin/rm';
					
					if (is_executable($find) && is_executable($rm)) {
						
						$cmd = $find . ' . -mtime +' . $CFG->cachecleaner_maxage_days . ' -type f -exec ' . $rm . ' -v {} \;';
						$output = shell_exec($cmd);
						mtrace($output);
					} else {
						mtrace('ERROR: Missing either ' . $find . ' or ' . $rm);
					}
					
				} else {
					mtrace('ERROR: Missing ' . $cachedir);
				}
				
			}
			
			set_config('cachecleaner_lastcron',time());
		}
		
	} // end function cachecleaner_cron
	
?>