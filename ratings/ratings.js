function ratings_showhide(oid)
{
	var e = document.getElementById(oid);
	if(e.style.display == 'none') {
		e.style.display = 'block';
	} else {
		e.style.display = 'none';
	}
}
function ratings_getAjaxObj()
{
	var xmlHttp;
	
	try
	{
    		// Firefox, Opera 8.0+, Safari
    		xmlHttp=new XMLHttpRequest();
    	}
  	catch (e)
    	{
    		// Internet Explorer
    		try
      		{
      			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      		}
    		catch (e)
      		{
      			try
        		{
        			xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        		}
      			catch (e)
        		{
				return false;
        		}
      		}
    	}

	return xmlHttp;
}

function ratings_alertContents()
{
	if (http_Request.readyState == 4) {
		if (http_Request.status == 200) {
			
			result = http_Request.responseXML;
			message = result.getElementsByTagName('message')[0];

			error = result.getElementsByTagName('error')[0];
			if (error.textContent == '0') {
				success = true;
			}

			if (success == true)
			{
				document.getElementById('ratings_ajaxmessages_' + objectid).innerHTML = '<a href="">' + message.textContent + '</a>'; 
				document.getElementById('ratings_link_' + objectid).innerHTML = '&nbsp;';
			}
			else
			{
				document.getElementById('ratings_ajaxmessages_' + objectid).innerHTML = message.textContent; 
			}
		} else {
			alert('There was a problem with the request.');
		}
	}
}

function ratings_sendcomment(url, formid, oid)
{
	var parameters;
	objectid = oid;

	http_Request = getAjaxObj();

	if (http_Request==false)
	{
		return false;
	}

	// Construct parameters
	frm = document.getElementById(formid);

	parameters = "";
	for(var i = 0;i < frm.elements.length;i++) 
	{ 
		element = frm.elements[i]; 

		element_value = '';
		add_element = false;
		if ((element.type=='radio') || (element.type=='checkbox'))
		{
			if (element.checked == true)
			{
				element_value = element.value;
				add_element = true;
			}
		}
		else
		{
			element_value = element.value;
			add_element = true;
		}

		if (add_element==true)
		{
			parameters = parameters + element.name +"=" + encodeURI( element_value ) + "&";
		}
	}

	// Post result
        http_Request.onreadystatechange = ratings_alertContents;
	http_Request.open('POST', url, true);
	http_Request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http_Request.setRequestHeader("Content-length", parameters.length);
	http_Request.setRequestHeader("Connection", "close");
	http_Request.send(parameters);

}