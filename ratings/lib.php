<?php
	/*
    	 Ratings plugin for Elgg
    	 Initial author: Marcus Povey <marcus@dushka.co.uk>
	*/

	function ratings_init()
	{
		global $CFG, $db, $metatags, $function, $template;

		// Register javascript
		$metatags .= "<script type=\"text/javascript\" src=\"{$CFG->wwwroot}mod/ratings/ratings.js\"><!-- ratings js --></script>";

		// Set up the database
		$tables = $db->Metatables();
		if ((!in_array($CFG->prefix . "ratings", $tables)) || (!in_array($CFG->prefix . "ratingsdetails", $tables)))
		{
        		if (file_exists($CFG->dirroot . "mod/ratings/$CFG->dbtype.sql")) 
			{
            			modify_database($CFG->dirroot . "mod/ratings/$CFG->dbtype.sql");
        		} 
			else 
			{
            			error("Error: Your database ($CFG->dbtype) is not yet fully supported by the Elgg ratings.  See the mod/ratings directory.");
        		}
        
			print_continue("index.php");
			exit;
		}

		// If rating type not set then set a default rating TODO: Do this better
		if ((!isset($CFG->ratings)) || (!is_array($CFG->ratings)))
			$CFG->ratings = array();

		if (!isset($CFG->ratings['default_type']))
			$CFG->ratings['default_type'] = 'default';

		// Add annotation support
		display_set_display_annotation_function("file::file", "ratings_displayobjectannotations");
		display_set_display_annotation_function("weblog_post::post", "ratings_displayobjectannotations");
		display_set_display_annotation_function("mediastream::media", "ratings_displayobjectannotations"); 


		// Now add hooks for various publish events
		listen_for_event("file","publish","ratings_newobjectpublish"); 

		listen_for_event("weblog_post","publish","ratings_newobjectpublish"); 
			$function['weblog:post:links'][] = $CFG->dirroot . "mod/ratings/lib/weblog_post_links.php"; // Add ratings link to blog entry

		listen_for_event("mediastream", "publish", "ratings_newobjectpublish"); // Listen to media stream events
		
		// Template additions
		$template['css'] .= file_get_contents($CFG->dirroot . "mod/ratings/css");
	}


	function ratings_pagesetup()
	{
	}

	/** Enable ratings for a given object */
	function ratings_enable($object_id, $object_type, $rating_type, $owner)
	{
		$dataobject = new stdClass;
			$dataobject->object_id = $object_id;
			$dataobject->object_type = $object_type;
			$dataobject->owner = $owner;
			$dataobject->ratingtype = $rating_type;

		return @insert_record('ratings', $dataobject, false);
	}

	/** Store the rating a given user gives a rating object. Returns true or false */
	function ratings_storerating($ident, $userid, $rating)
	{
		$dataobject = new stdClass;
			$dataobject->rating_ident = $ident;
			$dataobject->ratedby = $userid;
			$dataobject->rating = $rating;
			$dataobject->ts = time();

		return @insert_record('ratingsdetails', $dataobject, false);
	}

	/** Hook listening for a publish event for an object and creating the appropriate entry in the ratings db. */
	function ratings_newobjectpublish($object_type, $event, $object)
	{
		global $CFG;

		if ($event == "publish")
		{
			if ($object_type=="file")
				ratings_enable($object->ident, 'file::file', $CFG->ratings['default_type'], $object->owner);

			if ($object_type=="weblog_post")
				ratings_enable($object->ident, 'weblog_post::post', $CFG->ratings['default_type'], $object->owner);
				
			if ($object_type=="mediastream")
				ratings_enable($object->ident, "mediastream::media", $CFG->ratings['default_type'], $object->owner);
		}

		return $object;
	}

	/** Draw ratings values out in a specific way */
	function ratings_drawrating($dbrow)
	{
		global $CFG;

		$annotation = "";

		if ($dbrow->ratingtype=='125') 
		{
			// One to 5 rating based numbers of people rated
			$result = get_record_sql("select ident, sum(rating) as rating, count(distinct ratedby) as ratedby from {$CFG->prefix}ratingsdetails where rating_ident={$dbrow->ident} group by rating_ident");

			if ($result)
			{
				$sum = $result->rating;
				$outof = (5 * (int)$result->ratedby);
				$outoffive = (($sum / $outof) / 2) * 10;
				
				$annotation .= sprintf(__gettext("%d users gave this an average rating %d/5"), $result->ratedby, $outoffive);
			}
			else
			{
				$annotation .= __gettext("Be the first to rate this!");
			}

		}
		else
		{
			// Default rating (dig it): X number of people have dug this
			$result = get_records('ratingsdetails','rating_ident', $dbrow->ident);

			$num = 0;
			if ($result) 
				$num = count($result);
			
			if ((!$result) || ($num != 1))
				$annotation .= $num . " " . __gettext("users rated this!");
			else
				$annotation .= $num . " " . __gettext("user rated this!");
		}

		return $annotation;
	}

	/** HACK: Output the given code as document.write */
	function ratings_todocwrite($text)
	{
		$body = "";
		foreach (explode("\n",addslashes($text)) as $line)
			$body .= "document.write(\"" . trim($line) . "\");\n";

		return $body;
	}

	/** Draw ratings form values out in a specific way */
	function ratings_drawratingform($dbrow, $ratedby)
	{
		global $CFG;

		$annotation = "";

		$object_id = $dbrow->object_id;
		$object_type = $dbrow->object_type;
		$ident = $dbrow->ident;
		$ratingtype = $dbrow->ratingtype;
		$returnurl = urlencode($_SERVER['REQUEST_URI']);

		// Common form elements
		$frm_elements_common = <<< END
			<input type="hidden" name="rating_ident" value="$ident" />
			<input type="hidden" name="action" value="rating::add" />
			<input type="hidden" name="object_id" value="{$object_id}" />
			<input type="hidden" name="object_type" value="{$object_type}" />
			<input type="hidden" name="ratedby" value="{$ratedby}" />
			<input type="hidden" name="ratingtype" value="$ratingtype" />		
END;
		
		if ($dbrow->ratingtype=='125') 
		{
			// One to 5 rating
			$buttontxt = __gettext("Rate!");
			$rate = <<< END
				1 <input type="radio" name="rating" value="1" /> 
				<input type="radio" name="rating" value="2" /> 
				<input type="radio" name="rating" value="3" /> 
				<input type="radio" name="rating" value="4" /> 
				<input type="radio" name="rating" value="5" /> 5
END;

			$ajaxy = <<< END
				<div id="ratings_$object_id">
					<div id="ratings_ajaxmessages_$object_id"></div>
					<form id="ratingsfrm_$object_id">
						$frm_elements_common
						<span id="ratings_link_$object_id"><p>
							$rate
						<span style="cursor:hand; cursor:pointer" onclick="ratings_sendcomment('{$CFG->wwwroot}mod/ratings/rating_action.php','ratingsfrm_$object_id', $object_id)"> $buttontxt</span></p></span>

						<input type="hidden" name="displaymode" value="xml" />
					</form>	
					
				</div>
END;
			$ajaxy = ratings_todocwrite($ajaxy);

			$annotation .= <<< END

				<script type="text/javascript">
				<!--
					$ajaxy
				-->
				</script>
				<noscript>
					<form id="ratingsfrm_$object_id" action="{$CFG->wwwroot}mod/ratings/rating_action.php" action="POST">
						$frm_elements_common

						<p>
							$rate
							<input type="hidden" name="returnurl" value="$returnurl" />
							<input type="submit" name="$buttontxt" value="$buttontxt" />
						</p>
					</form>	
				</noscript>
END;
		}
		else
		{
			// Default rating (dig it)
			$buttontxt = __gettext("Rate this..");
			$ajaxy = <<< END
				<div id="ratings_$object_id">
					<div id="ratings_ajaxmessages_$object_id"></div>
					<span id="ratings_link_$object_id" style="cursor:hand; cursor:pointer" onclick="ratings_sendcomment('{$CFG->wwwroot}mod/ratings/rating_action.php','ratingsfrm_$object_id', $object_id)">$buttontxt</span>
					<form id="ratingsfrm_$object_id">
						$frm_elements_common
						<input type="hidden" name="rating" value="1" />
						<input type="hidden" name="displaymode" value="xml" />
					</form>	
				</div>
END;
			$ajaxy = ratings_todocwrite($ajaxy);

			$annotation .= <<< END

				<script type="text/javascript">
				<!--
					$ajaxy
				-->
				</script>
				<noscript>
					<form id="ratingsfrm_$object_id" action="{$CFG->wwwroot}mod/ratings/rating_action.php" action="POST">
						$frm_elements_common
						<input type="hidden" name="rating" value="1" />
						<input type="hidden" name="returnurl" value="$returnurl" />
						<input type="submit" name="$buttontxt" value="$buttontxt" />
					</form>	
				</noscript>
END;
		}

		return $annotation;
	}


	/** Have we rated this object before */
	function ratings_ratedbefore($object_rated, $ratinguserid)
	{
		$result = get_record('ratingsdetails', 'rating_ident', $object_rated->ident, 'ratedby', $ratinguserid);
		if ($result) return true; 

		return false;
	}

	/** Display rating & allow ratethis - draw form/alreadyrated */
	function ratings_displayobjectannotations($object, $object_type, $view)
	{
		global $CFG, $db;

		$object_id = $object->ident;
		$annotation = "";

		// Pull ratings details from db
		$result = get_record('ratings','object_id', $object_id, 'object_type', $object_type);

		if ($result) 
		{
			$annotation .= "<div id=\"ratings\">";

			// Add rating
			$annotation .= ratings_drawrating($result);

			// See if we have already rated this before
			if (isLoggedin()) 
			{
				if (!ratings_ratedbefore($result, $_SESSION['userid'])) 
				{

					// Not rated before
					$annotation .= ratings_drawratingform($result, $_SESSION['userid']);
				}
			}

			$annotation .= "</div>";
		}


		// Return the annotation
		return $annotation;
	}

?>