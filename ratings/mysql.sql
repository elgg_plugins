-- Ratings table
-- This stores the ratings for an object
-- @author Marcus Povey <marcus@dushka.co.uk>

CREATE TABLE `prefix_ratings` (

  ident int(11) NOT NULL auto_increment COMMENT '-> rating number',
  object_id int(11) NOT NULL COMMENT '-> ident of object being rated',
  object_type varchar(128) NOT NULL COMMENT '-> type of object being rated',
  owner int(11) NOT NULL COMMENT '-> ident of user rating the object',

  ratingtype varchar(128) NOT NULL COMMENT '-> The type of rating it is eg 1-5, button only etc',

  PRIMARY KEY (ident),
  KEY object_id (object_id),
  KEY object_type (object_type),
  KEY owner (owner)
) ENGINE=MyISAM;


CREATE TABLE `prefix_ratingsdetails` (
	
	ident int(11) NOT NULL auto_increment COMMENT '-> record ident',

	rating_ident int(11) NOT NULL COMMENT '-> rating number',

	ratedby int(11) NOT NULL COMMENT '-> Who rated this',
	rating int(3) NOT NULL default 0 COMMENT '-> Rating given',
	ts int(11) NOT NULL COMMENT '-> Time rated',

	UNIQUE KEY (rating_ident, ratedby),
	PRIMARY KEY (ident)
) ENGINE=MyISAM;
