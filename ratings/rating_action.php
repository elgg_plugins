<?php

	/** @file rating_action.php Handle ratings action.
	 * This file handles a rating action based on input
	 */

	@require_once("../../includes.php");

	global $messages, $CFG;

	// Get parameters
	$action = optional_param('action',''); // Action - rate etc
	$rating_ident = optional_param('rating_ident',''); // Ident of the action field
	$ratedby = optional_param('ratedby',''); // User rating this object
	$rating = optional_param('rating',''); // Rating they gave it
	$displaymode = optional_param('displaymode',''); // Display XML or normal (default)
	$returnurl = urldecode(optional_param('returnurl','')); // The return url
	$object_id = optional_param('object_id','');
	$object_type = optional_param('object_type','');
	$userid = $ratedby;

	$page = "";
	
	if ($action) {

		if ($action == "rating::add")
		{
			// Store the rating
			$success = ratings_storerating($rating_ident, $ratedby, $rating);

			// Message
			if ($success) 
			{
				$messages[] = __gettext("Object rated.");  
				if ($displaymode=="xml") 
					$messages[] = __gettext(" Click here to see the updated rating.");

				// Hook for the river plugin (if installed)
				if (function_exists('river_save_event'))
				{
					$username = "<a href=\"" . river_get_userurl($userid) . "\">" . user_info("username", $userid) . "</a>";

					river_save_event($userid, $object_id, $userid, $object_type, $username . " " . __gettext("rated") . " " . river_get_friendly_id($object_type, $object_id) );
					
				}
			}
			else
			{
				$messages[] = __gettext("Object could not be rated");
			}
	
			// Are we outputing XML or text
			if ($displaymode=="xml")
			{
				$message = implode(" \n", $messages);
				if ($success) 
					$err = "0";
				else
					$err = "1";	

				$page = "<ajax>\n<message>$message</message>\n<error>$err</error>\n</ajax>\n";
			}
			else
			{
				$page = <<< END
						<p>Your rating has been recorded.</p>
						<a href="$returnurl">Go back...</a>
END;

				templates_page_setup();    

				$page = templates_page_draw( 
						array( sitename,
							templates_draw(
								array(
									'body' => $page,
									'title' => __gettext("Rating"),
									'context' => 'contentholder'
								)
							)
						)
					);
			}

		}
	}

	// Output the page
	if ($displaymode=="xml")
		header("Content-type: text/xml");
	echo $page;

?>