<?php

    global $page_owner;
    global $PAGE;
    //global $run_result;
    global $CFG;

    if (logged_on) {
        
        $forwarder = user_flag_get('forwarder', $page_owner);
        if (!$forwarder) {
            $forwarder = "profile";
        }
        
        $pulldown = '<select name="default_page">';
        
        foreach($PAGE->menu as $menu) {
            if ($menu['name'] == $forwarder) {
                $selected = 'selected = "selected"';
            } else {
                $selected = "";
            }
            $pulldown .= '<option value="' . $menu['name'] . '" ' . $selected . '>' . $menu['name'] . '</option>';
        }
        $pulldown .= "</select>";
        
        $run_result .= "<h2>" . __gettext("Change your default page") . "</h2>";
        $run_result .= "<p>" . __gettext("This is the page visitors see when they click on your icon.") . "</p>";
        
        $run_result .= templates_draw(array(
            'context' => 'databox',
            'name' => __gettext("Your default page: "),
            'column1' => $pulldown
        )
        );
        
        $forwarder = user_flag_get('friendlyname', $page_owner);
        if ($forwarder) {
            $forwarder = htmlspecialchars(stripslashes($forwarder), ENT_COMPAT, 'utf-8');
        } else {
            $forwarder = '';
        }
        
        $run_result .= "<h2>" . __gettext("Change your friendly name") . "</h2>";
        $run_result .= "<p>" . sprintf(__gettext("Once set, you can access your account from <i>%shome/[your&nbsp;friendly&nbsp;name]/</i>"),$CFG->wwwroot) . "</p>";
        
        $run_result .= templates_draw(array(
            'context' => 'databox',
            'name' => __gettext("Your friendly name: "),
            'column1' => "<input name=\"friendly_name\" type=\"text\" value=\"$forwarder\" />"
        )
        );
        
    }

?>