<?php

    require("../../includes.php");
    
    global $page_owner;
    global $CFG, $db;
    $profile_name = optional_param('profile_name', '-1', PARAM_ALPHANUM);   
    $sql = "select * from ".$CFG->prefix."user_flags where flag = 'friendlyname' and value = " . $db->qstr($profile_name);

    if ($page_owner = get_record_sql($sql)) {
        $page_owner = $page_owner->user_id;
    } else {
        header("Location: $CFG->wwwroot");
        exit;
    }
    
    $username = user_info("username",$page_owner);
    $forwarder = user_flag_get('forwarder', $page_owner);
    
    if ($forwarder) {
        header("Location: ".$CFG->wwwroot . $username . "/" . $forwarder . "/");
    } else {
        header("Location: ".$CFG->wwwroot."$username/profile/");
    }
    
?>