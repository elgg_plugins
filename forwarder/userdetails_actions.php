<?php

    $id = optional_param('id',0,PARAM_INT);
    $action = optional_param('action');
    
    global $messages;
    global $CFG;
    
    if (logged_on && !empty($action) && run("permissions:check", array("userdetails:change",$id))) {
        
        if ($action == "userdetails:update") {
            
            $default_page = optional_param('default_page');
            if ($usertype == 'person' && isset($default_page)) {
                if (!empty($default_page)) {
                    user_flag_set("forwarder", $default_page, $id);
                    $messages[] = __gettext("Default page set to '$default_page'.");
                } else {
                    user_flag_unset("forwarder", $id);
                    $messages[] = __gettext("Default page set to 'profile'.");
                }
            }
            
            $friendly_name = optional_param('friendly_name');
            if (!empty($friendly_name)) {
                
                user_flag_unset("friendlyname", $id);
                $names = get_records('user_flags','flag','friendlyname','value',$friendly_name);
                if (preg_match("/^[A-Za-z0-9]{3,12}$/",$friendly_name)) {
                    if (empty($names)) {
                        user_flag_set("friendlyname", $friendly_name, $id);
                        $messages[] = sprintf(__gettext("Your friendly URL was set to %s"),$CFG->wwwroot . "home/" . $friendly_name . "/");
                    } else {
                        $messages[] = __gettext("That friendly name was taken. Please try another.");
                    }
                } else {
                    $messages[] = __gettext("Error! Your friendly name must contain letters and numbers only, cannot be blank, and must be between 3 and 12 characters in length.");
                }                
            }
            
        }
        
    }

?>