<?php

    require("../../includes.php");
    
    global $page_owner;
    global $CFG;
    global $messages;
    
    $profile_name = optional_param('profile_name', '-1', PARAM_ALPHANUM);   
    if (!empty($profile_name)) {
        $page_owner = user_info_username('ident',$profile_name);
    }
    if (empty($page_owner)) {
        $page_owner = optional_param('profile_id', $page_owner, PARAM_INT);
    }
    
    $username = user_info("username",$page_owner);
    $forwarder = user_flag_get('forwarder', $page_owner);

    $_SESSION['messages'] = $messages;
    
    if ($forwarder) {
        header("Location: ".$CFG->wwwroot . $username . "/" . $forwarder . "/");
    } elseif ($username) {
        header("Location: ".$CFG->wwwroot . $username . "/profile/");
    } else {
        header("Location: ".$CFG->wwwroot);
    }
    
?>