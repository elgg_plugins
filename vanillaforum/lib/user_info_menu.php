<?php
/*
 * Script that shows the Forum links in the sidebar
 * Created on May 9, 2007
 *
 * @author Diego Andr�s Ram�rez Arag�n <diego@somosmas.org>
 * @copyright Diego Andr�s Ram�rez Arag�n - 2007
 */
global $page_owner;
global $CFG;

if (user_type($page_owner) == "community") {
$title = __gettext("Forum");

$body="<ul>";
if(logged_on || (isset($page_owner) && $page_owner != -1)){
  
    $communityWeblog = __gettext("Community Forum");
    $vanilla_url = $CFG->wwwroot."mod/vanillaforum/vanilla/?CategoryID=$page_owner";
    $body .="<li><a href=\"$vanilla_url\">$communityWeblog</a> (<a href=\"".$vanilla_url."&Feed=RSS2\">RSS</a>)</li>";
  $run_result .= "<li id=\"sidebar_weblog\">";
  $run_result .= templates_draw(array('context' => 'sidebarholder',
                                   'title' => $title,
                                   'body' => $body)
                              );
  $run_result .= "</ul></li>";
  }
}

?>
