<?php
/*
 * Created on May 9, 2007
 *
 * @author Diego Andr�s Ram�rez Arag�n <diego@somosmas.org>
 * @copyright Diego Andr�s Ram�rez Arag�n - 2007
 */

function vanillaforum_pagesetup() {
  global $PAGE, $CFG, $messages;
  $vanilla_url = $CFG->wwwroot."mod/vanillaforum/vanilla/";

  // Checking if the database config its vanilla compatible
  if ($CFG->dbtype != "mysql") {
    $messages[] = __gettext("Your database engine its not compatible with vanilla.<br> Vanilla forum supports only mysql");
  } else {
    if (isloggedin()) {
      if (defined("context") && context == "forum") {
        $PAGE->menu[] = array (
          'name' => 'forum',
          'html' => "<li><a href=\"" . $vanilla_url . " class=\"selected\" >" . __gettext("Forums"
        ) . '</a></li>');
      } else {
        $PAGE->menu[] = array (
          'name' => 'forum',
          'html' => "<li><a href=\"" . $vanilla_url . "\" >" . __gettext("Forums"
        ) . '</a></li>');
      }
    }
  }
}

function vanillaforum_init() {
  global $CFG, $function, $db, $METATABLES;
  if (!get_config('vanilla')) {
    if (file_exists(dirname(__FILE__)."/$CFG->dbtype.sql")) {
      // Vanilla forum tables
      modify_database(dirname(__FILE__)."/vanilla/setup/mysql.sql");
      // Elgg related tables updates
      modify_database(dirname(__FILE__)."/$CFG->dbtype.sql");
      // Default template
      execute_sql("INSERT INTO LUM_Style Values(1,0,'default',".$db->qstr($CFG->wwwroot."mod/vanillaforum/vanilla/themes/vanilla/styles/default/").",'preview.gif')");
      // Creating vanilla categories for existing communities
      $communities = get_records('users','user_type','community');
      foreach($communities as $community){
        $forum_desc = sprintf(__gettext("'%s' discussion forum"), $community->name);
        $forum_name = sprintf(__gettext("'%s' discussions"), $community->name);
        $query = "INSERT INTO LUM_Category (CategoryID,Name,Description) VALUES (" . $db->qstr($community->ident) . "," . $db->qstr($forum_name) . "," . $db->qstr($forum_desc) . ")";
        execute_sql($query);
        $query = "INSERT INTO LUM_CategoryRoleBlock (CategoryID,RoleID,Blocked) VALUES ($community->ident,1,1)";
        execute_sql($query);
        $query = "INSERT INTO LUM_CategoryRoleBlock (CategoryID,RoleID,Blocked) VALUES ($community->ident,2,1)";
        execute_sql($query);
      }
    } else {
      error("Error: Your database ($CFG->dbtype) is not yet fully supported by the Elgg messages plug-in.  See the mod/messages directory.");
    }
    set_config('vanilla',time());
    print_continue("");
    exit;
  }
  // Setting sidebar
  $function['display:sidebar'][] = dirname(__FILE__) . "/lib/user_info_menu.php";

  // Listen for community creation
  listen_for_event("community", "publish", "vanilla_forum_sync");
  listen_for_event("user", "delete", "vanilla_forum_sync");
  
}

/**
 * Function that handles the the syncronization with the Vanilla forum database
 * @param string $object_type
 * @param string $event
 * @param mixed $object
 * @return mixed
 */
function vanilla_forum_sync($object_type, $event, $object) {
  global $db, $messages;

  // If a community has been created create a new category in vanilla
  if ($object_type == "community" && $event == "publish") {
    $forum_desc = sprintf(__gettext("'%s' discussion forum"), $object->name);
    $forum_name = sprintf(__gettext("'%s' discussions"), $object->name);
    $query = "SELECT CategoryID from LUM_Category where CategoryID = ? ";
    $vanilla_category = get_field_sql($query, array ($object->ident));
    if (!$vanilla_category) {
      $query = "INSERT INTO LUM_Category (CategoryID,Name,Description) VALUES (" . $db->qstr($object->ident) . "," . $db->qstr($forum_name) . "," . $db->qstr($forum_desc) . ")";
      if (!execute_sql($query, false)) {
        $messages[] = __gettext("Error creating the community forum");
        return $object;
      }
      $vanilla_category = $object->ident;
    }
    $query = "INSERT INTO LUM_CategoryRoleBlock (CategoryID,RoleID,Blocked) VALUES ($vanilla_category,1,1)";
    execute_sql($query, false);
    $query = "INSERT INTO LUM_CategoryRoleBlock (CategoryID,RoleID,Blocked) VALUES ($vanilla_category,2,1)";
    execute_sql($query, false);
  }

  if ($object_type == "user" && $event == "delete") {
    if ($object->user_type == "community") {
      // Dropping vanilla category
      $query = "SELECT CategoryID FROM LUM_Category WHERE CategoryID=?";
      $vanilla_category = get_field_sql($query, array ($object->ident));
      if ($vanilla_category) {
        $query = "UPDATE LUM_Discussion SET CategoryID = 1 WHERE CategoryID = " . $db->qstr($vanilla_category);
        if (execute_sql($query, false)) {
          execute_sql("DELETE FROM LUM_Category WHERE CategoryID=" . $db->qstr($vanilla_category), false);
          execute_sql("DELETE FROM LUM_CategoryRoleBlock WHERE CategoryID=" . $db->qstr($vanilla_category), false);
          $messages[] = sprintf(__gettext("Forums discussions related with the group '%s' were returned to the 'General' forum"), $object->name);
        } else {
          $messages[] = __gettext("Error updating the discussion information");
        }
      } else {
        $messages[] = sprintf(__gettext("Coudn't find any forum related with the %s community"), $object->name);
      }
      // Delete community owner from vanilla
    }
  }
  return $object;
}
?>
