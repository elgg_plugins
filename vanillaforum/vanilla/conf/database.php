<?php
require_once (dirname(__FILE__) ."/../../../../config.php");

// Database Configuration Settings
$Configuration['DATABASE_HOST'] = $CFG->dbhost;
$Configuration['DATABASE_NAME'] = $CFG->dbname;
$Configuration['DATABASE_USER'] = $CFG->dbuser;
$Configuration['DATABASE_PASSWORD'] = $CFG->dbpass;
$Configuration['DATABASE_CHARACTER_ENCODING'] = 'utf8';

$DatabaseTables['User'] = $CFG->prefix.'users';
$DatabaseColumns['User']['UserID'] = 'ident';
$DatabaseColumns['User']['code'] = 'code';
$DatabaseColumns['User']['Name'] = 'username';
$DatabaseColumns['User']['Password'] = 'password';
$DatabaseColumns['User']['Email'] = 'email';
?>