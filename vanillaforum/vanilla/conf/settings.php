<?php
require_once (dirname(__FILE__) ."/../../../../config.php");

$web_root = substr($_SERVER['PHP_SELF'],0,strrpos($_SERVER['PHP_SELF'],"/")+1);

// Application Settings
$Configuration['SETUP_TEST'] = '1'; 
$Configuration['APPLICATION_PATH'] = dirname(__FILE__).'/../'; 
$Configuration['DATABASE_PATH'] = dirname(__FILE__).'/database.php'; 
$Configuration['LIBRARY_PATH'] = dirname(__FILE__).'/../library/'; 
$Configuration['EXTENSIONS_PATH'] = dirname(__FILE__).'/../extensions/'; 
$Configuration['LANGUAGES_PATH'] = dirname(__FILE__).'/../languages/'; 
$Configuration['THEME_PATH'] = dirname(__FILE__).'/../themes/vanilla/'; 
$Configuration['DEFAULT_STYLE'] = $CFG->wwwroot.'mod/vanillaforum/vanilla/themes/vanilla/styles/default/'; 
$Configuration['WEB_ROOT'] = $web_root; 
$Configuration['BASE_URL'] = $CFG->wwwroot.'mod/vanillaforum/vanilla/'; 
$Configuration['FORWARD_VALIDATED_USER_URL'] = $CFG->wwwroot.'mod/vanillaforum/vanilla/'; 
$Configuration['SUPPORT_EMAIL'] = 'lowfill@gmail.com'; 
$Configuration['SUPPORT_NAME'] = 'News'; 
$Configuration['APPLICATION_TITLE'] = 'Elgg'; 
$Configuration['BANNER_TITLE'] = 'Elgg'; 
$Configuration['COOKIE_DOMAIN'] = 'zeitgeist.homelinux.org'; 
$Configuration['COOKIE_PATH'] = $web_root; 
$Configuration['SETUP_COMPLETE'] = '1'; 

$Configuration['LAST_UPDATE'] = '1179112327'; 
$Configuration['ADDON_NOTICE'] = '0';

$Configuration['AUTHENTICATION_MODULE'] = 'People/People.Class.ElggAuthenticator.php';
$Configuration['USER_MODULE'] = 'People/People.Class.ElggUser.php';
?>