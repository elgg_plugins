-------------------------------------------------------------------
| VANILLAFORUM MODULE: Configuration options and Extension points |
-------------------------------------------------------------------

This plugin provides basic integration for the vanilla forum software 
(http://getvanilla.com)

It provides:
  - Elgg authentications through cookies
  - Community to Vanilla forum categories syncronization
  - A preconfigured vanilla instalation forum.
  
By default it set the 'Manager' role for new news user and 'Member' 
role for normal elgg users. 

If you want a more deep elgg integration you must to implement your
own vanilla theme.

If you want to authenticate directly to vanilla you must to move the
file 'vanillaforum/vanilla/people.php.old' to 'vanillaforum/vanilla/people.php' 
