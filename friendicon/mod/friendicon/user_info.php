<?php

global $profile_id, $page_owner;
global $CFG, $USER;

// Given a title and series of user IDs as a parameter, will display a box containing the icons and names of each specified user
// $parameter[0] is the title of the box; $parameter[1..n] is the user ID

if (isset($parameter[0]) && sizeof($parameter) > 1) {

    $name = $parameter[0];

    $body  = "";
    $body .= "<table>\n\t<tr>\n";
    $cellnum = -1;

    foreach ($parameter[1] as $key => $ident) {

        $ident = (int) $ident;
        $info = get_record('users','ident',$ident);
        $_SESSION['user_info_cache'][$ident] = $info;
        $info = $info;

        $info = $_SESSION['user_info_cache'][$ident];
        $username = run("profile:display:name", $info->ident);
        $usermenu = '';

        $cellnum++;
        if ($cellnum % 2 == 0 && $cellnum > 0) {
            $body .= "</tr><tr>";
        }

        if ($icon = user_flag_get('friendicon_sidebar', $USER->ident)) {
            $body .= <<< END
        <td>
        <a href="{$CFG->wwwroot}{$info->username}/">
            <img src="{$CFG->wwwroot}_icon/user/{$info->icon}/w/50" alt="{$username}" />
        </a>
        <br/>
        <a href="{$CFG->wwwroot}{$info->username}/">{$username}</a>
        </td>
END;
        } else {
            // No friends
        }        
    }

    $body .= "\t</tr>\n</table>\n";
    
//     if ($page_owner != $_SESSION['userid']) {
//         $body .= "<a href=\"".url."_friends/?owner=$profile_id\">[" . __gettext("View all Friends") . "]</a>";
//     } else {
//         $body .= "<a href=\"".url.$_SESSION['username']."/friends/\">[" . __gettext("View all Friends") . "]</a>";
//     }
    if (isset($parameter[2]) && $parameter[2] != "") {
        $body .= "<li><p>" . $parameter[2] . "</p></li>";
    }

    $run_result .= templates_draw(array(
                                        'context' => 'sidebarholder',
                                        'title' => $name,
                                        'body' => $body
                                        )
                                  );
    
}

?>
