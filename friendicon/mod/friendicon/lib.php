<?php

    function friendicon_init() {
        global $CFG, $function;

        $function['userdetails:init'][] = $CFG->dirroot . 'mod/friendicon/friendicon_userdetails_actions.php';
        $function['userdetails:edit:details'][] = $CFG->dirroot . 'mod/friendicon/friendicon_userdetails_edit_details.php';
    }

    function friendicon_pagesetup() {
        global $CFG, $USER, $function, $page_owner;

        // Replace the displaying of the friends and add the image element
        if (logged_on) {
            $setting = user_flag_get('friendicon_sidebar', $page_owner);
            if ($setting == 'true') {
                $i = 0;
                foreach ($function['users:infobox'] as $element) {
                    if ($element == $CFG->dirroot . 'units/users/user_info.php') {
                        unset($function['users:infobox'][$i]);
                        $function['users:infobox'][$i] = $CFG->dirroot . 'mod/friendicon/user_info.php';
                    }
                    
                    $i++;
                }
                
                // Replace community membership sidebar view
                $i = 0;
                foreach ($function['display:sidebar'] as $element) {


                    if ($element == $CFG->dirroot . 'units/communities/community_memberships.php') {
                        unset($function['display:sidebar'][$i]);

                        $function['display:sidebar'][$i] = $CFG->dirroot . 'mod/friendicon/community_memberships.php';
                    }

                    $i++;
                }

                // We've mangled the array, tidy up
                ksort($function['display:sidebar']);
            }
        }
    }
    
    function friendicon_userdetails_edit_details($userid) {
        $title = __gettext("Sidebar friend icon:");
        $blurb = __gettext("Choose if you would like your friends' user icons displayed in your sidebar friends list.");

        $body = <<< END
            <h2>$title</h2>
            <p>$blurb</p>
END;

        $true = __gettext('Yes');
        $false = __gettext('No');

        $checked_true = '';
        $checked_false = '';

        $setting = user_flag_get('friendicon_sidebar', $userid);
                
        if (!empty($setting)) {
            if ($setting == 'true') {
                $checked_true = 'checked="checked"';
            } elseif ($setting == 'false') {
                $checked_false = 'checked="checked"';
            }
        } else {
            // Default
            $checked_false = 'checked="checked"';
        }

        $chooser = <<< END
            <input type="radio" name="friendicon_sidebar" value="true" {$checked_true}/>&nbsp;{$true}&nbsp;
            <input type="radio" name="friendicon_sidebar" value="false" {$checked_false}/>&nbsp;{$false}
END;

        $body .= templates_draw( array(
                    'context' => 'databox',
                    'name' => __gettext("Sidebar user icon: "),
                    'column1' => $chooser
        )
        );
        
        return $body;
    }

    function friendicon_userdetails_actions() {
        
        global $messages;

        $action = optional_param('action');
        $id = optional_param('id',0,PARAM_INT);
        $friendicon_sidebar = optional_param('friendicon_sidebar','false');
        $friendicon_sidebar_db = user_flag_get('friendicon_sidebar', $id);

        if (logged_on && !empty($action) && run("permissions:check", array("userdetails:change",$id)) && $action == "userdetails:update") {
            if (!empty($friendicon_sidebar)) {
                if ($friendicon_sidebar != $friendicon_sidebar_db) {
                    if(user_flag_set('friendicon_sidebar', $friendicon_sidebar, $id)) {
                        $messages[] .= __gettext("Display of user icons") . " " . __gettext("saved") .".";
                    } else {
                        $messages[] .= __gettext("Display of user icons") . " " . __gettext("not saved") .".";
                    }
                }
            }
        }
    }
?>
