<?php

    function z_resource_home_init() {
    }

    function z_resource_home_pagesetup() {
        global $CFG, $PAGE, $profile_id;

        $page_owner = $profile_id;

        $selected = "";

        if (defined("context") && context == "resources" && $page_owner == $_SESSION['userid'])
        {
            $selected = "class=\"selected\"";
        }

        $resources_link = "<li><a href=\"{$CFG->wwwroot}{$_SESSION['username']}/feeds/all/\" {$selected} >".__gettext("Your Resources")."</a></li>";

        $i=0;

        while($i < count($PAGE->menu))
        {
            if ($PAGE->menu[$i]['name'] == "feeds")
            {
                $PAGE->menu[$i]['html'] = $resources_link;
            }

            $i++;
        }
    }

?>
