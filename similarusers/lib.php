<?php

    function similarusers_pagesetup() {
        // Dummy function
    }
    
    function similarusers_get_users($userid, $limit = 25) {
        global $CFG;
        return get_records_sql("select u.*, count(t.tag) as metric from {$CFG->prefix}tags t join {$CFG->prefix}users u on u.ident = t.owner where t.tagtype = \"interests\" and t.owner <> {$_SESSION['userid']} and tag in ( select tag from {$CFG->prefix}tags where tagtype = \"interests\" and owner = {$_SESSION['userid']} ) group by u.ident order by metric desc limit $limit");
    }

?>