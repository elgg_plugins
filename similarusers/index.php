<?php

    require("../../includes.php");
    
        global $CFG, $page_owner;
        $page_owner = page_owner();
        
        if ($page_owner == -1) {
            $page_owner = $_SESSION['userid'];
        }
        
        if ($page_owner == -1) {
            header("Location: " . $CFG->wwwroot);
            exit;
        }
    
        $name = user_info("name", $_SESSION['userid']);
        
        if ($page_owner == $_SESSION['userid']) {
            $body = "<p>" . sprintf(__gettext("The following users have interests in common with %s (with the number of interests in common listed):"),$name) . "</p>";
        }
        
        if ($similarusers = similarusers_get_users($page_owner,25)) {
            
            $row = 0;
            $column = 0;
            
            $body .= <<< END
            
<div class="networktable">
    <table>
        <tr>
END;
            
            foreach($similarusers as $similaruser) {
                $row++;
                $usermenu = run("users:infobox:menu",array($similaruser->ident));
                $similarmetric = sprintf(__gettext("%s interests in common"),$similaruser->metric);
                
                $body .= <<< END
                
                <td align="center" valign="top">
                    <a href="{$CFG->wwwroot}{$similaruser->username}"><img src="{$CFG->wwwroot}_icon/user/{$similaruser->icon}" border="0" /></a><br />
                    <a href="{$CFG->wwwroot}{$similaruser->username}">{$similaruser->name}</a><br />
                    {$similarmetric}
                </td>
                
END;
                
                if ($row == 5) {
                    $row = 0;
                    $column++;
                    $body .= "</tr><tr>";
                }
            }
            
            $body .= <<< END
        </tr>
    </table>
</div>
END;

        }
        
        templates_page_setup();
        
        $title = sprintf(__gettext("%s: similar users"), $name);
        
        $body  = templates_draw( array(
                                   'context' => 'contentholder',
                                   'title' => $title,
                                   'body' => $body
                                   ));
    
        echo templates_page_draw(array($title, $body));


?>