<?php
	/*
    	 Event log and river plugin for Elgg
    	 Initial author: Marcus Povey <marcus@dushka.co.uk>

	 Notes:
	 This is a little rough and ready. If you want to add events to the river you need to register
	 a handle and listen to various publish / delete /create events applicable to your module.

	 For compatibility I would suggest that you place the listen_for_event calls inside a function exists
	 test. This will allow handle the situation where the river plugin is not present.

	 eg. 

	 if (function_exists('river_save_event'))
	 {
		listen_for_event('file','publish', 'file_river_hook');
		listen_for_event('file','delete', 'file_river_hook');
	 }
	*/

	function river_init()
	{
		global $CFG, $db, $template;

		// Set up the database
		$tables = $db->Metatables();
		if (!in_array($CFG->prefix . "river", $tables))
		{
        		if (file_exists($CFG->dirroot . "mod/river/$CFG->dbtype.sql")) 
			{
            			modify_database($CFG->dirroot . "mod/river/$CFG->dbtype.sql");
        		} 
			else 
			{
            			error("Error: Your database ($CFG->dbtype) is not yet fully supported by the Elgg river.  See the mod/river directory.");
        		}
        
			print_continue("index.php");
			exit;
		}

		// Test for widget support
		if (!function_exists('widget_init'))
		{
			error("Widget support isn't installed. Please download and install the widgets module to mod/widget.");
			exit;
		}

		// Register widget
		$CFG->widgets->list[] = array(
			'name' => gettext("River widget"),
			'description' => gettext("Display a list of recent activity."),
			'type' => "river::river"
		);
		
		$template['css'] .= file_get_contents($CFG->dirroot . "mod/river/css.css");

	}

	function river_pagesetup()
	{
	}

	/** Save a river event to the database */
	function river_save_event($userid, $object_id, $object_owner, $object_type, $string, $access = "PUBLIC")
	{
		$entry = new stdClass;
			$entry->userid = $userid;
			$entry->object_id = $object_id;
			$entry->object_owner = $object_owner;
			$entry->object_type = $object_type;
			$entry->access = $access;
			$entry->ts = time();
			$entry->string = $string;

		return insert_record('river', $entry);
	}

	/** Get the display data */
	function river_get_river($userid, $limit = 10, $starttime = "", $endtime = "")
	{
		global $CFG;

		// Get mutual friend network

			// My friends 	
			$query = "SELECT friend from {$CFG->prefix}friends where owner=$userid";
//echo "$query\n";
			$myfriends = get_records_sql($query); 
		
			$list = "";
			foreach ($myfriends as $friend)
				$list .= $friend->friend . ",";
			$list = trim($list, " ,");

			// Get a subset who also have me as a friend 
			$query = "SELECT owner from {$CFG->prefix}friends where owner in ($list) and friend=$userid";
//echo "$query\n";
			$theirfriends = get_records_sql($query); 
		
			$list2 = "";
			foreach ($theirfriends as $friend)
				$list2 .= $friend->owner . ",";

			$list = "$list2";
				
			$list2 = trim($list2, " ,");
			$list .= $userid; // We also want to include myself ( to the first list not the second).

			if ($list2=="") return false;

			// Time range
			if ($starttime!="")
				$starttime = "and ts >= $starttime";
			
			if ($endtime!="")
				$endtime = "and ts <= $endtime";

			// Fix limit
			if ($limit == 0)
				$limit = "";
			else
				$limit = "limit $limit";
				
			// Do access
			$access = " and (access='PUBLIC' ";
			if (isloggedin()) $access .= " or access='LOGGED_IN' ";
			if ($userid == $_SESSION['userid'])  $access .= " or access='PRIVATE' ";
			$access .= ") ";

			$query = "select * from {$CFG->prefix}river where 1 $starttime $endtime $access and (userid in ($list) or object_owner in ($list2))  order by ts desc $limit";
//echo "$query\n";
			return get_records_sql($query);

	}

	/** Display the widget */
	function river_widget_display($widget)
	{
		global $CFG;

		// Pull number of river entries
		$river_num_display = widget_get_data("river_num_display",$widget->ident);
		if ($river_num_display == "") $river_num_display = 10;

		$userid = page_owner(); // user id is page owner
		$name = user_info("name", $userid);

		$river = river_get_river($userid, $river_num_display);

		$body = "";
		if ($river)
		{
			$d1 = "";
			$d2 = "";
			
			foreach ($river as $r)
			{
				$d2 = date("j-n-y", $r->ts); // Set second marker
				
				$type = str_replace(":", "_", $r->object_type);

				if (strcmp($d1,$d2)!=0) $body .= "<p class=\"widget_river_date\">". date("l jS F Y", $r->ts) . "</p>";
				$body .= "<div class=\"widget_river_type_$type\"><div class=\"widget_river_text\"> " . __gettext($r->string) . "<span class=\"widget_river_time\">" . date("g:ia", $r->ts) . "</span></div></div>";

				$d1 = date("j-n-y", $r->ts); // Now set first marker to date
			}
			
			$body .= "<div class=\"widget_river_rss\"><a href=\"{$CFG->wwwroot}mod/river/rss.php?owner=$userid\"><img src=\"{$CFG->wwwroot}mod/template/icons/rss.png\" border=\"0\" /></a></div>";
		}
		else
		{
			$body = __gettext("This user hasn't done anything yet.");
		}
        
		//$ttl = "<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><tr><td align=\"left\">".  $name . __gettext("'s River") . "</td><td align=\"right\"><a href=\"{$CFG->wwwroot}mod/river/rss.php?owner=" . page_owner() . "\"><img src=\"{$CFG->wwwroot}mod/template/icons/rss.png\" border=\"0\" /></a></td></tr></table>";
        
		return array('title'=> $name . __gettext("'s River"),'content'=>$body);	
		//return array('title'=> $ttl,'content'=>$body);
	}

	/** Edit the widget */
	function river_widget_edit($widget)
	{
		global $CFG;

		$num_display = widget_get_data("river_num_display", $widget->ident);
		if (!$num_display) $num_display = 10;

		$body = "<h2>". __gettext("River Widget") . "</h2>";
		$body .= "<p>" . __gettext("This widget displays a river of events to do with a user.") . "</p>";

		$body .= "<p><select name=\"widget_data[river_num_display]\">\n";
		for ($n = 5; $n <50; $n += 5)
		{
			if ($n == $num_display) {
				$selected= "selected=\"selected\"";
      			} else {
				$selected= "";
      			}

			$body .= "<option value=\"" . $n . "\" $selected>" . $n. "</option>\n";
		}

  		$body .= "</select></p>\n";

		// Return HTML
		return $body;
	}

	/** Register a hook to return the friendly name of an object */
	function river_register_friendlyname_hook($object_type, $function)
	{
		global $CFG;

		if (!isset($CFG->river_friendlyname_hooks))
			$CFG->river_friendlyname_hooks = array();

		$CFG->river_friendlyname_hooks[$object_type][] = $function;
	}

	/** Look through registed hooks for something that will return a friendly string identifying $object_id of type $object_type */
	function river_get_friendly_id($object_type, $object_id)
	{
		global $CFG;

		$fn = "";

		if (isset($CFG->river_friendlyname_hooks[$object_type]))
		{
			foreach ($CFG->river_friendlyname_hooks[$object_type] as $function)
			{ 
				if (is_callable($function))
					$fn = $function( $object_type, $object_id);
			}

			if ($fn=="")
				$fn = $object_type . " " . $object_id;
		}

		return $fn;
	}

	/** Simple function to get the URL of a user from their userid */
	function river_get_userurl($userid)
	{
		global $CFG;
		
		return $CFG->wwwroot . user_info("username", $userid) . "/";
	}

/* FUture notes:
 - collections on river so that you can filter them
*/

?>