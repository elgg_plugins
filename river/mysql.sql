-- River table
-- This stores a list of events as they happen in the system.
-- @author Marcus Povey <marcus@dushka.co.uk>

CREATE TABLE `prefix_river` (

	ident int(11) NOT NULL auto_increment COMMENT '-> record number',

	userid int(11) NOT NULL COMMENT '-> ident of user triggering this event',
	
	object_id int(11) NOT NULL COMMENT '-> ident of object this event was triggered on',
  	object_owner int(11) NOT NULL COMMENT '-> ident of objects owner',
	object_type varchar(128) NOT NULL COMMENT '-> the type of event, publish etc',
	access varchar(128) NOT NULL COMMENT '-> Access permissions on message, you should set this to the access permissions of the object inself',

	string text NOT NULL COMMENT '-> String describing the event',

	ts int(11) NOT NULL COMMENT '-> Time the event was triggered',

	PRIMARY KEY (ident)
) ENGINE=MyISAM;
