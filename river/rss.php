<?php
	/**
     * RSS view of river
     * (c) 2007 Curverider Ltd
     * Plugin for Elgg 0.8+
     * Released under a GPL v2 license.
     * 
     * Written by Marcus Povey <marcus@dushka.co.uk>
     **********************************/

     
    // Load the Elgg framework
    require_once("../../includes.php");
    global $CFG;
    
    /*
     * Variable initialisation
     */
        
    $owner = page_owner();                          // Is this page owned by anybody?
    if (empty($owner)) {
        $owner = -1;
    }
    
    $river = river_get_river($owner, $limit = 10);
    
    $body = "";
    
    if ($river)
    {
    	// River here, render RSS
		$name = user_info("name", $owner);
		$username = user_info("username", $owner);
		
		$title = sprintf(__gettext("%s's River"), $name);
		$link = $CFG->wwwroot . $username . "/";
		$description = sprintf(__gettext("An RSS feed from %s's river"), $name);
		
		$body .= <<< END
<?xml version="1.0" encoding="ISO-8859-1" ?>
<rss version="0.91">
	<channel>
		<title>$title</title> 
		<link>$link</link> 
		<description>$description</description> 
		<language>en-gb</language>
END;
		
		foreach ($river as $r)
		{
			$itemTitle = __gettext("River Item");
			$itemDesc = htmlentities($r->string);
			$itemLink = $link;
			
			$body .= <<< END
				<item>
					<title>$itemTitle</title>
					<link>$itemLink</link>
					<description>$itemDesc</description>
				</item>
END;
			
		}

		$body .= <<< END
</rss>
END;
    }
    
    echo $body;
?>