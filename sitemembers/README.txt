Sitemembers template keyword
Originally written for Elgg.org, February 2007
Curverider Ltd
info@curverider.co.uk

USAGE:

Install in the /mod/ folder of your Elgg installation. Then, in your pageshell
or frontpage_loggedin/loggedout template elements, you can add:

{{sitemembericons}}

For the icons of 9 random members, or

{{sitemembericons:N}}

For the icons of N random members.

This keyword will return people only - no communities.