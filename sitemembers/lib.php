<?php

    function sitemembers_pagesetup() {
    }
    
    function sitemembers_init() {
        
        global $CFG;
        $CFG->templates->variables_substitute['sitemembericons'][] = "sitemembers_random_users";
        
    }
    
    function sitemembers_random_users($vars) {
        
        global $CFG;
        if (!isset($vars[1])) {
            $numberofusers = 9;
        } else {
            $numberofusers = (int) $vars[1];
        }
        
        $html = "";
        
        if ($users = get_records_sql("select * from ".$CFG->prefix."users where icon <> -1 and user_type = 'person' order by rand() limit $numberofusers")) {
            foreach($users as $user) {
                
                $html .= "<a href=\"{$CFG->wwwroot}{$user->username}\"><img src=\"{$CFG->wwwroot}_icon/user/{$user->icon}/h/67/w/67\" alt=\"" . htmlspecialchars($user->name) . "\" /></a> ";
                
            }
        }
        
        return $html;
        
    }

?>