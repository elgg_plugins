<?php

//widget ident tells you which widget you are dealing with

// sets up the widget, leave blank
function commentwall_pagesetup() {
}

// tells Elgg the widget exists
function commentwall_init() {
	global $CFG, $profile_id, $db, $messages, $page_owner;

	$tables = $db->Metatables();
	if (!in_array($CFG->prefix . "profile_commentwall",$tables)) {
    		if (file_exists($CFG->dirroot . "mod/commentwall/$CFG->dbtype.sql")) {
        		modify_database($CFG->dirroot . "mod/commentwall/$CFG->dbtype.sql");
    		} else {
        		error("Error: Your database ($CFG->dbtype) is not yet fully supported by the comment wall.  See the mod/commentwall directory.");
    		}
    		print_continue("index.php");
    		exit;
	}

	//If a comment has been submitted, populate the database here
	$action = optional_param("action");

    if($action == 'commentwall:add' && $sneaky == '') {

        $widget_id = optional_param("widget_id");
    	$comment_id = optional_param("comment_id");
    	$comment = optional_param("comment");
    	
    	// TODO: check for access permissions!
    	$access = run("users:access_level_sql_where",$_SESSION['userid']);
    	if ($widget = get_record_sql("select ident from {$CFG->prefix}widgets where {$access} and type = \"commentwall::example\"")) {
    	
        	$logged_in_user = $_SESSION['userid']; // get the id of the person logged in
        	$profile_owner = optional_param("profile_id"); // get the profile owner
        	$sneaky = optional_param("sneaky"); // used to try and stop spam
    		$commentwall = new stdClass;
    		$commentwall->parent_widget = $widget_id;
    		$commentwall->comment_owner = $_SESSION['userid'];
    		$commentwall->content = $comment;
    		$commentwall->time_posted = time();
    		
    		$comment = new stdClass;
    		$comment->owner = $comment_owner;
            $comment->posted = time();
            $comment->body = $comment;
            $comment = plugin_hook("weblog_comment","create",$comment);
    		
            if ($comment) {
    	        insert_record("profile_commentwall",$commentwall);

    		    //used to email the comment wall owner
    		    notify_user(page_owner(), __gettext("Someone has posted to your comment wall"), __gettext("Check it out") . ": " . $CFG->wwwroot . user_info("username", page_owner()) . "/profile/");
		    }
    		
	    }
		
		//To stop the back browser issue, use redirect
		$_SESSION['messages'] = $messages;
		header("Location: {$CFG->wwwroot}" . user_info("username", page_owner()) . "/profile/");
		exit;
     } else if ($action == 'commentwall:delete' && 
                                                ($page_owner == $_SESSION['userid'])
                                                || user_flag_get('admin',$_SESSION['userid'])) {
		$widget_id = optional_param("widget_id");
    	$comment_id = optional_param("comment_id");
		//check the comment actually exists
		if ($records = get_records_sql("select * from {$CFG->prefix}profile_commentwall where ident={$comment_id}")) {
			//delete the comment
			delete_records("profile_commentwall","ident",$comment_id);
			$_SESSION['messages'] = $messages;
			header("Location: {$CFG->wwwroot}" . user_info("username", page_owner()) . "/profile/");
		    exit;
		}
	}

    listen_for_event("widget","delete","commentwall_widget_delete");

    $CFG->widgets->list[] = array(
                                        'name' => __gettext("Comment Wall"),
                                        'description' => __gettext("Leave me a comment."),
                                        // Note that type must be of the form modulename::widgetname
                                        'type' => "commentwall::example"
                                );
}

// handles the widget display
function commentwall_widget_display($widget) {
    global $CFG;
	$owner = $widget->ident;
	$title = __gettext("Comment Wall");
	if (!$limit_number = widget_get_data("widget_comment_number",$widget->ident)) {
		$limit_number = 6;
    }

	$body = <<<END
<style>
   .comment {
		margin:0 0 20px 0;
		border-top:1px solid #ccc;
		background:url({$CFG->wwwroot}mod/commentwall/images/comments_bk.gif) repeat-x;
	}
   .comment img {
		border:1px solid #eee;
		padding:2px;
		margin:0 4px 5px 0;
		float:left;
	}
	#sneaky {
		display:none;
    }
	.delete-comment-icon img {
        border:none;
		float:right;
	}
</style>
END;

	if ($records = get_records_sql("select * from {$CFG->prefix}profile_commentwall where parent_widget={$owner} ORDER BY time_posted DESC LIMIT {$limit_number}")) {
		foreach($records as $record) {
			$date = date("g:i a, F j", $record->time_posted);
			$body .= "<div class=\"comment\"><p>";
            $returnConfirm = __gettext("Are you sure you want to delete this comment?");
			$pathtodeleteicon = $CFG->wwwroot . "/mod/commentwall/images/delete.gif";

			//delete comments
			$delete_path = $CFG->wwwroot . "profile/index.php?profile_id=" . page_owner() ."&action=commentwall:delete&comment_id=" . $record->ident;

			// only show the option to delete if the logged in user owns the profile
			if(page_owner() == $_SESSION['userid']) {
				$commentmenu = <<< END
				<a href="$delete_path" onclick="return confirm('$returnConfirm')" title="delete comment" class="delete-comment-icon"><img src="$pathtodeleteicon" /></a>
END;
				$body .= $commentmenu;
            }
			if ($record->comment_owner > 0) {
				$body .= "<a href=\"" . $CFG->wwwroot . user_info("username", $record->comment_owner) . "\">";
				$body .= user_icon_html($record->comment_owner, 50);
				$body .= "</a>";
				$body .= "<b>" . user_name($record->comment_owner) . "</b><small> at " . $date . " " . __gettext("says") . "</small><br />";
			} else {
				//display comments posted by guests
                $body .= user_icon_html($record->comment_owner, 50);
				$body .= "<b>A guest</b><small> at " . $date . " " . __gettext("says") . "</small><br />";
			}
			$body .= $record->content . "</p></div>";
	   }
	} else {
		$body .= "No comments - why not be the first!";
	}
	    
	    $body .= "<br /><form action=\"\" method=\"post\"><input type=\"hidden\" name=\"widget_id\" value=\"{$widget->ident}\"><input type=\"hidden\" name=\"comment-owner\" value=\"{$_SESSION['userid']}\"><input type=\"hidden\" name=\"action\" value=\"commentwall:add\"><p><textarea name=\"comment\" row=\"4\" column=\"40\"></textarea></p>";
		$body .= "<p><input type=\"text\" name=\"sneaky\" value=\"\" id=\"sneaky\"><input type=\"submit\" name=\"\" value=\"post it\" /></form></p>";
	
    return array(
                    'title' => $title,
                    'content'=>$body
                );
}

// handles editing of the widget
function commentwall_widget_edit($widget) {
    global $CFG, $page_owner;
    
    /*
        Display form contents - without the form tags or save button.
        Fields are of the form widget_data[data_name]; you can get data
        out using widget_get_data("data_name",$widget->ident).
    */

	$widget_comment_number = widget_get_data("widget_comment_number",$widget->ident);
	if (empty($widget_comment_number)) { $widget_comment_number = 6; }
	$body = "<h2>Comment Wall</h2>";
	$body .= "<p>" . __gettext("How many comments would you like to appear?") . "</p>";
	$body .= "<p><input type=\"text\" name=\"widget_data[widget_comment_number]\" value=\"{$widget_comment_number}\">";
    $body .= "<p>Change the privacy level of your comment wall.</p>";
    return $body;
}

// delete all comments from the database when a user kills their comment wall
function commentwall_widget_delete($object_type, $event, $object) {

    if ($object_type == "widget" && $event == "delete") {

        delete_records("profile_commentwall", "parent_widget",$object->ident);

    }

    return $object;

}

?>