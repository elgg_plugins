CREATE TABLE `prefix_profile_commentwall` (
  `ident` int(11) NOT NULL auto_increment,
  `parent_widget` int(11) NOT NULL,
  `comment_owner` int(11) NOT NULL,
  `content` text character set utf8 NOT NULL,
  `time_posted` int(11) NOT NULL,
  PRIMARY KEY  (`ident`)
);
