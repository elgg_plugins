<?php

    /*
        Elgg Akismet plugin
    
        Ben Werdmuller, Dec 18 2006
        ben@curverider.co.uk
        
        Released under the GPL v2
    */

        global $CFG;
    
    // Configuration - enter your Akismet API key and blog URL (including http://) here
    // See http://akismet.com/commercial/ for more details.
        $CFG->akismet->apikey = "";
        $CFG->akismet->blogurl = "";
    
    // Menu items
        function akismet_pagesetup() {            
            // TODO: add a menu item to flagged comments
        }
        
    // Initialise the database etc
        function akismet_init() {
            
            global $CFG, $function, $db, $METATABLES;
            
            /*
            
            TODO: add flagged comments database and interface to manage false positives
                  and erase real spam
            
            // Check for the akismet database
                if (!in_array($CFG->prefix . "akismet_comments", $METATABLES) || !in_array($CFG->prefix . "akismet_comments", $METATABLES)) {
                    if (file_exists($CFG->dirroot . "mod/akismet/$CFG->dbtype.sql")) {
                        modify_database($CFG->dirroot . "mod/akismet/$CFG->dbtype.sql");
                    } else {
                        error("Error: Your database ($CFG->dbtype) is not yet fully supported by the Elgg Akismet plugin.  See the mod/akismet directory.");
                    }
                    print_continue("index.php");
                    exit;
                }
            */    
            // Register the event: we want to know when a comment is created
            
                listen_for_event("comment","create","akismet_vet");
                listen_for_event("weblog_comment","create","akismet_vet");
            
        }
        
    // Vet comments for spam
        function akismet_vet($object_type, $event, $object) {
            
            global $CFG, $messages;
            
            if (($object_type == "comment" || $object_type == "weblog_comment") && !empty($CFG->akismet->apikey) && !empty($CFG->akismet->blogurl)) {
            
                // Load Akismet class
                    @require_once($CFG->dirroot . "mod/akismet/PHP5Akismet.0.2/Akismet.class.php");
                
                // Get the parent post
                    $post = get_record('weblog_posts','ident',$object->post_id);
                    
                // Generate the post URL
                    if (!isset($object->comment_type)) {
                        $object->comment_type = "weblog";
                    }
                    switch($object->comment_type) {
                        case "weblog":
                            $url = $CFG->wwwroot . user_info("username", $post->weblog) . "/". $object->comment_type ."/" . $post->ident . ".html";
                            break;
                    }
                    
                // Initialise Akismet
                    $akismet = new Akismet($CFG->akismet->blogurl,$CFG->akismet->apikey);
                    $akismet->setCommentAuthor($object->postedname);
                    $akismet->setCommentAuthorEmail("");
                    $akismet->setCommentAuthorURL("");
                    $akismet->setCommentContent($object->body);
                    $akismet->setPermalink($url);
                    
                    if ($akismet->isCommentSpam()) {
                        $messages[] = __gettext("Your comment could not be posted.");
                        return null;
                    } else {
                        return $object;
                    }
                
            } else {
                return $object;
            }
            
        }

?>