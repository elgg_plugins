<?php
/*
    Mailblogger plugin for Elgg
    Initial author: Sven Edge <sven@elgg.net> September 2006

    Plugin to read messages from an IMAP mailbox and parse and post them to users' blogs.
    
    Config options:
    
    $CFG->mailblogger_imapusername - required
    $CFG->mailblogger_imappassword - required
    $CFG->mailblogger_toaddress - required, defines the address messages are sent to
    
    $CFG->mailblogger_hostname - optional, defaults to "localhost"
    $CFG->mailblogger_imapport - optional, defaults to 143 (SSL is 993)
    $CFG->mailblogger_imapmailbox - optional, defaults to "INBOX"
    
    $CFG->mailblogger_imapflags - optional, see http://php.net/manual/en/function.imap-open.php
    e.g. "/ssl" or "/ssl/novalidate-cert"
    
    NB: SSL unlikely to work on Windows
    
    $CFG->mailblogger_disablecron - optional, defaults to false;
    $CFG->mailblogger_imaprejectedmailbox - optional, if not set, rejected messages are deleted. can not be the same as $CFG->mailblogger_imapmailbox
    $CFG->mailblogger_imapposteddmailbox - optional, if not set, posted messages are deleted. can not be the same as $CFG->mailblogger_imapmailbox
    $CFG->mailblogger_maildomain - Used in To address detection and in user blurb - NOT CURRENTLY USED
    
*/
    /*========================================================================*/

    //required function
    function mailblogger_pagesetup() {
        
    } // end function mailblogger_pagesetup
    
    
    /*========================================================================*/
    
    function mailblogger_init() {
        
        global $function;
        global $CFG;
        
        if (!empty($CFG->mailblogger_maildomain)) {
            $CFG->mailblogger_maildomain = trim($CFG->mailblogger_maildomain);
        } else {
            $CFG->mailblogger_maildomain = '';
        }
        
        if (!empty($CFG->mailblogger_toaddress)) {
            $CFG->mailblogger_toaddress = trim($CFG->mailblogger_toaddress);
        } else {
            $CFG->mailblogger_toaddress = '';
        }
        
        // Action handling
        $function['userdetails:init'][] = path . "mod/mailblogger/mailblogger_userdetails_actions.php";
        // User details editable options
        $function['userdetails:edit:details'][] = path . "mod/mailblogger/mailblogger_userdetails_edit_details.php";
        
    } // end function mailblogger_init
    
    
    /*========================================================================*/
    
    // called by cron.php
    function mailblogger_cron() {
        
        global $CFG;
        
        if (empty($CFG->mailblogger_disablecron)) {
            if (!empty($CFG->mailblogger_lastcron) && (time() - 300) < $CFG->mailblogger_lastcron) {
                return true;
            } else {
                mailblogger_processnewmessages();
            }
            
            set_config('mailblogger_lastcron',time());
        }
        
    } // end function mailblogger_cron
    
    /*========================================================================*/
    
    // open connection
    function mailblogger_openmailbox() {
        
        global $CFG;
        $returnvalue = false;
        
        if (!empty($CFG->mailblogger_imapusername) && !empty($CFG->mailblogger_imappassword)) {
            
            if (empty($CFG->mailblogger_hostname)) {
                $CFG->mailblogger_hostname = 'localhost';
            } else {
                $CFG->mailblogger_hostname = trim($CFG->mailblogger_hostname);
            }
            
            if (!isset($CFG->mailblogger_imapport)) {
                $CFG->mailblogger_imapport = 143;
            } else {
                $CFG->mailblogger_imapport = (int) $CFG->mailblogger_imapport;
                if (!$CFG->mailblogger_imapport) {
                    $CFG->mailblogger_imapport = 143;
                }
            }
            
            if (empty($CFG->mailblogger_imapmailbox)) {
                $CFG->mailblogger_imapmailbox = 'INBOX';
            } else {
                $CFG->mailblogger_imapmailbox = trim($CFG->mailblogger_imapmailbox);
            }
            
            if (empty($CFG->mailblogger_imapflags)) {
                $CFG->mailblogger_imapflags = '';
            } else {
                $CFG->mailblogger_imapflags = trim($CFG->mailblogger_imapflags);
            }
            
            $mailbox = '{' . $CFG->mailblogger_hostname . ':' . $CFG->mailblogger_imapport . $CFG->mailblogger_imapflags . '}' . $CFG->mailblogger_imapmailbox;
            
            if ($resource = imap_open($mailbox, $CFG->mailblogger_imapusername, $CFG->mailblogger_imappassword)) {
                $returnvalue = $resource;
            } else {
                mtrace("mailblogger error: could not open IMAP mailbox.");
                $errs = imap_errors();
                if (is_array($errs)) {
                    mtrace(implode("\n", $errs));
                }
            }
            
        } else {
            mtrace("mailblogger error: IMAP username or password not specified.");
        }
        
        return $returnvalue;
    }
    
    /*========================================================================*/
    
    function mailblogger_closemailbox($imapresource) {
        if (is_resource($imapresource)) {
            imap_expunge($imapresource);
            imap_close($imapresource);
        }
    }
    
    /*========================================================================*/
    
    
    
    // search imap mailbox
    // returns array of message UIDs on success, false on failure
    function mailblogger_findmessages($imapresource, $searchfield, $searchvalue) {
        
        $searchfield = trim($searchfield);
        $returnvalue = array();
        
        if (is_resource($imapresource) && $searchfield) {
            switch ($searchfield) {
                case "TO":
                    // searchvalue is username/email alias/etc
                    $searchvalue = trim($searchvalue);
                    if ($searchvalue) {
                        $searchresult = imap_search ($imapresource, 'UNDELETED TO "' . $searchvalue . '"', SE_UID);
                    }
                    break;
                case "SINCE":
                    //searchvalue is unix timestamp
                    $searchvalue = (int) $searchvalue;
                    if ($searchvalue) {
                        $imapdate = gmdate("D, d M Y H:i:s +0000", $searchvalue);
                        //var_dump($imapdate);
                        $searchresult = imap_search ($imapresource, 'UNDELETED SINCE "' . $imapdate . '"', SE_UID);
                    }
                    break;
                case "ALL":
                    $searchresult = imap_search ($imapresource, 'UNDELETED', SE_UID);
                    break;
            }
            //var_dump($searchresult);
            if (is_array($searchresult)) {
                $returnvalue = $searchresult;
            }
        }
        
        return $returnvalue;
    } // end function mailblogger_findmessages
    
    
    /*========================================================================*/
    
    function mailblogger_processnewmessages() {
        
        global $CFG;
        
        if ($imap = mailblogger_openmailbox()) {
            $msgs = mailblogger_findmessages($imap, "ALL", '');
            //$msgs = mailblogger_findmessages($imap, "SINCE", gmmktime(12, 00, 00, 9, 11, 2006));
            
            $messaged = 0;
            $messages = count($msgs);
            if ($messages) {
                foreach ($msgs as $auid) {
                    @set_time_limit(120);
                    $messaged += mailblogger_processmessage($imap, $auid);
                }
                mtrace('mailblogger: posted ' . $messaged . ' of ' . $messages . ' messages.');
            }
            mailblogger_closemailbox($imap);
        }
        
    }
    
    /*========================================================================*/
    
    
    function mailblogger_processmessage($imapresource, $uid) {
        
        global $CFG;
        $posted = 0;
        
        if (is_resource($imapresource) && $uid) {
            if ($structure = imap_fetchstructure($imapresource, $uid, FT_UID)) {
                $overview = array_shift(imap_fetch_overview($imapresource, $uid, FT_UID));
            
                $continue = false;
                // TODO - this regexp might want tuning if impure addresses are expected
                if (preg_match('/([0-9a-zA-Z]+)([0-9]{4})$/', $overview->subject, $matches)) {
                    $username = $matches[1];
                    $userpin = $matches[2];
                    // $domain = $matches[3];
                    /*
                    if ($CFG->mailblogger_maildomain && $CFG->mailblogger_maildomain != $domain) {
                        mtrace('mailblogger: ignored a message due to bad To domain');
                    } else
                    */
                    if ($userid = user_info_username('ident', $username)) {
                        
                        //in the absence of a general "can this username log in" function, cheat...
                        $userpass = user_info_username('password', $username);
//                         if ($loginsuccess = authenticate_account($username,$userpass)) {
                            
                            $userpindb = user_flag_get('mailblogger_pin', $userid);
                            if ($userpin && $userpindb && $userpin == $userpindb) {
                                //authenticated, continue posting
                                $continue = true;
                            } else {
                                //not authenticated
                                mtrace('mailblogger: ignored a message due to bad PIN');
                            }
                            
//                         } else {
//                             mtrace('mailblogger: ignored a message due to disabled user account');
//                         }
                    } else {
                        mtrace('mailblogger: ignored a message due to bad username');
                    }
                } else {
                    mtrace('mailblogger: ignored a message due to missing username/PIN');
                }
                
                if ($continue) {
                    
                    if (!isset($structure->parts)) {
                        //put top node into a tree if there are no children
                        $structure2 = new StdClass;
                        $structure2->parts[] = $structure;
                        $structure = $structure2;
                    }
                    
                    $billofparts = mailblogger_msgpartwalker($structure->parts, '', 0);
                    //var_dump($billofparts);
                    //echo '<hr />';
                    //var_dump($structure);
                    //var_dump($overview);
                    //return;
                    
                    $postbody = '';
                    $uploadedfiles = false;
                    
                    foreach ($billofparts as $apartid => $apart) {
                        
                        @set_time_limit(120);
                        
                        if ($guff = imap_fetchbody($imapresource, $uid, $apartid, FT_UID)) {
                            $guff = mailblogger_transfer_decode($apart->encoding, $guff);
                            
                            if ($apart->type == 0 && strtolower($apart->subtype) == 'plain') {
                                //text
                                $postbody .= $guff;
                                
                            } else {
                                //attachment
                                
                                //check file for goodness.
                                //TODO - could do with using at least some of uploadlib, but that assumes POSTed files
                                
                                $total_quota = get_field_sql('SELECT sum(size) FROM '.$CFG->prefix.'files WHERE owner = ?',array($userid));
                                $max_quota = get_field('users','file_quota','ident',$userid);
                                $maxbytes = $max_quota - $total_quota;
                                
                                if (strlen($guff) <= $maxbytes) {
                                    
                                    $reldir =  "files/" . substr($username, 0, 1) . "/" . $username . "/"; 
                                    make_upload_directory($reldir, false);
                                    $dir = $CFG->dataroot .$reldir;
                                    
                                    //$newfileloc = tempnam($dir, "mailblog_");
                                    
                                    $f = new StdClass;
                                    
                                    if ($apart->ifdparameters) {
                                        foreach ($apart->dparameters as $adparam) {
                                            if (strtolower($adparam->attribute) == 'filename') {
                                                $f->originalname = trim($adparam->value);
                                            }
                                        }
                                    }
                                    
                                    //elgg mimetype handling relies on the file extension, so have to generate our own filename rather than using tempnam.
                                    
                                    if (!empty($f->originalname)) {
                                        $filename = time() . '_' . md5(serialize($apart)) . '_' . basename($f->originalname);
                                    } else {
                                        $filename = time() . '_' . md5(serialize($apart));
                                        require_once($CFG->libdir.'/filelib.php');
                                        if (function_exists("mimetype_to_extension")) { // filelib.php svn rev 674+
                                            if ($ext = mimetype_to_extension(strtolower(mailblogger_mimetype_number_to_name($apart->type) . '/' . $apart->subtype) )) {
                                                $filename .= '.' . $ext;
                                            }
                                        }
                                        $f->originalname = $filename;
                                    }
                                    
                                    $newfileloc = $dir . $filename;
                                    
                                    if ($fhandle = @fopen($newfileloc, "wb")) {
                                        fwrite($fhandle, $guff);
                                        fclose($fhandle);
                                        
                                        $f->owner = $userid;
                                        $f->files_owner = $userid;
                                        $f->folder = '-1';
                                        
                                        if ($CFG->default_access == 'PRIVATE') {
                                            $f->access = 'user' . $userid;
                                        } else {
                                            $f->access = $CFG->default_access;
                                        }
                                        
                                        $f->title = 'Untitled';
                                        $f->description = 'Uploaded from a mail attachment';
                                        $f->location = $reldir . $filename;
                                        $f->size = filesize($newfileloc);
                                        $f->time_uploaded = time();
                                        if ($file_id = insert_record('files',$f)) {
                                            $uploadedfiles = true;
                                            $postbody .= "\n{{file:" . $file_id . "}}\n";
                                        }
                                        
                                    } // end if opened file
                                    
                                } else {
                                    // file is over quota
                                }
                                
                            } // end text vs attachment
                        } // end if got part from imap
                    } // end foreach
                    
                    if ($uploadedfiles) {
                        $rssresult = run("files:rss:publish", array($userid, false));
                    }
                    
                    if ($postbody) {
                        $post = new StdClass;
                        // $post->title = $overview->subject;
                        $post->title = __gettext("External message");
                        $post->body = $postbody;
                        if ($CFG->default_access == 'PRIVATE') {
                            $post->access = 'user' . $userid;
                        } else {
                            $post->access = $CFG->default_access;
                        }
                        $post->posted = time();
                        $post->owner = $userid;
                        $post->weblog = $userid;
                        
                        $insert_id = insert_record('weblog_posts',$post);
                        
                        $rssresult = run("profile:rss:publish", array($userid, false));
                        $rssresult = run("weblogs:rss:publish", array($userid, false));
                        
                        $posted = 1;
                    }
                    
                    if (!empty($CFG->mailblogger_imapposteddmailbox) && $CFG->mailblogger_imapposteddmailbox != $CFG->mailblogger_imapmailbox) {
                        imap_mail_move($imapresource, $uid, $CFG->mailblogger_imapposteddmailbox, CP_UID);
                    } else {
                        imap_delete($imapresource, $uid, FT_UID);
                    }
                    
                } else {
                    
                    if (!empty($CFG->mailblogger_imaprejectedmailbox) && $CFG->mailblogger_imaprejectedmailbox != $CFG->mailblogger_imapmailbox) {
                        imap_mail_move($imapresource, $uid, $CFG->mailblogger_imaprejectedmailbox, CP_UID);
                    } else {
                        imap_delete($imapresource, $uid, FT_UID);
                    }
                    
                } // end if continue
                
            } // end if imap_fetchstructure
            
        } // end if valid params
        
        return $posted;
    }
    
    /*========================================================================*/
    
    //recursive function to flatten mime part tree into a simple array of leaf nodes
    function mailblogger_msgpartwalker($partsarray, $mypartid, $depth) {
        
        $depth = (int) $depth;
        $mypartid = trim($mypartid);
        $childpartid = '';
        $flatdata = array();
        
        if (is_array($partsarray)) {
            
            foreach ($partsarray as $apartid => $apart) {
                
                // array is 0-based, imap message parts are 1-based
                if ($mypartid) {
                    $childpartid = $mypartid . '.' . ($apartid + 1);
                } else {
                    $childpartid = ($apartid + 1);
                }
                
                if (isset($apart->parts) && is_array($apart->parts) && count($apart->parts)) {
                    //part is a group of subparts
                    
                    if ($depth < 10){ // prevent excessive recursion
                        $apartresult = mailblogger_msgpartwalker($apart->parts, $childpartid, ++$depth);
                        if ($apartresult) {
                            $flatdata = array_merge($flatdata, $apartresult);
                        }
                    }
                    
                } else {
                    //part is leaf node, add contents
                    $flatdata[$childpartid] = $apart;
                }
            }
            
        }
        
        return $flatdata;
    }
    
    /*========================================================================*/
    
    // examples in php manual seem to try to do to much
    // then again, this plugin ignores character sets
    function mailblogger_transfer_decode($encoding, $data) {
        
        switch ($encoding) {
            case 0:
//                 return imap_utf7_decode($data);
//                 break;
            case 1:
//                 return imap_utf8($data);
//                 break;
            case 2:
                return $data;
                break;
            case 3:
                return base64_decode($data);
                break;
            case 4:
                return imap_qprint($data);
                break;
            case 5:
            default:
                return $data;
                break;
        }
    }
    
    /*========================================================================*/
    
    // imap_fetchstructure represents the major mimetype part as a number
    function mailblogger_mimetype_number_to_name ($typenum) {
        
        $types = array(
            0 => 'text',
            1 => 'multipart',
            2 => 'message',
            3 => 'application',
            4 => 'audio',
            5 => 'image',
            6 => 'video',
            7 => 'other'
        );
        
        if (isset($types[$typenum])) {
            return $types[$typenum];
        } else {
            return $types[7];
        }
    }
    
    /*========================================================================*/
    
    // user account settings page form
    function mailblogger_userdetails_edit_details($userid) {
        
        global $CFG;
        
        $title = __gettext("Mobile/Mail blogging:");
        $blurb = __gettext("Choose a 4-digit PIN to identify yourself to the server in a phone/mail post.");
        $userpin = user_flag_get('mailblogger_pin', $userid);
        $username = user_info('username', $userid); 
        if ($username && $userpin && $CFG->mailblogger_toaddress) {
            $blurb .= '<br />' . sprintf(__gettext("To blog by phone/mail, send your post to %s with %s as your subject line."), $CFG->mailblogger_toaddress, $username . $userpin);
        }
        
        $body = <<< END
            <h2>$title</h2>
            <p>$blurb</p>
END;
        
        // TODO - password or normal text box? second box for confirmation?
        
        $body .= templates_draw( array(
                'context' => 'databox',
                'name' => __gettext("PIN: "),
                'column1' => '<input type="text" maxlength="4" size="5" name="mailblogger_pin" value="' . $userpin . '"/>'
        )
        );
        
        return $body;
        
    } // end function mailblogger_userdetails_edit_details
    
    /*========================================================================*/
    
    // user account settings page actions
    function mailblogger_userdetails_actions() {
        
        global $messages;
        
        // Save the user's PIN
        $action = optional_param('action');
        $id = optional_param('id',0,PARAM_INT);
        $form_pin = optional_param('mailblogger_pin',0,PARAM_INT);
        $db_pin = user_flag_get('mailblogger_pin', $id);
        
        if (logged_on && !empty($action) && run("permissions:check", array("userdetails:change",$id)) && $action == "userdetails:update") {
            if (!empty($form_pin)) {
                if (strlen($form_pin) == 4) {
                    if ($form_pin == $db_pin) {
                        //$messages[] .= __gettext("Your PIN has been saved.");
                    } else {
                        if (user_flag_set('mailblogger_pin', $form_pin, $id)) {
                            $messages[] .= __gettext("Your PIN has been saved.");
                        } else {
                            $messages[] .= __gettext("Your PIN could not be saved.");
                        }
                    }
                } else {
                    $messages[] .= __gettext("Your PIN could not be saved. It was not 4 digits long.");
                }
            } else {
                user_flag_unset('mailblogger_pin', $id);
                $messages[] .= __gettext("Your PIN has been deleted. Mobile/Mail posting is disabled until you set a new PIN.");
            }
        }
        
    } // end function mailblogger_userdetails_actions
    
    /*========================================================================*/
    
?>