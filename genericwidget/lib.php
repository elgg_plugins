<?php

    /*
     * Generic Javascript / Flash widgets
     * Curverider, April 2007
     * Requires Elgg 0.8 or above
     * info@curverider.co.uk
     * (Ben Werdmuller)
     */
     
    // Required function, more or less here for fun
        function genericwidget_pagesetup() {
        }
    
    // Let's define our widgets
        function genericwidget_init() {
            
            global $CFG;
            $CFG->widgets->list[] = array(
                                                'name' => __gettext("Generic widget"),
                                                'description' => __gettext("Allows you to add Javascript or Flash widgets to your profile."),
                                                'type' => "genericwidget::generic"
                                        );
            $CFG->widgets->allcontent[] = "genericwidget::generic";

        }
        
    /**************************************************************************
     Widget editing
     **************************************************************************/

     // General editing placeholder
        function genericwidget_widget_edit($widget) {
            
            switch($widget->type) {
                case "genericwidget::generic":     return genericwidget_generic_edit($widget);
                                                break;
            }
            
        }
        
     // generic editing
        function genericwidget_generic_edit($widget) {

            $generic_body = widget_get_data("genericwidget_generic_body",$widget->ident);
            $generic_title = widget_get_data("genericwidget_generic_title",$widget->ident);
                        
            $body = "<h2>" . __gettext("Generic widget") . "</h2>";
            $body .= "<p>" . __gettext("This widget allows you to add Javascript or Flash widgets to your profile. Just enter the title and code below:") . "</p>";
            $body .= "<p>" . display_input_field(array("widget_data[genericwidget_generic_title]",$generic_title,"text")) . "</p>";
            $body .= "<p>" . display_input_field(array("widget_data[genericwidget_generic_body]",$generic_body,"longtext")) . "</p>";
            $body .= "<input type=\"hidden\" name=\"widget_allcontent\" value=\"yes\" />";
            
            return $body;
            
        }

    /**************************************************************************
     Widget display
     **************************************************************************/
     
     // General display placeholder
        function genericwidget_widget_display($widget) {
            switch($widget->type) {

                case 'genericwidget::generic':         return genericwidget_generic_display($widget);
                                                break;
            }

        }
        
    // generic display
        function genericwidget_generic_display($widget) {
            
            $generic_body = widget_get_data("genericwidget_generic_body",$widget->ident);
            $generic_title = widget_get_data("genericwidget_generic_title",$widget->ident);
            
            if (empty($generic_body)) {
                $generic_title = __gettext("Your widget here");
                $generic_body = __gettext("This widget is undefined.");
            }
            
            return array('title'=>$generic_title,'content'=>$generic_body);
            
        }

?>