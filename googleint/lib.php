<?php

    /*
     * Google integration widgets
     * Curverider, April 2007
     * Requires Elgg 0.8 or above
     * info@curverider.co.uk
     * (Ben Werdmuller)
     */
     
    // Required function, more or less here for fun
        function googleint_pagesetup() {
        }
    
    // Let's define our widgets
        function googleint_init() {
            
            global $CFG;
            $CFG->widgets->list[] = array(
                                                'name' => __gettext("Google Calendar"),
                                                'description' => __gettext("Displays your Google Calendar, if you provide a username."),
                                                'type' => "googleint::calendar"
                                        );

        }
        
    /**************************************************************************
     Widget editing
     **************************************************************************/

     // General editing placeholder
        function googleint_widget_edit($widget) {
            
            switch($widget->type) {
                case "googleint::calendar":     return googleint_calendar_edit($widget);
                                                break;
            }
            
        }
        
     // Calendar editing
        function googleint_calendar_edit($widget) {

            $calendar_username = widget_get_data("googleint_calendar_username",$widget->ident);
                        
            $body = "<h2>" . __gettext("Google Calendar") . "</h2>";
            $body .= "<p>" . __gettext("This widget displays your Google Calendar. Just enter your Google username (usually your Gmail address) below:") . "</p>";
            $body .= "<p>" . display_input_field(array("widget_data[googleint_calendar_username]",$calendar_username,"text")) . "</p>";
            
            return $body;
            
        }

    /**************************************************************************
     Widget display
     **************************************************************************/
     
     // General display placeholder
        function googleint_widget_display($widget) {
            switch($widget->type) {

                case 'googleint::calendar':         return googleint_calendar_display($widget);
                                                break;
            }

        }
        
    // Calendar display
        function googleint_calendar_display($widget) {
            
            if ($calendar_username = widget_get_data("googleint_calendar_username",$widget->ident)) {
                $body = "<iframe src=\"http://www.google.com/calendar/embed?src=".urlencode($calendar_username)."&amp;height=220\" style=\" border-width:0 \" width=\"320\" frameborder=\"0\" height=\"220\"></iframe>";
            } else {
                $body = __gettext("This widget is undefined.");
            }
            
            $title = __gettext("Google Calendar");
            
            return array('title'=>$title,'content'=>$body);
            
        }

?>