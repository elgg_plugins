<?php

    global $CFG;
    global $page_owner;
    
    if ($page_owner != $_SESSION['userid']) {
        
        $tasks = gettext("Tasks");
        $tasklist = gettext("Tasklist");
        
        $run_result .= <<< END
        
<li id="sidebar_tasks"> 
    <h2>{$tasks}</h2>
    <ul>
        <li><a href="{$CFG->wwwroot}mod/tasks/?owner={$page_owner}">{$tasklist}</a></li>
    </ul>
</li>
        
END;
                
    }

?>