<?php

    function logging_pagesetup() {
    }
    
    function logging_init() {
        
        global $CFG, $db, $messages;
                
        $tables = $db->Metatables();
        if (!in_array($CFG->prefix . "logging",$tables)) {
            modify_database($CFG->dirroot . "mod/logging/table.sql");
            print_continue("index.php");
            exit;
        }
        
        listen_for_event("all","all","logging_logger");
        
    }
    
    function logging_logger($object_type, $event, $object) {
        
        global $CFG, $messages, $db;
        
        if ($event != "create") {
        
            $prev_access = "";
            
            if ($event == "update" || $event == "delete") {
                if (!empty($object->ident)) {
                    $messages[] = "select ident, new_access from ".$CFG->prefix."logging where object_type = ".$db->qstr($object_type)." and object_id = ".$db->qstr($object->ident)." order by timestamp desc limit 1";
                    if ($previous = get_records_sql("select ident, new_access from ".$CFG->prefix."logging where object_type = ".$db->qstr($object_type)." and object_id = ".$db->qstr($object->ident)." order by timestamp desc limit 1")) {
                        foreach($previous as $prev_item) {
                            $prev_access = $prev_item->new_access;
                        }
                    }
                    
                }
            }
            
            $log = new stdClass;
            $log->timestamp = time();
            $log->object_type = $object_type;
            $log->event_type = $event;
            $log->object_id = $object->ident;
            $log->old_access = $prev_access;
            $log->new_access = $object->access;
            $log->user_id = $_SESSION['userid'];
            
            if (!insert_record('logging',$log)) {
                $messages[] = gettext("Logging failed.");
            }
        
        }
        
        return $object;
        
    }


?>