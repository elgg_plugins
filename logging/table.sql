CREATE TABLE `elgglogging` (
  `ident` int(11) NOT NULL auto_increment,
  `timestamp` int(11) NOT NULL,
  `object_type` varchar(128)  NOT NULL,
  `object_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `event_type` varchar(128)  NOT NULL,
  `old_access` varchar(128)  NOT NULL,
  `new_access` varchar(128)  NOT NULL,
  PRIMARY KEY  (`ident`),
  KEY `timestamp` (`timestamp`,`object_type`)
) ENGINE=MyISAM  ;
