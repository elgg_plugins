<?php

    /*
     * Explode integration widget
     * Curverider, April 2007
     * Requires Elgg 0.8 or above
     * info@curverider.co.uk
     * (Ben Werdmuller)
     */
     
    // Required function, more or less here for fun
        function explodewidget_pagesetup() {
        }
    
    // Let's define our widgets
        function explodewidget_init() {
            
            global $CFG;
            $CFG->widgets->list[] = array(
                                                'name' => __gettext("Explode widget"),
                                                'description' => __gettext("Allows you to add Javascript or Flash widgets to your profile."),
                                                'type' => "explodewidget::explode"
                                        );
            $CFG->widgets->allcontent[] = "explodewidget::explode";

        }
        
    /**************************************************************************
     Widget editing
     **************************************************************************/

     // General editing placeholder
        function explodewidget_widget_edit($widget) {
            
            switch($widget->type) {
                case "explodewidget::explode":     return explodewidget_explode_edit($widget);
                                                break;
            }
            
        }
        
     // explode editing
        function explodewidget_explode_edit($widget) {

            $explode_username = widget_get_data("explodewidget_explode_username",$widget->ident);
                        
            $body = "<h2>" . __gettext("Explode widget") . "</h2>";
            $body .= "<p>" . __gettext("This widget allows you to add your Explode friends to your profile. Just enter your username below:") . "</p>";
            $body .= "<p>" . display_input_field(array("widget_data[explodewidget_explode_username]",$explode_username,"text")) . "</p>";
            
            return $body;
            
        }

    /**************************************************************************
     Widget display
     **************************************************************************/
     
     // General display placeholder
        function explodewidget_widget_display($widget) {
            switch($widget->type) {

                case 'explodewidget::explode':         return explodewidget_explode_display($widget);
                                                break;
            }

        }
        
    // explode display
        function explodewidget_explode_display($widget) {
            
            $explode_username = widget_get_data("explodewidget_explode_username",$widget->ident);
            
            if (empty($explode_username)) {
                $body = __gettext("This widget is undefined.");
            } else {
                $body = "<script src=\"http://explode.elgg.org/". $explode_username ."/js/\"></script>";
            }
            
            return array('title'=>"Explode",'content'=>$body);
            
        }

?>