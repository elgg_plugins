<?php

    function fancysubmenu_pagesetup() {
    }
    
    function fancysubmenu_init() {
      
      global $CFG, $metatags, $template;

	  $template['submenu'] = ''; // have to erase the old submenu
      $CFG->templates->variables_substitute['submenu'][] = "fancysubmenu_main";
      $metatags .= @file_get_contents($CFG->dirroot . "mod/fancysubmenu/css");  
    }

    function fancysubmenu_main($vars) {
        
		global $CFG, $PAGE, $template;

		//define("context", "submenu");
		//$template['css'] .= file_get_contents($CFG->dirroot . "mod/fancysubmenu/css");
		
        if (!isset($vars[1])) {
            $cols = 4;
        } else {
			$cols = (int) $vars[1];
        }

		$items = count($PAGE->menu_sub);
		$content = '';
		$count = 0;
		foreach ($PAGE->menu_sub as $item){
			if ($count % $cols == 0){
				if($count != 0)
					$content .= "</tr>\n";
				$content .= "<tr>\n";
			}

			if (preg_match("/<img/i",$item['html']))
				$icon = '';
			else 
				$icon = fancysubmenu_icon($item['name']);
			
			$content .= "<td>".$icon.$item['html']."</td>\n";
			$count ++;
		}
		$content .= "</tr>\n";

		$html = <<< END
		<div id="sub-menu">
		<table>
			{$content}
		</table> 
		</div>
END;
		
		return $html;
        
    }

	// set icon to a submenu item
	function fancysubmenu_icon ($itemname) {
		global $CFG;

		$icon = '';
		switch ($itemname){
			// blog icons
			case 'blog:view';
				$icon = "blog.gif";
				break;
			case 'blog:archive':
				$icon = "pasta.gif";
				break;
			case 'blog:friends':
				$icon = "contatos_blog.gif";
				break;
			case 'blog:interesting':
				$icon = "estrela.gif";
				break;
			case 'blog:everyone':
				$icon = "globo.gif";
				break;
			case 'blog:category':
				$icon = "ferramenta.gif";
				break;
			case 'weblog:forum':
				$icon = "forum.gif";
				break;
			case 'blog:help':
				$icon = "ajuda.gif";
				$title = "Ajuda com 'Seu blog'";
				break;
			// file icons
			case 'file:add':
				$icon = "novo_arquivo.gif";
				break;
			case 'file:rss':
				$icon = "feeds.gif";
				break;
			// agreggator icons
			case 'newsfeed:subscription':
				$icon = "adfeed.gif";
				break;
			case 'newsfeed:subscription:publish:blog':
				$icon = "blogfeed.gif";
				break;
			case 'newsclient':
				$icon = "agregador.gif";
				break;
			case 'feed':
				$icon = "estrelafeed.gif";
				break;
			// file icons
			case 'file:add':
				$icon = "novo_arquivo.gif";
				break;
			case 'file:rss':
				$icon = "feeds.gif";
				break;
			// community icons
			case 'community':
				$icon = "comunidades.gif";
				break;
			case 'community:owned':
				$icon = "comunidadessuas.gif";
				break;
			case 'community:help':
				$icon = "ajuda.gif";
				$title = "Ajuda com 'Sua rede'";
				break;
			case 'community:pic':
				$icon = "avatar.gif";
				break;
			case 'community:edit':
				$icon = "edit_comunidade.gif";
				break;
			case 'community:requests':
				$icon = "req_comunidades.gif";
				break;
			// friend icons
			case 'friend':
				$icon = "contatos.gif";
				break;
			case 'friend:of':
				$icon = "contatos2.gif";
				break;
			case 'friend:requests':
				$icon = "requisicao.gif";
				break;
			case 'friend:foaf':
				$icon = "foaf.gif";
				break;
			case 'friend:accesscontrols':
				$icon = "controle.gif";
				break;
			// profile icons
			case 'profile:edit':
				$icon = "edit_perfil.gif";
				break;
			case 'profile:picedit':
				$icon = "avatar.gif";
				break;
			case 'profile:widget:add':
				$icon = "nova_caixa.gif";
				break;
			case 'profile:help':
				$icon = "ajuda.gif";
				$title = "Ajuda com 'Seu perfil'";
				break;
			// dashboard icons
			case 'dashboard:edit':
				$icon = "nova_caixa.gif";
				break;
			case 'dashboard:help':
				$icon = "ajuda.gif";
				$title = "Ajuda com 'Seu painel'";
				break;
			// file icons
			case 'file:help':
				$icon = "ajuda.gif";
				$title = "Ajuda com 'Seus arquivos'";
				break;
			// resources icons
			case 'resources:help':
				$icon = "ajuda.gif";
				$title = "Ajuda com 'Seu agregador'";
				break;
			default:
				return '';
			// forum plugin icons
			case 'forum:blogview';
				$icon = "blog.gif";
				break;
			case 'forum:add_discussion':
				$icon = "msg_blog.gif";
				break;
			case 'forum:add_comment':
				$icon = "msg_blog.gif";
				break;
			// Presentation
		}
		
		$html = "<img src=\"{$CFG->wwwroot}mod/fancysubmenu/img/{$icon}\" border=\"0\"";
		
		if(isset($title)) {$html .= ' title="'.$title.'"';}
		
		$html .= '> ';
		
		return $html;
			
	}

?>
