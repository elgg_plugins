Fancy Submenu Plugin
Originally written, March 2007
Renato Mendes Coutinho, 
member of the Stoa Team at University of Sao Paulo (http://stoa.usp.br)
renato@cecm.usp.br

ABOUT:

This plugin changes the way submenu items appear. It puts them into a table
along with some icons, helping navigation and niceness =)

USAGE:

Just install in the /mod/ folder of your Elgg installation.

