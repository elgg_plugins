<?php
    // Return the current post icon id
    
    // $parameter[0] should be a query resultset

    $current = (int) get_field('weblog_posts','icon','ident',$parameter->ident);

    // Icon not set yet
    if ($current == -1) {
        // Default user selected site image
        $current = get_field_sql('SELECT i.ident
                                  FROM '.$CFG->prefix.'users u
                                  JOIN '.$CFG->prefix.'icons i ON i.ident = u.icon
                                  WHERE u.ident = ?',array($parameter->ident));
    }

    $run_result = $current;
?>
