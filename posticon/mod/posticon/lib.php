<?php

    function posticon_init()
    {
        global $CFG, $function;
        
        // Icon selection box
        $function['display:icon:select'][] = $CFG->dirroot . "mod/posticon/function_icon_select.php";

        // Funtion for getting the post icon
        $function['weblogs:posts:geticon'][] = $CFG->dirroot . "mod/posticon/weblogs_posts_geticon.php";

    }

    function posticon_pagesetup()
    {
    }

?>
