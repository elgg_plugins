<?php

    if (isset($parameter)) {
        global $CFG, $USER;

        $page_username = run("users:id_to_name", $USER->ident);

        // Current user icon variable
        $current = null;

        switch ($parameter[0]) {
            case "new_weblog_icon":
                $current = user_info("icon",$USER->ident);
                break;
            case "edit_weblog_icon":
                // Switch the icon
                $current = run("weblogs:posts:geticon",$parameter[1]);
                break;
        }

        $body = "";
        $postIcon = __gettext("Post icon");
        $postIconDesc = __gettext("A personal icon that will get displayed next to your weblog entry");

        // Get all icons associated with a user
        $icons = get_records('icons','owner',$USER->ident);

        // TODO apply style to the icon selection

        /* Styling idea

            div.figure {
              float: right;
              width: 25%;
              border: thin silver solid;
              margin: 0.5em;
              padding: 0.5em;
            }
            div.figure p {
              text-align: center;
              font-style: italic;
              font-size: smaller;
              text-indent: 0;
            }

        */
      
         if (!empty($icons)) {
    $body .= "<span>";
    $body .= "<table><tr>";
    $cellnum = -1;
            foreach ($icons as $icon) {
    	     $cellnum++;
                if ($cellnum % 4 == 0 && $cellnum > 0) {
                    $body .= "</tr><tr>";
                }
                if ($icon->ident == $current) {
                    $checked = 'checked="checked"';
                } else {
                    $checked = "";
                }
                $body .=  <<< END
    <td>
        <div class="figure">
            <p><img style="width: 50px;" alt="{$icon->description}" src="{$CFG->wwwroot}_icon/user/{$icon->ident}" />
                <label>$default <input type="radio" name="{$parameter[0]}" value="{$icon->ident}" {$checked} /></label></p>
                <p>{$icon->description}</p>
        </div>
    </td>
END;
            }
    $body .= "</tr></table>";
    $body .= "</span>";

        } else {
            $body .= "<p>".__gettext("You don't have any site pictures loaded yet.")." ";
            $body .= __gettext("Why not add some in your account preferences?")."</p>";
        }
        
        $run_result = templates_draw(array(
                                    'context' => 'databoxvertical',
                                    'name' => $postIcon . "<br />" . $postIconDesc,
                                    'contents' => $body)
                                
                                );

    }

?>
