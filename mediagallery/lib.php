<?php

    function mediagallery_pagesetup() {
    }
    
    function mediagallery_init() {
        
        global $CFG;
        $CFG->folders->handler["mediagallery"]['menuitem'] = __gettext("media gallery");
        $CFG->folders->handler["mediagallery"]['view'] = "mediagallery_folder_view";
        $CFG->folders->handler["mediagallery"]['preview'] = "mediagallery_folder_preview";
        
    }
    
    function mediagallery_folder_view($folder) {
        
        global $CFG, $metatags, $messages;
        
        require_once($CFG->dirroot.'lib/filelib.php');
        $metatags .= file_get_contents($CFG->dirroot . "mod/mediagallery/css");
        $metatags .= <<< END
        <script type="text/javascript">
            var elggWwwRoot = "{$CFG->wwwroot}";
        </script>
        <script type="text/javascript" src="{$CFG->wwwroot}mod/mediagallery/ufo.js"></script>
<script>
	var FO = {movie:"{$CFG->wwwroot}mod/mediagallery/mediaplayer.swf",id:"mediaplayer",name:"mediaplayer",width:"480",height:"560",majorversion:"8",build:"0",bgcolor:"#FFFFFF",allowfullscreen:"true",flashvars:"file={$CFG->wwwroot}mod/mediagallery/get_playlist.php?ident={$folder->ident}&autostart=false&overstretch=false&displayheight=360" };
	UFO.create(FO, "player");
</script>
		
		
		
		
        
END;
        
        $file_html = "";
        $media_html = "";
        $folder_html = "";
        
        
        
        // Get all the files in this folder
            if ($files = get_records_select('files',"folder = ? AND files_owner = ? ORDER BY time_uploaded desc",array($folder->ident,$folder->files_owner))) {
                
				$media_html .='<p id="player" align=center><a href="http://www.macromedia.com/go/getflashplayer">Get the Flash Player</a> to see this movie.</p>';
				
                foreach($files as $file) {
                    
                    if (run("users:access_level_check",$file->access) == true) {
                    
                        $image = $CFG->wwwroot . "_files/icon.php?id=" . $file->ident . "&amp;w=200&amp;h=200";
                        $filepath = $CFG->wwwroot . user_info("username",$file->files_owner) . "/files/$folder->ident/$file->ident/" . urlencode($file->originalname);
                        $image = "<a href=\"{$CFG->wwwroot}_files/icon.php?id={$file->ident}&w=500&h=500\"><img src=\"$image\" /></a>";
                        $fileinfo = round(($file->size / 1048576),4) . "Mb";
                        $filelinks = file_edit_links($file);
                        $uploaded = sprintf(__gettext("Uploaded on %s"),strftime("%A, %e %B %Y",$file->time_uploaded));
                        $keywords = display_output_field(array("","keywords","file","file",$file->ident,$file->owner));
                        $mimetype = mimeinfo('type',$file->originalname);
    
                        if (empty($file->title)) {
                            $file->title = __gettext("No title");
                        }
                        
						$media_html .= <<< END
                            
                            <div class="mediagallery-media-container">
							
							<a href="$filepath" >{$file->title}</a></h2>
                            <p>{$file->description}</p>
                                    <p>{$keywords}</p>
                                    <p>{$uploaded}<br />
                                        {$fileinfo} {$mimetype} {$filelinks}
                                    </p>
                            </div>
                            
END;

                    }
                }
                
            }
            
            if ($subfolders = get_records_select('file_folders',"parent = ? AND files_owner = ? ORDER BY name desc",array($folder->ident,$folder->owner))) {
                foreach($subfolders as $subfolder) {
                    $folderlinks = file_folder_edit_links($subfolder);
                    $keywords = display_output_field(array("","keywords","folder","folder",$subfolder->ident,$subfolder->owner));
                    $filepath = $CFG->wwwroot . user_info("username",$folder->files_owner) . "/files/" . $subfolder->ident;
                    $folder_html .= <<< END
                    
                        <div class="mediagallery-file-container">
                            <div class="mediagallery-file-image">
                                <a href="{$filepath}"><img src="{$CFG->wwwroot}_files/folder.png" /></a>
                            </div>
                            <div class="mediagallery-file-info">
                                <h2 class="mediagallery-file-title"><a href="{$filepath}">{$subfolder->name}</a></h2>
                                <p class="mediagallery-file-keywords">
                                    {$keywords}
                                </p>
                                <p class="mediagallery-file-infobar">
                                    {$folderlinks}
                                </p>
                            </div>
                        </div>
                    
END;
                }
            }
            
            
            if (!empty($folder_html)) {
                $folder_html = "<h2>" . __gettext("Subfolders") . "</h2>" . $folder_html;
            }
            
            $body = $media_html . $file_html . $folder_html;
            if (empty($body)) {
                $body = "<p>" . __gettext("This folder is currently empty.") . "</p>";
            }
            
            return $body;
        
    }
    
    function mediagallery_folder_preview($folder) {
    }

?>