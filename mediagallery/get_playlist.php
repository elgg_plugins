<?php

		require_once('../../includes.php');
        global $CFG, $metatags, $messages;
        //require_once($CFG->dirroot.'includes.php');
        require_once($CFG->dirroot.'lib/filelib.php');
		
		$folder_ident=$_GET['ident'];
		//echo $folder_ident;
		
		$folder=get_record_select('file_folders',"ident = " . $folder_ident);
		//echo $folder;
		
        $output = <<< END
	
		<playlist version="1" xmlns="http://xspf.org/ns/0/">
		<trackList>
        
END;
        
        $file_html = "";
        $media_html = "";
        $folder_html = "";
        
        
        
        // Get all the files in this folder
            if ($files = get_records_select('files',"folder = ? AND files_owner = ? ORDER BY time_uploaded desc",array($folder->ident,$folder->files_owner))) {
                
                foreach($files as $file) {
                    
                    if (run("users:access_level_check",$file->access) == true) {
                    
                        $image = $CFG->wwwroot . "_files/icon.php?id=" . $file->ident . "&amp;w=200&amp;h=200";
                        $filepath = $CFG->wwwroot . user_info("username",$file->files_owner) . "/files/$folder->ident/$file->ident/" . urlencode($file->originalname);
                        $image = "<a href=\"{$CFG->wwwroot}_files/icon.php?id={$file->ident}&w=500&h=500\"><img src=\"$image\" /></a>";
                        $fileinfo = round(($file->size / 1048576),4) . "Mb";
                        $filelinks = file_edit_links($file);
                        $uploaded = sprintf(__gettext("Uploaded on %s"),strftime("%A, %e %B %Y",$file->time_uploaded));
                        $keywords = display_output_field(array("","keywords","file","file",$file->ident,$file->owner));
                        $mimetype = mimeinfo('type',$file->originalname);
						$file_owner = user_info("name",$file->owner);
						
    
                        if (empty($file->title)) {
                            $file->title = __gettext("No title");
                        }
                        
                        if ((substr_count($mimetype, "image") > 0) || (substr_count($mimetype, "mp3") > 0) || (substr_count($mimetype, "flash") > 0) || (substr_count($mimetype, "x-flv") > 0) || (substr_count($file->originalname, ".flv") > 0)){
                            $media_html .= <<< END
                            
							<track>
							<title>$file->title</title>
							<creator>{$file_owner}</creator>

END;
                            if (substr_count($mimetype, "mp3") > 0){
                                $media_html .="<image>{$CFG->wwwroot}mod/mediagallery/images/mp3.png</image>";
                            }

                            if (substr_count($file->originalname, ".flv") > 0){
                                //$media_html .="<location>{$CFG->wwwroot}mod/mediagallery/stream.flv.php?ident={$file->ident}</location>";
								$media_html .="<location>{$filepath}</location>";
								
                            } else {
								$media_html .="<location>{$filepath}</location>";
							}
                            
                            
                            $media_html .= <<< END
                            </track>
END;
                            

                        }
                    }
                }
                
            }
            

            
            $body = $output . $media_html;
            if (empty($body)) {
                $body = "<p>" . __gettext("This folder is currently empty.") . "</p>";
            }
            
$body.= <<< END

	</trackList>
</playlist>
 
END;

header("content-type:text/xml;charset=utf-8");
echo $body; 

?>