<?php
/*
    Filecrawler plugin for Elgg
    Initial author: Sven Edge <sven@elgg.net> September 2006

    Plugin to crawl user-uploaded files, and
    - for files that exist on the filesystem but not in the db, either add to the user's root filestore folder or delete them from the filesystem
    - for files with entries in the db but not on the filesystem, purge them from the database
    
    Config options:
    
    $CFG->filecrawler_add_fs_orphans_to_db = true;
    $CFG->filecrawler_purge_fs_orphans = false;
    $CFG->filecrawler_purge_db_orphans = true;
    $CFG->filecrawler_fix_fs_permissions = true;
    
*/

    function filecrawler_pagesetup() {
        
    } // end function filecrawler_pagesetup
    
    
    
    function filecrawler_init() {
        
        // global $function;
        global $CFG;
        if (isset($CFG->filecrawler_purge_db_orphans) && $CFG->filecrawler_purge_db_orphans == false) {
            $CFG->filecrawler_purge_db_orphans = false;
        } else {
            $CFG->filecrawler_purge_db_orphans = true;
        }
        
        if (isset($CFG->filecrawler_fix_fs_permissions) && $CFG->filecrawler_fix_fs_permissions == false) {
            $CFG->filecrawler_fix_fs_permissions = false;
        } else {
            $CFG->filecrawler_fix_fs_permissions = true;
        }
        
        // default to adding, but purge if explicitly told to.
        if (isset($CFG->filecrawler_add_fs_orphans_to_db) && $CFG->filecrawler_add_fs_orphans_to_db == false) {
            if (empty($CFG->filecrawler_purge_fs_orphans)) {
                $CFG->filecrawler_purge_fs_orphans = false;
            } else {
                $CFG->filecrawler_purge_fs_orphans = true;
            }
        } else {
            $CFG->filecrawler_add_fs_orphans_to_db = true;
            $CFG->filecrawler_purge_fs_orphans = false;
        }
        
    } // end function filecrawler_init
    
    
    
    // called by cron.php
    function filecrawler_cron() {
        
        if (!empty($CFG->filecrawler_lastcron) && (time() - (60*60*24)) < $CFG->filecrawler_lastcron) {
            return true;
        } else {
            filecrawler_crawl();
        }
        
        set_config('filecrawler_lastcron',time());
        
    } // end function filecrawler_cron
    
    
    
    // perambulate the user filestore, or just one directory
    function filecrawler_crawl($limitinitial = "") {
        
        global $CFG;
        if (is_dir($CFG->dataroot . 'files/')) {
            chdir($CFG->dataroot . 'files/');
            
            $limitinitial = substr($limitinitial, 0, 1);
            if ($limitinitial) {
                $initials = array($limitinitial);
            } else {
                $initials = glob("*", GLOB_ONLYDIR); // GLOB_ONLYDIR only on windows php > 4.3.3
            }
            
            $userdirs = array();
            if (is_array($initials)) {
                foreach ($initials as $aninitial) {
                    chdir($CFG->dataroot . 'files/' . $aninitial);
                    $userdirs = array_merge($userdirs , glob("*", GLOB_ONLYDIR)); // GLOB_ONLYDIR only on windows php > 4.3.3
                }
            }
            
            foreach ($userdirs as $adir) {
                filecrawler_process_dir($adir);
            }
        }
        
    } // end function filecrawler_crawl
    
    
    
    // process a (user's) directory's files.
    function filecrawler_process_dir($username) {
        
        global $CFG;
        $parentdir = $CFG->dataroot . 'files/';
        $username = trim($username);
        $initial = substr($username, 0, 1);
        $userdir = $initial . '/' . $username . '/';
        
        if ($username && is_dir($parentdir . $userdir) && $userid = user_info_username('ident',$username)) {
            
            if ($CFG->filecrawler_purge_db_orphans == true) {
                $dbfiles = get_records('files', 'files_owner', $userid);
                foreach ($dbfiles as $key => $adbfile) {
                    if (!file_exists($CFG->dataroot . $adbfile->location)) {
                        
                        mtrace('Deleting files table entry for missing file ' . $adbfile->location . '.');
                        // how safe to be...?
                        //delete_records('files', 'ident', $adbfile['ident']);
                        delete_records('files', 'ident', $adbfile->ident, 'files_owner', $userid);
                        
                    }
                }
            }
            
            if ($CFG->filecrawler_add_fs_orphans_to_db || $CFG->filecrawler_purge_fs_orphans) {
                
                // query repetition less efficient but safer
                $dbfiles = get_records('files', 'files_owner', $userid);
                foreach ($dbfiles as $adbfile) {
                    $dbfilelocations[] = $CFG->dataroot . $adbfile->location;
                }
                
                //get list of files
                chdir($parentdir . $userdir);
                $fsfiles = glob("*");
                if (is_array($fsfiles)) {
                    foreach ($fsfiles as $afsfile) {
                        if (!in_array($parentdir . $userdir . $afsfile, $dbfilelocations)) {
                            if ($CFG->filecrawler_add_fs_orphans_to_db) {
                                
                                mtrace('Adding ' . $userdir . $afsfile . ' to files table.');
                                //add it
                                $f = new StdClass;
                                $f->owner = $userid;
                                $f->files_owner = $userid;
                                $f->folder = '-1';
                                $f->originalname = $afsfile;
                                $f->title = 'Auto-added file';
                                $f->description = '';
                                $f->location = 'files/' . $userdir . $afsfile;
                                if ($CFG->default_access == 'PRIVATE') {
                                    $f->access = 'user' . $userid;
                                } else {
                                    $f->access = $CFG->default_access;
                                }
                                $f->size = filesize($CFG->dataroot . $f->location);
                                $f->time_uploaded = time();
                                $file_id = insert_record('files', $f);
                                if ($file_id) {
                                    $rssresult = run("files:rss:publish", array($userid, false));
                                    $rssresult = run("profile:rss:publish", array($userid, false));
                                }
                                
                            } elseif ($CFG->filecrawler_purge_fs_orphans) {
                                
                                mtrace('Deleting ' . $userdir . $afsfile . '.');
                                unlink($CFG->dataroot . $afsfile);
                                
                            }
                            
                        } // end if
                    } // end foreach
                } // end if
            }
            
            
        } else {
            return false;
        }
        
        if ($CFG->filecrawler_fix_fs_permissions) {
            //mtrace('setting perms on ' . $parentdir . $userdir);
            chmod($parentdir . $userdir, $CFG->directorypermissions);
            chdir($parentdir . $userdir);
            $fsfiles = glob("*");
            if (is_array($fsfiles)) {
                foreach ($fsfiles as $afsfile) {
                    //mtrace('setting perms on ' . $parentdir . $userdir . $afsfile);
                    chmod($parentdir . $userdir . $afsfile, $CFG->filepermissions);
                }
            }
        }
        
    } // end function filecrawler_process_dir
        
?>