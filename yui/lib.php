<?php

require_once("config.php");

function yui_init() {
	global $CFG, $function;
	
	 // What is the user preference
        $function['userdetails:my_uses_YUI'][] = $CFG->dirroot . "mod/yui/yui_userdetails.php";

    // Action handling
        $function['userdetails:init'][] = $CFG->dirroot . "mod/yui/yui_userdetails_actions.php";

    // User details editable options
        $function['userdetails:edit:details'][] = $CFG->dirroot . "mod/yui/yui_userdetails_edit.php";
        
    if (isloggedin() && run("users:flags:get", array("my_uses_YUI", $_SESSION['userid'])) == 'no') {
	    // turn off YUI if the user has asked for that
	    $CFG->uses_YUI = 0;
    }
}

function yui_pagesetup() {
}
?>
