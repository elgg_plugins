<?php

    // Get the user preferences for the editor

    // Userid
    $id = (int) $parameter;

    // Editor is enabled by default
    $value = "yes";

    // Query result
    if ($result = user_flag_get('my_uses_YUI', $id)) {
        $value = $result;
    }

    $run_result = $value;
?>

