<?php

    // Save the user's editor choice
    $action = optional_param('action');
    $id = optional_param('id',0,PARAM_INT);
    $value = optional_param('my_uses_YUI');

    if (logged_on && !empty($action) 
        && run("permissions:check", array("userdetails:change",$id))) {
        if (!empty($value) && in_array($value,array('yes','no'))) {

            // Get the current value
            $current = run('userdetails:my_uses_YUI', $id);
            if ($current == $value) {
                // say nothing if the value has not changed
            } else {
                if (user_flag_set('my_uses_YUI', $value, $id)) {
	                if ($value == 'yes') {
                    	$messages[] .= __gettext("Your account has been changed to use the Yahoo user interface (YUI) library.");
                	} else {
	                	$messages[] .= __gettext("Your account has been changed to not use the Yahoo user interface (YUI) library.");
                	}
                } else {
                    $messages[] .= __gettext("Your Yahoo user interface (YUI) preference could not be changed");
                }
            }
        }
    }

?>