<?php

    $yui = __gettext("Yahoo user interface library");
    $yuiRules = __gettext("Set this to 'yes' if you would like to use the Yahoo user interface library (fancy Javascript effects).");

    $body = <<< END

    <h2>$yui</h2>
    <p>
        $yuiRules
    </p>

END;

    $use_yui = run('userdetails:my_uses_YUI', $page_owner);

    if ($use_yui == "yes") {
            $body .= templates_draw( array(
                    'context' => 'databox',
                    'name' => __gettext("Enable Yahoo user interface library: "),
                    'column1' => "<label><input type=\"radio\" name=\"my_uses_YUI\" value=\"yes\" checked=\"checked\" /> " . __gettext("Yes") . "</label> <label><input type=\"radio\" name=\"my_uses_YUI\" value=\"no\" /> " . __gettext("No") . "</label>"
            )
            );
    } else {
            $body .= templates_draw( array(
                    'context' => 'databox',
                    'name' => __gettext("Enable Yahoo user interface library: "),
                    'column1' => "<label><input type=\"radio\" name=\"my_uses_YUI\" value=\"yes\" /> " . __gettext("Yes") . "</label> <label><input type=\"radio\" name=\"my_uses_YUI\" value=\"no\" checked=\"checked\" /> " . __gettext("No") . "</label>"
            )
            );
    }


    $run_result .= $body;

?>
