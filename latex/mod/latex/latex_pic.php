<?php

global $CFG;
require_once(dirname(dirname(dirname(__FILE__)))."/includes.php");
require_once($CFG->dirroot."lib/iconslib.php");

if($id = optional_param('id',0,PARAM_CLEAN)){
	$mimetype = 'image/gif';
	$filepath = $CFG->dataroot . 'latex/pictures/' . $id . '.gif';
	
	spitfile_with_mtime_check($filepath,$mimetype);
}
	
?>
