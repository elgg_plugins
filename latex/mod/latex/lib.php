<?php

function latex_init() {
	global $CFG;

	include_once($CFG->dirroot . "mod/latex/class.latexrender.php");
	
	listen_for_event("weblog_post","process","latex_render");
}

function latex_pagesetup() {
}

function latex_render($object_type,$event,$text) {

    global $CFG;
    
    $latexrender_path = $CFG->dataroot . "latex";
    $latexrender_path_http = $CFG->wwwroot . "_latex";
    
    preg_match_all("#\[tex\](.*?)\[/tex\]#si",$text,$tex_matches);

    $latex = new LatexRender($latexrender_path."/pictures",$latexrender_path_http,$latexrender_path."/tmp");

    for ($i=0; $i < count($tex_matches[0]); $i++) {
        $pos = strpos($text, $tex_matches[0][$i]);

		// get formula and replace blank spaces
		$latex_formula = str_replace('&nbsp;', ' ', $tex_matches[1][$i]);

		// convert to new-line <p> and <br>
		$latex_formula = preg_replace('#<p\\s*?/??>#i', '', $latex_formula);
		$latex_formula = preg_replace('#</p\\s*?/??>#i', "\n", $latex_formula);
		$latex_formula = preg_replace('#<br\\s*?/??>#i', "\n", $latex_formula);
		
		// decode html entities
		$latex_formula = html_entity_decode($latex_formula);
		
        $url = $latex->getFormulaURL($latex_formula);

		$alt_latex_formula = htmlentities($latex_formula, ENT_QUOTES);
		$alt_latex_formula = str_replace("\r","&#13;",$alt_latex_formula);
		$alt_latex_formula = str_replace("\n","&#10;",$alt_latex_formula);

        if ($url != false) {
            $text = substr_replace($text, "<img src='".$url."' title='".$alt_latex_formula."' alt='".$alt_latex_formula."' align='absmiddle'>",$pos,strlen($tex_matches[0][$i]));
        } else {
	    // unparseable or potentially dangerous latex formula
            //$text = substr_replace($text, $tex_matches[1][$i],$pos,strlen($tex_matches[0][$i]));
        }
    }
    return $text;
}

?>
