<?php
/*
 * This script defines constants that allow to modify the messages plugin behavior
 * 
 * Created on Jun 10, 2007
 *
 * @author Diego Andrés Ramírez Aragón <diego@somosmas.org>
 * @copyright Corporación Somos Más - 2007
 */
 /**
  * Configure if the sidebar link display if available for no member communities
  * Type: boolean
  */
 define("MESSAGES_SIDEBAR_NO_MEMBER_LINK",false);
?>