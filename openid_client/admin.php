<?php

require_once(dirname(dirname(dirname(__FILE__)))."/includes.php");

// let admins configure the OpenID client

if (logged_on && run("users:flags:get", array("admin", $_SESSION['userid']))) {
	$action = optional_param('action');
	if ($action && $action == 'submit_form') {
		$always_sync = optional_param('always_sync');
		$default_server = trim(optional_param('default_server'));
		$greenlist = optional_param('greenlist');
		$yellowlist = optional_param('yellowlist');
		$redlist = optional_param('redlist');
		set_config('openid_client_default_server',$default_server);
		if ($always_sync) {
			set_config('openid_client_always_sync',$always_sync);
		} else {
			set_config('openid_client_always_sync','no');
		}		
		set_config('openid_client_greenlist',$greenlist);
		set_config('openid_client_yellowlist',$yellowlist);
		set_config('openid_client_redlist',$redlist);
		$messages[] = gettext('OpenID client configuration values saved.');
	}
	
	$body .= '<form action="" method="post">'."\n";
	$body .= '<input type="hidden" name ="action" value="submit_form" />'."\n";
	
	$body .= '<h2>'.gettext('Default server').'</h2>';
	$body .= gettext('You can simplify logging on using OpenID by specifying a default OpenID server. ');
	$body .= gettext('Users who enter a simple account name (eg. "susan") during an OpenID login can have it expanded to a full OpenID ');
	$body .= gettext('if you provide a default server here. Put "%s" where you want the account name added. For example, enter ');
	$body .= gettext('"http://openidserver.com/%s/" if you want the OpenID to become "http://openidserver.com/susan/" or ');
	$body .= gettext('"http://%s.openidserver.com/" if you want the OpenID to become "http://susan.openidserver.com/" . ');
	$body .= gettext('The presence of dots (".") is used to distinguish OpenID URLs from simple account names, ');
	$body .= gettext('so you can only use this feature for default servers that do not allow dots in their simple account names.');
	$body .= '<br /><br />';
	$body .= '<input type="text" size="60" name="default_server" value="'.$CFG->openid_client_default_server.'" />';
	$body .= '<h2>'.gettext('Server synchronisation').'</h2>';
	$body .= gettext('Check this box if you want to automatically update this client site if a user logs in and their email address or name ');
	$body .= gettext('is different from that on their OpenID server. Leave this box unchecked if you want to allow your users to ');
	$body .= gettext('have the ability to maintain a different name or email address on this system from the ones on their OpenID server.');
	$body .= '<br /><br />';
	if ($CFG->openid_client_always_sync == 'yes') {
		$checked = 'checked="checked"';
	} else {
		$checked = '';
	}
	$body .= '<input type="checkbox" name="always_sync" value="yes" '.$checked.' /> ';
	$body .= gettext('Automatically update from the OpenID server.');
	$body .= '<h2>'.gettext('OpenID lists').'</h2>';
	$body .= gettext('You can set up a green, yellow or red list of OpenIDs that this client will accept. ');
	$body .= '<br /><br />';
	$body .= gettext('The green list contains OpenIDs that will be accepted to provide identification and ');
	$body .= gettext('that can supply a trusted email address. ');
	$body .= '<br /><br />';
	$body .= gettext('The yellow list contains OpenIDs that will be accepted for identification only. ');
	$body .= gettext('If they provide an email address, a message will be sent to that address for confirmation before registration is allowed. ');
	$body .= '<br /><br />';
	$body .= gettext('The red list contains OpenIDs that should be rejected. ');
	$body .= '<br /><br />';
	$body .= gettext('If you do not provide a green, yellow or red list, by default all OpenIDs will be given a green status ');
	$body .= gettext('(they will be accepted for identification and email addresses that they provide will be accepted without confirmation). ');
	$body .= '<br /><br />';
	$body .= gettext('Put one OpenID entry on each line. You can use "*" as a wildcard character to match a number of possible OpenIDs or OpenID servers. ');
	$body .= gettext('Each OpenID must begin with http:// or https:// and end with a slash ("/") - eg. http://*.myopenid.com/');

	$body .= '<h2>'.gettext('Green list').'</h2>'."\n";
	$body .= '<textarea name="greenlist" rows="5" cols="60">'."\n";
	$body .= $CFG->openid_client_greenlist;
	$body .= '</textarea>'."\n";
	$body .= '<h2>'.gettext('Yellow list').'</h2>'."\n";
	$body .= '<textarea name="yellowlist" rows="5" cols="60">'."\n";
	$body .= $CFG->openid_client_yellowlist;
	$body .= '</textarea>'."\n";
	$body .= '<h2>'.gettext('Red list').'</h2>'."\n";
	$body .= '<textarea name="redlist" rows="5" cols="60">'."\n";
	$body .= $CFG->openid_client_redlist;
	$body .= '</textarea>'."\n";
	$body .= '<br /><br />';
	$body .= '<input type="submit" name ="submit" value="'.gettext('OK').'" />'."\n";
	$body .= '</form>'."\n";
	
	define("context", "admin");
	
	templates_page_setup();    
    
    echo templates_page_draw( array(
                    sitename,
                    templates_draw(array(
                                                    'body' => $body,
                                                    'title' => gettext("Configure OpenID client"),
                                                    'context' => 'contentholder'
                                                )
                                                )
            )
            );
    
}

?>
