<?php

// You may need to uncomment this to add the directory of the OpenID Auth and Services
// libraries if they are not in a standard PHP include place on your server

// set_include_path(get_include_path() . PATH_SEPARATOR . "/if needed/replace this with/the path to your/OpenID library includes/");

require_once(dirname(dirname(dirname(__FILE__))).'/includes.php');
require_once(dirname(dirname(dirname(__FILE__))).'/lib/validateurlsyntax.php');

really_require_once('Auth/OpenID/Consumer.php');
really_require_once('Auth/OpenID/MySQLStore.php');
really_require_once('Auth/OpenID/DatabaseConnection.php');

$activate_confirmation1 = __gettext("A confirmation message has been sent to ");
$activate_confirmation2 = __gettext(" Please click on the link in that message to activate your account. You will then be able to login using the OpenID you have supplied.");

$change_confirmation1 = __gettext("Your email address has changed. A confirmation message has been sent to your new address at ");
	
$change_confirmation2 = __gettext(". Please click on the link in that message to confirm this new email address. ");
			
$created_external_msg = __gettext("Created external account, transferred email, name from OpenID server:");

$email_in_use_msg = __gettext("Cannot change your email address because the new address is already in use.");

$invalid_code_msg = __gettext("Your form code appears to be invalid. Codes only last for seven days; it's possible that yours is older.");

/**
 * This function replaces require_once and attempts to fix a PHP bug
 * that interferes with require_once behavior when it is used inside a
 * function.  require_once, when used inside a function, only puts
 * functions and classes into the global namespace; global variables
 * in the required file are put into the local scope of the function
 * call, which is not what is desired.  Because this module is
 * required in the scope of another function, the bug occurs and we
 * have to put things into the global namespace manually.  Whee!
 */
 
function really_require_once($path) {
    require_once($path);
    foreach (get_defined_vars() as $k => $v) {
        $GLOBALS[$k] = $v;
    }
}

if (!function_exists('fnmatch')) {
function fnmatch($pattern, $string) {
   for ($op = 0, $npattern = '', $n = 0, $l = strlen($pattern); $n < $l; $n++) {
       switch ($c = $pattern[$n]) {
           case '\\':
               $npattern .= '\\' . @$pattern[++$n];
           break;
           case '.': case '+': case '^': case '$': case '(': case ')': case '{': case '}': case '=': case '!': case '<': case '>': case '|':
               $npattern .= '\\' . $c;
           break;
           case '?': case '*':
               $npattern .= '.' . $c;
           break;
           case '[': case ']': default:
               $npattern .= $c;
               if ($c == '[') {
                   $op++;
               } else if ($c == ']') {
                   if ($op == 0) return false;
                   $op--;
               }
           break;
       }
   }

   if ($op != 0) return false;

   return preg_match('/' . $npattern . '/i', $string);
}
}


/**
 * Emulates a PEAR database connection which is what the Auth_OpenID
 * library expects to use for its database storage.  This just wraps
 * Elgg's database abstraction calls.
 */
class Elgg_OpenID_DBConnection extends Auth_OpenID_DatabaseConnection {

    function setFetchMode($mode = 0) {
		global $db;
		global $ADODB_FETCH_ASSOC;
		
		if ($mode == 0) {
			$mode = $ADODB_FETCH_ASSOC;
		}
		
		return $db->SetFetchMode($mode);
    }

    function autoCommit($mode) {
        // Not implemented.
    }

    function query($sql, $params = array()) {
	    global $db;
	    $mode = $this->setFetchMode();
        $result = $db->Execute($sql, $params);
        $this->setFetchMode($mode);
        return $result;
    }

    function getOne($sql, $params = array()) {
        global $db;
	    $mode = $this->setFetchMode();
        $result = $db->GetOne($sql, $params);
        $this->setFetchMode($mode);
        return $result;
    }

    function getRow($sql, $params = array()) {
        global $db;
	    $mode = $this->setFetchMode();
        $result = $db->GetRow($sql, $params);
        $this->setFetchMode($mode);
        return $result;
    }

    function getAll($sql, $params = array()) {
        global $db;
	    $mode = $this->setFetchMode();
        $result = $db->GetAll($sql, $params);
        $this->setFetchMode($mode);
        return $result;
	}
}


/**
 * An Elgg-compatible store implementation that uses the
 * Elgg_OpenID_DBConnection.
 */
class OpenID_ElggStore extends Auth_OpenID_MySQLStore {

    function OpenID_ElggStore()
    {
	    global $CFG;
	    $this->nosync_table_name = $CFG->prefix."openid_client_nosync";
        $conn = new Elgg_OpenID_DBConnection();
        parent::Auth_OpenID_MySQLStore($conn,
                                       $CFG->prefix.'openid_client_consumer_settings',
                                       $CFG->prefix.'openid_client_consumer_associations',
                                       $CFG->prefix.'openid_client_consumer_nonces');
        $this->createTables();
    }
    
    function createTables() {
	    
	    parent::createTables();
	    $this->create_nosync_table();
   	}
   	
   	function _fixSQL() {
	   	global $CFG;
	   	parent::_fixSQL();
	   	$replacements = array(
                        'value' => $this->nosync_table_name,
                        'keys' => array('nosync_table',
                                        'add_nosync',
                                        'get_nosync',
                                        'remove_nosync')
                        );
	   	foreach ($replacements['keys'] as $key) {
		   	$this->sql[$key] = sprintf($this->sql[$key],$replacements['value']);
	   	}
   	}
   	
/**
 * If the data provided by an OpenID server differs from the data
 * stored on the client site, users may be asked if they want to 
 * update their client data each time they log on to the client.
 * The nosync table lists the idents of users who have asked to turn
 * this question off.
 */   	
   	
   	function create_nosync_table() {
	   	
	   	if (!$this->tableExists($this->nosync_table_name)) {
            $r = $this->connection->query($this->sql['nosync_table']);
            return $this->resultToBool($r);
        }
        return true;
   	}
   	
   	function getNoSyncStatus($ident) {
	   	
	   	$r = $this->connection->getOne($this->sql['get_nosync'],
                                        array($ident));
		return $this->resultToBool($r);
	}
	
	function addNoSyncStatus($ident) {
	   	return $this->connection->query($this->sql['add_nosync'],
                                        array($ident));
	}
	
	function removeNoSyncStatus($ident) {
	   	return $this->connection->query($this->sql['remove_nosync'],
                                        array($ident));
	}

    /**
     * Returns true if the specified value is considered an error
     * value.  Values returned from database calls will be passed to
     * this function to make decisions.
     */
    function isError($value)
    {
        return $value === false;
    }

    /**
     * This function is responsible for encoding binary data to make
     * it safe for use in SQL.
     */
    function blobEncode($blob)
    {
        return $blob;
    }

    /**
     * Given encoded binary data, this function is responsible for
     * decoding it from its encoded representation.  Some backends
     * will not return encoded data, like this one, so no conversion
     * is necessary.
     */
    function blobDecode($blob)
    {
        return $blob;
    }

    /**
     * This is called by the SQLStore constructor and gives the
     * implementor a place to specify SQL that is used to store and
     * retrieve specific data.  Here we call the parent class's setSQL
     * to get the basic SQL data for a MySQL store and overwrite a 
     * couple of pieces of SQL code that use the unsupported "!"
     * placeholder in the default MySQL store. We also add the SQL
     * to support the nosync table used by the Elgg OpenID client to
     * support OpenID client/server data syncronisation.
     */
     
    function setSQL()
    {
        parent::setSQL();
        
        $this->sql['create_auth'] =
            "INSERT INTO %s VALUES ('auth_key', ?)";
            
        $this->sql['set_assoc'] =
            "REPLACE INTO %s VALUES (?, ?, ?, ?, ?, ?)";
            
        $this->sql['nosync_table'] =
            "CREATE TABLE %s (ident INT(10) UNIQUE PRIMARY KEY) TYPE=InnoDB";
            
		$this->sql['get_nosync'] =
			"SELECT ident FROM %s WHERE ident = ?";
			
		$this->sql['add_nosync'] =
            "INSERT INTO %s (ident) VALUES (?)";
            
		$this->sql['remove_nosync'] =
            "DELETE FROM %s WHERE ident = ?";

    }
}

function openid_client_generate_i_code($prefix,$username,$ident,$email,$fullname) {
	$invite = new StdClass;
	$invite->name = $fullname;
	$invite->email = $email;	
	$invite->code = $prefix . substr(base_convert(md5(time() . $username), 16, 36), 0, 7);
	$invite->added = time();
	$invite->owner = $ident;
	insert_record('invitations',$invite);
	return $invite;
}

function openid_client_send_activate_confirmation_message($i_code) {
	global $CFG;
	
	$greetingstext = sprintf(__gettext("Thank you for registering with %s."),$CFG->sitename);
	$subjectline = sprintf(__gettext("%s account verification"),$CFG->sitename);
	$url = $CFG->wwwroot . "mod/openid_client/confirm.php?code=" . $i_code->code;
	$emailmessage = sprintf(__gettext("Dear %s,\n\n%s\n\nTo complete your registration, visit the following URL:\n\n\t%s\n\nwithin seven days.\n\nRegards,\n\nThe %s team.")
				,$i_code->name,$greetingstext,$url, $CFG->sitename);
	$emailmessage = wordwrap($emailmessage);
	email_to_user($i_code,null,$subjectline,$emailmessage);
}

function openid_client_send_change_confirmation_message($i_code) {
	global $CFG;
	
	$greetingstext = sprintf(__gettext("We have received a request to change your email address registered with %s."),$CFG->sitename);
	$subjectline = sprintf(__gettext("%s email change"),$CFG->sitename);
	$url = $CFG->wwwroot . "mod/openid_client/confirm.php?code=" . $i_code->code;
	$emailmessage = sprintf(__gettext("Dear %s,\n\n%s\n\nTo change your email address to {$i_code->email}, visit the following URL:\n\n\t%s\n\nwithin seven days.\n\nRegards,\n\nThe %s team.")
				,$i_code->name,$greetingstext,$url, $CFG->sitename);
	$emailmessage = wordwrap($emailmessage);
	email_to_user($i_code,null,$subjectline,$emailmessage);
}

$emailLabel = __gettext("Email:");
$nameLabel = __gettext("Name:");
$submitLabel = __gettext("Submit");
$cancelLabel = __gettext("Cancel");

function openid_client_generate_sync_form($new_email,$new_name, $user, $email_confirmation) {
	global $emailLabel, $nameLabel, $submitLabel, $cancelLabel;
	
	$old_email = $user->email;
	$old_name = $user->name;

	$noSyncLabel = __gettext("Do not notify me again if the data on this system is not the same as the data on my OpenID server.");
	$instructions = __gettext("The information on your Open ID server is not the same as on this system. Tick the check boxes next to the information you would like to update (if any) and press submit.");
	if ($new_email && $new_email != $old_email) {
		$change_fields .= '<table><tr><td><label for="emailchange"><input type="checkbox" id="emailchange" name="emailchange" value="yes" />'." $emailLabel</label></td><td>$old_email => $new_email</td></tr></table>\n";
		if (!$email_confirmation) {
			// the email address is from a green server, so we can change the email without a confirmation message
			// add an invitation code however to prevent this form from being forged
			// the user ident and new email address can then securely be stored in the database invitation table
			// rather than the form
			$i_code = openid_client_generate_i_code('c',$user->username,$user->ident,$new_email,$new_name);
			$form_stuff = '<input type="hidden" name="i_code" value="'.$i_code.'" />';
		} else {
			// the email will be confirmed anyway so it is safe to put it in the form
			$form_stuff .= <<< END
	        <input type="hidden" name="new_email" value="$new_email" />
END;
		}
			
	}
	if ($new_name && $new_name != $old_name) {
		$change_fields .= '<table><tr><td><label for="namechange"><input type="checkbox" id="namechange" name="namechange" value="yes" />'."$nameLabel</label></td><td>$old_name => $new_name</td></tr></table>\n";
	}
	$body .= <<< END
		$instructions
		<form action="sync.php" method="post">
	    <p>
	        $change_fields
	    </p>
	    <p>
	    	<label for="nosync"><input type="checkbox" id="nosync" name="nosync" value="yes" />$noSyncLabel</label>
	    	<br /><br />
	    	$form_stuff
	        <input type="hidden" name="new_name" value="$new_name" />
	        <input type="submit" name="submit" value="$submitLabel" />
	        <input type="submit" name="cancel" value="$cancelLabel" />
	    </p>
	</form>
	            
END;

	return $body;	
}


function openid_client_generate_missing_data_form($openid_url,$email = '',$fullname = '',$email_confirmation,$code) {
	global $emailLabel, $nameLabel, $submitLabel, $cancelLabel;
	
	$missing_email = __gettext("a valid email address");
	$missing_name = __gettext("your full name");
	$and = __gettext("and");
	$email_form = "<table><tr><td>$emailLabel</td><td><input type=".'"text" size="50" name="email" value=""></td></tr></table>';
	$name_form = "<table><tr><td>$nameLabel</td><td><input type=".'"text" size="50" name="name" value=""></td></tr></table>';
	$email_hidden = '<input type="hidden" name="email" value="'.$email.'"  />'."\n";
	$name_hidden = '<input type="hidden" name="name" value="'.$fullname.'"  />'."\n";
	
	if (!$email && !$fullname) {
		$missing_fields = $missing_email.' '.$and.' '.$missing_name;
		$visible_fields = $email_form.'<br />'.$name_form;
		$hidden_fields = '';
	} elseif (!$email) {
		$missing_fields = $missing_email;
		$visible_fields = $email_form;
		$hidden_fields = $name_hidden;
	} elseif (!$fullname) {
		$missing_fields = $missing_name;
		$visible_fields = $name_form;
		$hidden_fields = $email_hidden;
	}
	
	$hidden_fields .= '<input type="hidden" name="code" value="'.$code->code.'"  />'."\n";
	
	$instructions = __gettext("In order to create an account on this site you need to supply $missing_fields. Please enter this information below.");
		 
	$body .= <<< END
		$instructions
		<form action="missing.php" method="post">
	    <p>
	        $visible_fields
	    </p>
	    <p>
	        $hidden_fields
	        <input type="submit" name="submit" value="$submitLabel" />
	        <input type="submit" name="cancel" value="$cancelLabel" />
	    </p>
	</form>
	            
END;

	return $body;	
}

function openid_client_check_email_confirmation($openid_url) {
	global $CFG;
	
	$done = false;	
	$email_confirmation = false;
	
	if ($CFG->openid_client_greenlist) {		
		foreach (explode("\n",$CFG->openid_client_greenlist) as $entry ) {
			if (fnmatch($entry,$openid_url)) {
				$email_confirmation = false;
				$done = true;
				break;
			}
		}
	}
	if (!$done && $CFG->openid_client_yellowlist) {		
		foreach (explode("\n",$CFG->openid_client_yellowlist) as $entry ) {
			if (fnmatch($entry,$openid_url)) {
				$email_confirmation = true;
				break;
			}
		}
	}
	return $email_confirmation;
}

function openid_client_create_external_user($openid_url,$email, $fullname, $email_confirmation) {
	
	global $messages;
	
	if ($email && record_exists('users','email',$email)) {
		$messages[] = __gettext("Cannot create an account with the email address $email because it is already in use.");
		return 0;
	} else {					
					    
	    $u = new StdClass;
		$u->name = $fullname;
		$u->email = $email;
		$u->alias = $openid_url;
		$u->user_type = 'external';
		if ($email_confirmation) {
			$u->active = 'no';
		} else {
			$u->active = 'yes';
		}
	}						
		
	$id = insert_record('users',$u);
	if ($id) {
    	$u->ident = $id;
    	$u->username = "ext".$id;
	}
	
	update_record('users',$u);
	
	return $id;
}
?>