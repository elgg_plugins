<?php

require_once(dirname(dirname(dirname(__FILE__)))."/includes.php");
require_once('openid_includes.php');

$code = optional_param('code');
$name = trim(optional_param('name'));
$email = trim(optional_param('email'));
$error = false;
if (!$name) {
	$messages[] = gettext("You must provide a name.");
	$error = true;
}
if (!$email || !validateEmailSyntax($email)) {
	$messages[] = gettext("You must provide a valid email address.");
	$error = true;
}

if (empty($code) || !($details = get_record('invitations','code',$code))) {
	$messages[] = $invalid_code_msg;
	$error = true;
}

if (!$error) {
	// looks good	
	
	if ($code{0} == 'a') {
		// need to confirm first
		$details->email = $email;
		set_field('invitations','email',$email,'ident',$details->ident);
		$details->name = $name;
		set_field('invitations','name',$name,'ident',$details->ident);
		openid_client_send_activate_confirmation_message($details);
		$messages[] = $activate_confirmation1 . $email . $activate_confirmation2;
	} elseif ($code{0} == 'n') {
		//activate and login
		$messages[] = $created_external_msg." $email, $name";
		set_field('users','name',$name,'ident',$details->owner);
		set_field('users','active','yes','ident',$details->owner);
		$user = get_record_select('users',"ident = ? AND user_type = ? ", array($details->owner,'external'));
		$USER = init_user_var($user);
	}
	$_SESSION['messages'] = $messages;
	header("Location: " . $CFG->wwwroot);
	exit;
} elseif ($details) {
	// regenerate the form
	$user = get_record_select('users',"ident = ? AND user_type = ? ", array($details->owner,'external'));
	$openid_url = $user->username;
	$email_confirmation = openid_client_check_email_confirmation($openid_url);
	$body = openid_client_generate_missing_data_form($openid_url,$email,$fullname,$email_confirmation,$code);
	
	define("context", "OpenID register");
	
	templates_page_setup();    
	
	echo templates_page_draw( array(
	                sitename,
	                templates_draw(array(
	                                                'body' => $body,
	                                                'title' => gettext("OpenID information"),
	                                                'context' => 'contentholder'
	                                            )
	                                            )
	        )
	        );
} else {
	// bad code - not much to do but inform user
	$_SESSION['messages'] = $messages;
	header("Location: " . $CFG->wwwroot);
	exit;
}



?>