<?php

require_once(dirname(dirname(dirname(__FILE__)))."/includes.php");
require_once('openid_includes.php');

if (isloggedin()) {
 
	$userid = $USER->ident;
	$namechange = optional_param('namechange');
	$emailchange = optional_param('emailchange');
	$nosync = optional_param('nosync');
	
	if ($namechange) {
		$name = optional_param('new_name');
		set_field('users','name',$name,'ident',$userid);
		$USER->name = $name;
		$messages[] = gettext("Your name has been updated.");
	}
			
	if ($emailchange) {
		$i_code = optional_param('i_code');
		if (empty($i_code)) {
			$new_email = optional_param('new_email');
			// this is an email address change request from a yellow OpenID, so the
			// email address change must be confirmed with an email message
			if (count_records('users','email',$new_email)) {
				$messages[] = $email_in_use_msg;
			} else {
				$i_code = openid_client_generate_i_code('c',$USER->username,$userid,$new_email,$USER->name);
				openid_client_send_change_confirmation_message($i_code);
				$messages[] = $change_confirmation1 . $new_email. $change_confirmation2;
			}
		} elseif (!$details = get_record('invitations','code',$i_code)) {
			$messages[] = gettext("Your email change code appears to be invalid. Codes only last for seven days; it's possible that yours is older.");
		} else {
			// this is an email address change request from a green OpenID, so the
			// email address change does not need to be confirmed
			
			$email = $details->email;
			$ident = $details->owner;
			if (count_records('users','email',$email)) {
				$messages[] = $email_in_use_msg;
			} else {
				set_field('users','email',$email,'ident',$ident);
				$messages[] = gettext("Your email address has been updated.");
			}
		}
	}
	
	if ($nosync) {
		$store = new OpenID_ElggStore();
		$store->addNoSyncStatus($userid);
	}
}

$_SESSION['messages'] = $messages;
header("Location: " . $CFG->wwwroot);
exit;
?>