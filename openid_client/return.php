<?php

/**
 * Callback for return_to url redirection. The identity server will
 * redirect back to this handler with the results of the
 * authentication attempt.
 */
 
require_once('openid_includes.php');

$store = new OpenID_ElggStore();
$consumer = new Auth_OpenID_Consumer($store);

$query = array_merge( $_GET, $_POST );

// TODO - handle passthru_url properly
// $dest = $query['destination'];
$response = $consumer->complete($query);

if ($response->status == Auth_OpenID_CANCEL) {
    $messages[] = __gettext("OpenID authentication cancelled.");
} else if ($response->status != Auth_OpenID_SUCCESS) {
    $messages[] = __gettext("OpenID authentication failed (status: {$response->status}, url: {$response->url}, message: {$response->message} )");
} else { // SUCCESS.
	$openid_url = $response->identity_url;
	
    // Look for sreg data.
    $sreg = $response->extensionResponse('sreg');
	if ($sreg) {
        $email = trim($sreg['email']);
        $fullname = trim($sreg['fullname']);
    }
	$user = get_record_select('users',"alias = ? AND user_type = ? ", array($openid_url,'external'));
	if (!$user || $user->active == 'no') {
		if (!$user) {
			// this account does not exist
	    	if (!$email || !validateEmailSyntax($email)) {
				if ($ident = openid_client_create_external_user($openid_url,$email, $fullname, true)) {
		    		$code = openid_client_generate_i_code('a',$openid_url,$ident,$email,$fullname);
	    			$body = openid_client_generate_missing_data_form($openid_url,'',$fullname,true,$code);
				}
				$missing_data = true;
			} elseif (!$fullname) {
				$email_confirmation = openid_client_check_email_confirmation($openid_url);
				if ($email_confirmation) {
					$prefix = 'a';
				} else {
					$prefix = 'n';
				}
				// create the account
				if ($ident = openid_client_create_external_user($openid_url,$email, $fullname, $email_confirmation)) {
					$code = openid_client_generate_i_code($prefix,$openid_url,$ident,$email,$fullname);
					$body = openid_client_generate_missing_data_form($openid_url,$email,'',$email_confirmation,$code);
				}
				$missing_data = true;
			} else {
				// email address and name look good 
				
				$login = false;			
							    
			    // create a new account
				   
			   	$email_confirmation = openid_client_check_email_confirmation($openid_url);		    					    
			    							
				$ident = openid_client_create_external_user($openid_url,$email, $fullname, $email_confirmation);
				$missing_data = false;
			}
		} else {
			// this is an inactive account
			$ident = $user->ident;
			// assume that the account is inactive because the user
			// previously made an unconfirmed registration attempt
			// and that confirmation is still required
			$email_confirmation = true;
			$missing_data = false;
		}
		if ($ident && !$missing_data) {
				
			if ($email_confirmation) {
				$i_code = openid_client_generate_i_code('a',$openid_url,$ident,$email,$fullname);
				openid_client_send_activate_confirmation_message($i_code);
				$messages[] = $activate_confirmation1 . $email . $activate_confirmation2;
			} else {
				$messages[] = $created_external_msg." $email, $fullname";
				$login = true;
			}
		}
				
	} else {
		
		// account is active, check to see if this user has been banned
	
	    if (run("users:flags:get", array("banned", $user->ident))) { // this needs to change.
	        $ok = false;
	        $user = false;
	        $USER = false;
	        global $messages;
	        $messages[] = __gettext("You have been banned from the system!");
	    } else {
		    // user has not been banned
		    // check to see if email address has changed
		    if ($email && $email != $user->email && validateEmailSyntax($email)) {
			    // the email on the OpenID server is not the same as the email registered on this local client system
			    $email_confirmation = openid_client_check_email_confirmation($openid_url);
			    if ($CFG->openid_client_always_sync == 'yes') {
				    // this client always forces client/server data syncs
				    if ($fullname) {
				    	set_field('users','name',$fullname,'ident',$user->ident);
			    	}
				    if ($email_confirmation) {
					    // don't let this user in until the email address change is confirmed
					    $login = false;
					    $i_code = openid_client_generate_i_code('c',$openid_url,$user->ident,$email,$fullname);
					    openid_client_send_change_confirmation_message($i_code);
					    $messages[] = $change_confirmation1 . $email . $change_confirmation2;
					} else {
						$login = true;
						if (count_records('users','email',$email)) {
							$messages[] = __gettext("Cannot change your email address to $email because it is already in use.");
						} else {
							set_field('users','email',$email,'ident',$user->ident);
							$messages[] = __gettext("Your email address has been updated to $email");
						}
					}
				} else {
					$login = true;
					if (!$store->getNoSyncStatus($user->ident)) {
						// the following conditions are true:
						// the email address has changed on the server,
						// this client does not *require* syncing with the server,
						// but this user has not turned off syncing
						// therefore the user needs to be offered the chance to sync his or her data
						$body = openid_client_generate_sync_form($email,$fullname,$user,$email_confirmation);
					}
				}
			} elseif ($fullname && $fullname != $user->name) {
				// the fullname on the OpenID server is not the same as the name registered on this local client system
				$login = true;
				if ($CFG->openid_client_always_sync == 'yes') {
				    // this client always forces client/server data syncs
				    set_field('users','name',$fullname,'ident',$user->ident);
				} else {
					if (!$store->getNoSyncStatus($user->ident)) {
				    	// the following conditions are true:
						// the fullname has changed on the server,
						// this client does not *require* syncing with the server,
						// but this user has not turned off syncing
						// therefore the user needs to be offered the chance to sync his or her data
						$body = openid_client_generate_sync_form($email,$fullname,$user,false);
					}
				}
			} else {
				// nothing has changed or the data is null so let this person in
				$login = true;
			}
		}							    
	}
    
    if ($login) {
		
		// Set persistent cookie
		$rememberme = optional_param('remember',0);
		if (!empty($rememberme)) {
			remember_login($user->ident);
		}
	    
	    // log the user in
    
    	$user = get_record_select('users',"alias = ? AND active = ? AND user_type = ? ", array($openid_url,'yes','external'));    
    	$USER = init_user_var($user);
	}
} 

if($body) {

	define("context", "OpenID register");
	
	templates_page_setup();    
	
	echo templates_page_draw( array(
	                sitename,
	                templates_draw(array(
	                                                'body' => $body,
	                                                'title' => __gettext("OpenID information"),
	                                                'context' => 'contentholder'
	                                            )
	                                            )
	        )
	        );
} else {
	$_SESSION['messages'] = $messages;
	header("Location: " .$CFG->wwwroot);
	exit;
}

?>