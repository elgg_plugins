<?php

	global $CFG;
    global $page_owner;
    $url = $CFG->wwwroot;
        
    // If this is someone else's portfolio, display the user's icon
        if ($page_owner != -1) {
            // $run_result .= run("profile:user:info");
        }

    if ((!defined("logged_on") || logged_on == 0) && $page_owner == -1) {
	    
	    $where_am_i = 'http://'.$_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'];

        $body = '<li>';
        $body .= '<form action="'.$CFG->wwwroot.'mod/openid_client/login.php" method="post">';

        $body .= templates_draw(array(
                        'template' => -1,
                        'context' => 'sidebarholder',
                        'title' => __gettext("Log on using another service"),
                        'submenu' => '',
                        'body' => '
            <style>
				input.openid_login {
				background: url('.$url.'mod/openid_client/login-bg.gif) no-repeat;
				background-color: #fff;
				background-position: 0 50%;
				color: #000;
				padding-left: 18px;
				}
			</style>
            <table>
                <tr>
                    <td align="right"><p>
                        <label>' . __gettext("Service") . '&nbsp;<select name="externalservice">
                            <option value="">OpenID</option>
                            <option value="aim">AIM</option>
                            <option value="livejournal">LiveJournal</option>
                            <option value="vox">Vox</option>
                            <option value="pip">Verisign PIP</option>
                            <option value="explode">Explode</option>
                            <option value="wordpress">Wordpress.com</option>
                            </select>
                        </label>
                </tr>
                <tr>
                    <td align="right"><p>
                        <label>' . __gettext("Username") . '&nbsp;<input class="openid_login" type="text" name="username" id="username" style="size: 200px" /></label>
                    </td>
                </tr>
                <tr>
                    <td align="right"><p>
                        <input type="hidden" name="action" value="log_on" />
                        <input type="hidden" name="passthru_url" value="'.$where_am_i.'" />
                        <label>' . __gettext("Log on") . ':<input type="submit" name="submit" value="'.__gettext("Go").'" /></label><br />
                        <label><input type="checkbox" name="remember" checked="checked" />
                                '.__gettext("Remember Login").'</label>
                        </p>
                    </td>
                </tr>
            
            </table>

'
                    )
                    );
        $body .= "</form></li>";

        $run_result .= $body;
            
    }

?>
