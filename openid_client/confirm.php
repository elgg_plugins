<?php

require_once(dirname(dirname(dirname(__FILE__)))."/includes.php");
require_once('openid_includes.php');

$code = optional_param('code');
if (empty($code)) {
	$messages[] = gettext("Your confirmation code appears to be missing. Please check your link and try again.");
} elseif ($code{0} == 'a') {
	// request to activate an account
	$action = optional_param('action');
	$showform = false;
	if ($action && $action == 'submit_form') {
		$over13 = optional_param('over13');
		$username = optional_param('username');
		if (empty($over13)) {
			$messages[] = gettext("You must indicate that you are at least 13 years old to join.");
			$showform = true;
		} elseif (!$details = get_record('invitations','code',$code)) {
			$messages[] = $invalid_code_msg;
		} else {
			// OK, everything seems to be in order, so activate this user
			set_field('users','email',$details->email,'ident',$details->owner);
			set_field('users','name',$details->name,'ident',$details->owner);
			set_field('users','active','yes','ident',$details->owner);
			$username = get_field('users','username','ident',$details->owner);
			$alias = get_field('users','alias','ident',$details->owner);
			$messages[] = gettext("Your account was created! You can now log in using the OpenID ($alias) you supplied.");
		}
	} else {
		$showform = true;
	}
} elseif ($code{0} == 'c') {	
	// request to change an email address
	$showform = false;
	if (!$details = get_record('invitations','code',$code)) {
		$messages[] = $invalid_code_msg;
	} else {
		// OK, everything seems to be in order, so change the email address
		set_field('users','email',$details->email,'ident',$details->owner);
		$messages[] = gettext("Your email address has been changed to {$details->email} . "
		."You can now login using your OpenID if you are not already logged in.");
	}
}	

if ($showform) {
	$thankYou = gettext("Thank you for registering for an account with {$CFG->sitename}! "
	 ."Registration is completely free, but before you confirm your details,"
	 ." please take a moment to read the following documents:");
	$terms = gettext("terms and conditions"); // gettext variable
	$privacy = gettext("Privacy policy"); // gettext variable
	$age = gettext("Submitting the form below indicates acceptance of these terms. "
	."Please note that currently you must be at least 13 years of age to join the site."); // gettext variable
	$body .= <<< END
	    
	<p>
	$thankYou
	</p>
	<ul>
	<li><a href="{$CFG->wwwroot}content/terms.php" target="_blank">$sitename $terms</a></li>
	<li><a href="{$CFG->wwwroot}content/privacy.php" target="_blank">$privacy</a></li>
	</ul>
	<p>
	$age
	</p>
	
	<form action="" method="post">
	        
END;
	$correctAge = gettext("I am at least thirteen years of age."); // gettext variable
	$buttonValue = gettext("Join"); // gettext variable
	$body .= <<< END
	    <p align="center">
	        <label for="over13checkbox"><input type="checkbox" id="over13checkbox" name="over13" value="yes" /> <strong>$correctAge</strong></label>
	    </p>
	    <p align="center">
	        <input type="hidden" name="action" value="submit_form" />
	        <input type="hidden" name="invitecode" value="$code" />
	        <input type="submit" value="$buttonValue" />
	    </p>
	</form>
	            
END;
}

define("context", "OpenID confirmation");

templates_page_setup();    

echo templates_page_draw( array(
                sitename,
                templates_draw(array(
                                                'body' => $body,
                                                'title' => gettext("OpenID confirmation"),
                                                'context' => 'contentholder'
                                            )
                                            )
        )
        );


?>