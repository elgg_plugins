<?php

require_once('openid_includes.php');

$passthru_url = optional_param('passthru_url');

if ($passthru_url) {
	$redirect_url = $passthru_url;
} else {
	$redirect_url = $CFG->wwwroot . "index.php";
}

if (logged_on) {
	// if we're already logged in, say so and do nothing
    $messages[] = __gettext("You are already logged in.");
} else {
	$username = trim(optional_param('username'));
	$externalservice = optional_param('externalservice');
	
	if (!empty($externalservice)) {
        switch($externalservice) {
            
            case "livejournal":     $username = "http://" . $username . ".livejournal.com";
                                    break;
            case "aim":             $username = "http://openid.aol.com/" . $username;
                                    break;
            case "explode":         $username = "http://explode.elgg.org/" . $username;
                                    break;
            case "vox":             $username = "http://" . $username . ".vox.com";
                                    break;
            case "wordpress":       $username = "http://" . $username . ".wordpress.com";
                                    break;
            case "pip":             $username = "http://" . $username . ".pip.verisignlabs.com";
                                    break;
            
        }
    }
	
	if (!empty($username)) {
		
		// normalise username
		
		if (strpos($username,'.') === false) {
			// appears to be a bare account name, so try for a default server
			if ($CFG->openid_client_default_server) {
				$l = sprintf($CFG->openid_client_default_server,$username);
			}
		} elseif ((strpos($username,'http://') === false) && (strpos($username,'https://') === false)) {
			// allow for OpenID URLs that are missing the "http://" prefix
			$username = 'http://'.$username;
		}
		
		// add closing slash to normalise URL
	    //if (substr($username,-1,1) != "/") {
	    //	$username .= "/";
	    //}
	    
	    // Remove any malformed entries
	        delete_records('users', 'alias', $username, 'email', '');
	    
	    // try logging in
		$ok = openid_client_authenticate_user_login($username);
	    if ($ok) {
	        $messages[] = __gettext("You have been logged on.");
	    } 
	} else {
	    $messages[] = __gettext("The username was not specified. The system could not log you in.");
	}
}

$_SESSION['messages'] = $messages;
header("Location: " . $redirect_url);

function openid_client_authenticate_user_login($username) {
	
	global $CFG;
	global $messages;
	
	// match username against green, yellow and red lists	
	
	$passed = true;
	
	if ($CFG->openid_client_greenlist || $CFG->openid_client_yellowlist) {
		$passed = false;
		$yesarray = array_merge(explode("\n",$CFG->openid_client_greenlist),explode("\n",$CFG->openid_client_yellowlist));
		foreach ( $yesarray as $entry ) {
			if (fnmatch($entry,$username)) {
				$passed = true;
				break;
			}
		}
	}
	
	if ($passed) {
		if ($CFG->openid_client_redlist) {		
			foreach (explode("\n",$CFG->openid_client_redlist) as $entry ) {
				if (fnmatch($entry,$username)) {
					$passed = false;
					break;
				}
			}
		}
	}
	
	if (!$passed) {
		$messages[] = __gettext("This site does not allow the OpenID that you entered.");
		$messages[] = __gettext("Please try another OpenID or contact the site administrator for more information.");
		return false;
	}				
	
	$query = array_merge( $_GET, $_POST );

    $identity_url = $username;

    $consumer = new Auth_OpenID_Consumer(new OpenID_ElggStore());

    $auth_request = $consumer->begin($identity_url);

	if ($auth_request) {
        $trust_root = $CFG->wwwroot;

        // Add simple registration arguments.

        $auth_request->addExtensionArg('sreg', 'optional', 'email,fullname');
        
        $app_url = $CFG->wwwroot.'mod/openid_client/return.php';

        $redirect_url = $auth_request->redirectURL($trust_root, $app_url);

		session_write_close();
		if (headers_sent()) {
			die("headers sent!");
		}
        header( "Location: " . $redirect_url );
        exit;   
        
    } else {
        $messages[] = 'OpenID authentication failed: '.$username. ' is not a valid OpenID URL.';
    }

	return false;

}
?>