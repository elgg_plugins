<?php

function openid_client_init() {

	global $function;
	global $CFG;

// set search functions
    $function['display:sidebar'][] = $CFG->dirroot . "mod/openid_client/function_log_on_pane.php";
    
}

function openid_client_pagesetup() {
	global $CFG;
	global $PAGE;
	
	// Redirect profile links
	if (defined("context") && context == "profile") {
    	$page_owner = page_owner();
    	if (user_info("user_type",$page_owner) == "external") {
        	$alias = user_info("alias",$page_owner);
        	if (!empty($alias)) {
            	header("Location: {$alias}");
            	exit;
        	}
    	}
	}
	
	// set up admin page
	
	if (defined("context") && context == "admin") {
		$PAGE->menu_sub[] = array ( 'name' => 'openid_client:admin',
                                        'html' => "<a href=\"{$CFG->wwwroot}/mod/openid_client/admin.php\">"
                                        . gettext("Configure OpenID client") . '</a>');
	}
	
}

	
?>