OpenID client plugin

0.5 release
31 August 2006

This code has been adapted from example applications provided by
JanRain's OpenID PHP libraries, available from
http://www.openidenabled.com/openid/libraries/

This distribution includes the Auth and Services directories from
JanRain's PHP OpenID library.

If you install these directories somewhere other than in the standard
PHP include directories, then you may need to edit the include line at
the top of openid_includes.php in the openid_client directory to make sure
that the code can find the libraries.

This plugin adds an admin page that allows you to set various options,
including a default server to simplify logins, whether synchronisation with
OpenID servers is automatic or left to your users, which OpenIDs will
be accepted by your site, and how email confirmation will be handled.
More information can be found on the admin page.

This version of the OpenID client plugin has been tested with MySQL
but not yet Postgres.

Kevin Jardine
kevin AT radagast.biz
http://radagast.biz