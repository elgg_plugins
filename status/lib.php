<?php
/**
	 * A simple plugin which displays a users status in the profile box
	 * license GPL v2
	 * Copyright Curverider Ltd
	 * Initial author Dave Tosh <dave@curverider.co.uk>
**/

// sets up the plugin, leave blank
function status_pagesetup() {

}

// tells Elgg the widgets exists
function status_init() {
     
	 global $CFG, $metatags, $template;

	// Add meta tags 
	$metatags .= "<script type=\"text/javascript\" src=\"{$CFG->wwwroot}mod/status/js/edit.js\"><!-- status js --></script>";
        
	//include the css
	$template['css'] .= file_get_contents($CFG->dirroot . "mod/status/css");
    
	//set up the keyword for the pageshell
  	$CFG->templates->variables_substitute['statusdisplay'][]= "status_display";
  	
  	//if a user edits their status - capture the result
	if(optional_param("status:edit")) {
		status_set_values(optional_param("status_desc"));
	}
}

    function status_display() {
	    global $CFG, $page_owner;
	    $flag_name = "Status";
	    $body = '';

	    //function to get this users values
        $access = user_flag_get($flag_name, $page_owner);
		
		//Display the sidebar status box
		$body .= "<div id=\"status-box\">";
		$body .= "<p><b>$flag_name</b>: $access</p>";
		
		//find out if the current user owns the page and is logged in
		//then display the edit options

		if($page_owner == $_SESSION['userid']) {
			$body .= '<div class=\"presentation-contentslist\">';
			$body .= '<a href=javascript:toggleVisibility(\'presentationcontentslist\')>edit</a>';
			$body .= <<<END
				<div id="presentationcontentslist">
				<form action="" method="POST">
					<h3>Update your status:</h3>
					<input type="hidden" name="status:edit" value="true" />
END;
                //if statement to populate the textarea when someone already has a status comment
                if($access){
                    $body .= "<p><textarea name=\"status_desc\" value=\"\" rows=\"2\" cols=\"12\">$access</textarea>";
                }else {
                    $body .= "<p><textarea name=\"status_desc\" value=\"\" rows=\"2\" cols=\"12\"></textarea>";
                }
            
                $body .= <<<END
                <br /><input type="submit" value="submit" name="submit"></p>
				</form>
				</div>
				<script type="text/javascript">
				<!--
					toggleVisibility('presentationcontentslist');
				 -->
				</script>
END;
			$body .= '</div>';
		}

		$body .= "</div>";
		return $body;
}

function status_set_values($status_desc) {
	global $page_owner;
	$flag_name = "Status";

	$status_value = $status_desc;
	
	//put in a check to make sure that the only person who can edit is the
	//page owner (profile owner) in this case
	if($page_owner == $_SESSION['userid']) {
	
		//set the user flag with the correct values
		user_flag_set($flag_name, $status_value, $page_owner);

    }

}

?>