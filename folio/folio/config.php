<?php
// Folio Setup Options

global $CFG;
$FOLIO_CFG->version = 0.63;
$FOLIO_CFG->ajaxprefix = 'folio_control_tree_';

///////////////////////////////////////////////////////////////////////////////////////
//  WIKI OPTIONS
///////////////////////////////////////////////////////////////////////////////////////

// Default permission level for a new page.  Can be PUBLIC, MODERATED, or PRIVATE
$FOLIO_CFG->page_defaultpermission = 'MODERATED';

// Keep old pages in the rss/activity feed. AKA, if bob makes a change, and then tim, do you only want to see
// 	tim's?  If you want both, set to false, else true.
//	Please note that this doesn't change the page history records, those are always kept.  it's just about
//	exposing the record changes thru the feed system.
$FOLIO_CFG->page_deleteoldpagesfromrss = false;


///////////////////////////////////////////////////////////////////////////////////////
//  LOGGING OPTIONS
///////////////////////////////////////////////////////////////////////////////////////

// One way of improving Apache logs is to add the user's username to each entry.
// Some older versions of apache have problems with this, in which case you should
//      comment it out.
// Insert the following line into your httpd.conf file *****AFTER REMOVING the XXXXXXXXX section.***  
//      I had to add the XXXXXX as there is a php closing tag there.
//    LogFormat "Username:\"%{username}n\" %h %l %u %t \"%r\" %XXXXXXXXXXXXXXX>s %b \"%{Referer}i\" \"%{User-Agent}i\" " combined
    apache_note('username',$_SESSION['username']); 

// Folio has the option to keep track a historical record of user logons. 
// If you want to track this data, then set this option to "Y".  If you put it 
//	as 'N', then old logons will be deleted.
$FOLIO_CFG->track_logons = 'N';

    
///////////////////////////////////////////////////////////////////////////////////////
//  SIDEBAR DISPLAY OPTIONS
///////////////////////////////////////////////////////////////////////////////////////

// Disable  menu links. 
// If you do not want to disable certain options, leave the array empty.  Do not comment out
//	the variable.

// TOP MENU
// Some common options are 'feeds' <- subscriptions, 'folio' <- wiki page link, 'folio_feeds' <- activity
$FOLIO_CFG->disabled_topmenuitems = array(); 

// SIDE MENU
// Standard options are:  (include the path)
//  $CFG->dirroot . "mod/blog/lib/weblogs_user_info_menu.php"  <--- Elgg above v.8 has /mod/ instead of /units/
//  $CFG->dirroot . "mod/file/lib/files_user_info_menu.php"
//  These are normally disabled, as the folio side-menu replaces their functionality.  However, if you want these,
//      then remove them from the following array.  In addition, change the FOLIO_CFG->disabled_foliomenuitems variable
//      to keep that from showing them.
$FOLIO_CFG->disabled_sidemenuitems = 
    array( $CFG->dirroot . 'mod/blog/lib/weblogs_user_info_menu.php', // Elgg 8
           $CFG->dirroot . 'mod/file/lib/files_user_info_menu.php', // Elgg 8
		   $CFG->dirroot . 'units/weblogs/weblogs_user_info_menu.php', // Elgg 7
           $CFG->dirroot . 'units/files/files_user_info_menu.php' // Elgg 7
		   ); 
		
		
		
// If you are having troubles finding the particular include needed, set the following to true to print out the names of the included files.
$FOLIO_CFG->debug_print_sidemenu_includes = false; // true

// FOLIO MENU        
// This can be used to disable certain elements from the wiki folio sidemenu. As an example, if you decide to have
//      the normal blog sidebar shown, then you will want to not show the wiki blog link.
// Options ( 'weblog', 'files', 'profile', 'wiki' )
$FOLIO_CFG->disabled_foliomenuitems = array() ; //array( 'profile','weblog','files','wiki' ); 


///////////////////////////////////////////////////////////////////////////////////////
//  FRONTPAGE DISPLAY OPTIONS
///////////////////////////////////////////////////////////////////////////////////////


// Folio has the option to keep show a custom log-on page.  It will show
//      recent activity counts & global data.If you want to show this instead
//      of the default php value, then set this option to "Y".
$FOLIO_CFG->template_frontpage = 'N';

// Folio has the option to keep show a custom log-on page.  The following variable
//      shows a custom welcome message. {wwwroot}, {username} will be
//      automatically replaced with the correct values.
$FOLIO_CFG->template_frontpage_text = "<br/><b>Get started</b> by <a href='{wwwroot}profile/edit.php'><u>creating a profile</u></a>,
<a href='{wwwroot}_icons/'><u>uploading your picture</u></a>,
 or <a href='{wwwroot}{username}/weblog/'><u>posting a blog entry</u></a>.
<p align='center'><br/><a href='{wwwroot}activity/inbox/{username}/html/all/all/1'><img src='{wwwroot}mod/folio/image/inbox.png'   border=0/></a></p>";


?>