<?php
/**
* This file provides individual controls & formatting for the feeds.php file.
*
* @package folio
**/
    
//////////////////////////////////////////////////// 
// Helper Functions
//////////////////////////////////////////////////// 

    function rss_buildwhere( $fieldname, $good, $bad ) {
        $html = '';
        // add in good items
        // Assume that there is at least one.
        if ( count( $good ) > 0 & !in_array( 'all', $good ) ) {
            $html .= ' (';
            foreach ( $good as $i ) {
                $html .= " $fieldname = '$i' OR ";
            }
            // remove trailing OR
            
            $html = substr( $html, 0, strlen( $html ) - 3 ) . ' ) ';
        }

        // add in bad items
        if ( count( $bad ) > 0 ) {
            if ( strlen( $html ) > 0 ) { $html .= ' AND '; }
            
            $html .= ' (';
            foreach ( $bad as $i ) {
                $html .= " $fieldname <> '$i' AND ";
            }
            // remove trailing OR
            $html = substr( $html, 0, strlen( $html ) - 4 ) . ' ) ';
        }
        
        if ( strlen( trim( $html ) ) > 0 ) { $html = ' AND ' . $html; }
        return $html;

    }

/**
*Parse out a list of variables into a + and a - array.
*    ex: 
*        IN:   "sam+bob-jill+dave" 
*        OUT: plus(array) = ['sam', 'bob', 'dave'] and minus(array) = ['jill']
*
* @param $inarray string The incoming string list of arguments.
* @param OUTPUT $plus array Items that had a + by them
* @param OUTPUT $minus array Items that had a - by them.
**/
function folio_feed_parselist( $instring, &$plus, &$minus  ) {
    
    //     Break plus into array 
    $plus = explode( "+", $instring );

    $minus = '';
    array_walk(&$plus, 'folio_feed_parselist_removenegtags', &$minus);
    
    //     Remove leading - character (if present) as the function returns a string, not an array.
    if ( substr( $minus, 0, 1 ) == '-' ) { 
        $minus = substr( $minus, 1, strlen( $minus ) -1 ) ;
    }
    
    //     Break minus into array 
    if ( strlen( trim( $minus ) ) > 0 ) {
        $minus = explode( "-", $minus );
    } else {
        $minus = array();
    }

}
// Helper function for negative tags
//     Pulls labels with a - out from the array and returns them as a single string.
function folio_feed_parselist_removenegtags(&$item, $key, &$avoid) {
    if ( ! ( ($i = strpos( $item, '-')) === false) ) {
        $avoid .= substr( $item, $i );
        $item = substr( $item, 0, $i );
    }
}    

///////////////////////////////////////////////////////////////////////////////////////////
//  HTML VIEW
///////////////////////////////////////////////////////////////////////////////////////////

/**
* Return the html code for viewing the passed items.
*
**/
function folio_control_htmlfeed( $items, $itemcount, $page, $username, $summary ) {
    global $profile_id;
    global $page_owner;
    
    $perpage = 10;
    
    // Build $from variable
    $from = ($page-1) * $perpage;
    if ( $from > $itemcount ) { 
        error('Invalid page number ' . $page . ' passed to function folio_control_htmlfeed');
        die();
    }

    // Build top of page
    $body = folio_control_htmlfeed_header ($page, ceil( floatval( $itemcount) / $perpage ) ) .
        '<style type="text/css">
        .folioinfo { margin:10px 0 1px 1px;}</style>
        <div id="view_own_blog">';

    // Start list numbering (increment $from, as it's zero-based)
    //$body .= "<ol start='" . ( $from + 1 ) . "'>";
    
    // Add individual items. (false if empty)
    if ( $items ) {
        foreach ( $items as $item ) {
            $body .= folio_control_htmlfeed_item( $item, $summary );
        }
    } else {
        $body = 'No items to show.';
    }
    
    $body .= folio_control_htmlfeed_header ($page, ceil( floatval( $itemcount) / $perpage ) ) .
            '</div>'; 
        
    return $body;

}

// Format results
function folio_control_htmlfeed_item( $record, $summary ) {
    global $CFG;
    $html = '';
    $date = gmdate("D M d",intval($record->created));
    $type = str_replace( '_', ' ', $record->type );
    $body = ( $summary ? strip_tags( substr( $record->body, 0, 250 ) ) : $record->body );
    $iconid = intval( user_info("icon", $record->user_ident) == -1 ? 0 : user_info("icon", $record->user_ident)) ;
        
    $html .= <<< END
<div class="weblog-post">
    <div class="user">
        <a href="{$CFG->wwwroot}{$record->user_username}">
            <img alt="" src="{$CFG->wwwroot}_icon/user/{$iconid}"/>
        </a>
    </div>
    <div class="weblog-title">
        <h3><a href="{$record->link}">{$record->title}</a></h3>
    </div>
    <div class="folioinfo">
        <a href="{$CFG->wwwroot}{$record->owner_username}">{$record->owner_username}</a> | $type | $date<br/>
    </div>
    <div class="post">
        {$body}
        <br/>
        <h3><a href="{$record->link}">[More]</a></h3>
    </div>
</div>
<div class="clearing"></div> 
END;
    return $html;
}

// Build pagination control for top of page.
function folio_control_htmlfeed_header ($page, $pages ) {
        // Don't build header if there's only a single page.
        if ($pages == 1) { return '<br/>'; }
        
        $result = "<table border = 0 width=100% ><tr><td width=35% align=right>";

        if ( $page < $pages ) {
                $result .= "<a href='" . ($page + 1) . "'>&lt;&lt; Older</a> ";
        }

        $result .= "</td><td width=30% align=center><h2> Page $page of $pages </h2><td width=35% align=left>";
        
        if ( $page > 1 ) {
                $result .= "<a href='" . ($page - 1) . "'>Newer &gt;&gt;</a> ";
        }
        
        return $result . "</td></tr></table>" ; // Removed <table> as turned table into divs
    }    

///////////////////////////////////////////////////////////////////////////////////////////
//  RSS
///////////////////////////////////////////////////////////////////////////////////////////


/**
* Return the html code for viewing the passed items.
*
**/
function folio_control_rssfeed( $items, $itemcount, $arguments ) {

    $perpage = 2;
    $url = url;
    $rss_url = folio_feeds_url( $arguments, 1, array() );
    $base_url = folio_feeds_url( $arguments, 1, 
                    array(  
                        array( 'name'=>'format','value'=> 'summary'),
                        array( 'name'=>'format','value'=> $arguments['format'][0]))  );    
    
    // Build Header
    $body = <<< END
<?xml version="1.0"?>
<?xml-stylesheet type="text/xsl" href="{$url}news/rss/rssstyles.xsl"/?>
<rss version='2.0'   xmlns:dc='http://purl.org/dc/elements/1.1/'>
<channel xml:base='{$url}{$username}/weblog/'>
    <title><![CDATA[{$arguments['username'][0]} : RSS Feed]]></title>
    <description><![CDATA[RSS Feed showing {$arguments['mode'][0]} for {$arguments['username'][0]} using the Elgg software]]></description>
    <generator>Elgg</generator>
    <link>{$base_url}</link>
END;

// This just fixes my editor's syntax highlighting, which gets thrown off by the
//  xml declaration above.  NDG Feb 2007
//<?

    // Add individual items.
    if ( !( $items === false ) ) {
        // Only add if records are returned.
        array_walk( $items, 'folio_control_rssfeed_item', &$body );
    }
    
    // Build footer
    $body .= "    </channel></rss>";

    return $body;
}

// Format results
function folio_control_rssfeed_item( $result, $key, &$html ) {
        $url = url;
        $date = gmdate("m/d/y H:i",intval($result->created));
        
        $html .= <<< END
        
        <item>
            <title><![CDATA[{$result->title}]]></title>
            <link>{$result->link}</link>
            <guid isPermaLink="true">{$result->link}</guid>
            <pubDate>{$date}</pubDate>
            <description><![CDATA[{$result->body}]]></description>
        </item>
END;
    }

?>