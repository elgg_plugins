<?php
/**
* Edit a wiki page.
*
* @package folio
**/
include_once( 'page_edit_security.php' );
require_once('page_edit_move.php');

/**
* Edit a single wiki page (creating a new copy in the db, and marking the old one as not current).
* 	 or insert a new wiki page if no page_ident value is passed.
* Note: do not change html field names w/o also changing those in action_redirection.
*
* @package folio
* @param string $page The mysql page record.
* @param string $permissions The mysql permissions records.
* @param string $page_title The passed in title used to access the page.  Important
* 	in case we're building a new page.  Assumes that it has already been decoded by the function in lib.php
*	away from the URL form & into the normal presentation form.
* @param string $username The username of the page owner.  Used to create post link
* 	to the finished page.
* @param int $parentpage_ident The page being edited, or the parent page of the new page
* @todo Change way the pk for a new wiki page is created.
* @returns HTML code to edit a folio page.
**/
function folio_page_edit($page, $permissions, $page_title, $username, $tags, $parentpage_ident=-1) {
    global $CFG;
    global $profile_id;
    global $language;
    global $page_owner;
	global $metatags;
    $user = get_record('users','username',$username);
    $url = url;
    
    if (!$page && !isloggedin() ) {
		// We don't allow non-logged in users to create new pages
		error('You can not create a new page without logging in.');
	} elseif (!$page) {
        // Set values for the new page.

        // @TODO: Find a better random method, or at least verify that the page doesn't already exist.
        // Generate a new value for the page key.
		// If we change this, also update the stuff in lib that creates a new record.
        $page_ident = rand(1,999999999);
		// If this page is being created because of a hyperlink, then we
		//	already know what it should be named.  If not, then 
		//	set to something that obviously needs to be changed.
		if ( ! strlen( $page_title ) > 0 ) {
			$title = 'New Page Title';
		} else {
	        $title = $page_title; 
		}
        $body = 'Create your new page here!'; 

		// Translate passed username into a user ident.
        if ( $user === false ) {
			error('mod\folio\control\page_edit.php was called without a username parameter ' .
				"or with an invalid one ($username)");
		} else {
            $user_ident = $user->ident;
        }
        
		$page = new StdClass;
		$page->page_ident = $page_ident;
		$page->title = $title;
		$page->body = $body;
		$page->security_ident = $page_ident;
		$page->parentpage_ident = $parentpage_ident;
		$page->user_ident = $user_ident;
		$username = $username;		
        $created = 0;
		
    } else  {
        // Clean DB Entries, making them suitable for displaying.
        $page_ident = intval($page->page_ident);
        $body = stripslashes($page->body);
        $title = stripslashes($page->title);
        $parentpage_ident = intval($page->parentpage_ident);  
		$security_ident = intval($page->security_ident);
		$user_ident = intval($page->user_ident);
        $created = intval($page->created);
    }


	// Include javascript for javascript validation.
	$metatags .= <<< END
		<SCRIPT type="text/javascript" src="{$url}mod/folio/control/yav1_2_3/js_compact/yav.js"></SCRIPT>
		<SCRIPT type="text/javascript" src="{$url}mod/folio/control/yav1_2_3/js_compact/yav-config.js"></SCRIPT>
	    \n
<SCRIPT>
    var rules=new Array();
    rules[0]='page_title|regexp|^([A-Za-z0-9-_ ()]+)$|The Page title can only contain the following characters: ()-_';
</SCRIPT>
END;

    // Build control
    $run_result = <<< END
    
<form method="post" name="elggform" action="{$url}mod/folio/html/action_redirection.php" onsubmit="return performCheck('elggform', rules, 'classic');">

	<h2>{$page_title}</h2>
END;

	$run_result .= templates_draw(array(
                                'context' => 'databoxvertical',
                                'name' => 'Title',
                                'contents' => "<input type=text id='page_title' value='$title' name='page_title' style='width: 80%' />"
								) );

    $run_result .= templates_draw(array(
                                'context' => 'databoxvertical',
                                'name' => '',
                                'contents' => display_input_field(array("page_body",stripslashes($body),"weblogtext"))
                            )
                            );

    $run_result .= templates_draw(array(
                                'context' => 'databoxvertical',
                                'name' => 'Keywords (separate with commas)',
                                'contents' => "<input type=text id='page_tags' value='{$tags}' name='page_tags' style='width: 80%' />"
                            )
                            );



	// Include security
	$run_result .= folio_control_page_edit_security( $page, $user )
		. folio_control_page_edit_move( $page ) ;
        
        
	$run_result .= <<< END
	<p>
		<input type="hidden" name="action" value="folio:page:update" />
		<input type="hidden" name="username" value="{$username}" />
		<input type="hidden" name="user_ident" value="{$user_ident}" />
		<input type="hidden" name="page_ident" value="{$page_ident}" />
        <input type="hidden" name="created" value="{$created}" />
		<!-- <input type="hidden" name="parentpage_ident" value="{$parentpage_ident}" /> -->
		<input type="submit" value="Save" />
        <INPUT TYPE="button" VALUE="Cancel" onClick="javascript:history.back()">
	</p>
    </form>
END;

    return $run_result;
}
        
?>