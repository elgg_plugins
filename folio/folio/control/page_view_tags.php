<?php
/*
* View of list of tags for the current page (underlined with links)
*
* @package folio
* @param int $page_ident The page whose tags should be shown.
*/
function folio_page_view_tags( $page_ident ) {
    global $CFG;
    
    // Validate type.
 
    // Load tags for page
    $tags = recordset_to_array( get_recordset_sql( "SELECT ident, tag FROM " . $CFG->prefix . "tags ".
        "WHERE tagtype='page' AND ref = $page_ident ORDER BY tag" ) );

    if ( is_array( $tags ) ) {
        // User has logged on before
        foreach ( $tags as $tag ) {
            $t[] = '<a href="' . url . 'search/index.php?tag=' . $tag->tag . '">' . $tag->tag  . '</a>';
        }
        $tags = '<b>Keyword:</b> ' . implode( ', ', $t ) . '<br/><br/>';
    } else {
        // No tags
        $tags = '';
    }

    return $tags;
}
  
?>