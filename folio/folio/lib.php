<?php
/**
* Library of commonly-used functions
* @package folio
**/

require_once $CFG->dirroot . 'mod/folio/config.php';

/**
* Get the current version of this add-in.  This will always be a numeric value to allow for < or > comparisons.
**/
function folio_version() {
    global $FOLIO_CFG;
    return $FOLIO_CFG->version;
}


/**
* Listen for any api events
* Create the html code to overwrite the main page
* Record any log-on events
**/
function folio_init() {
    global $CFG;
    global $FOLIO_CFG;
    global $_SESSION;

    listen_for_event("weblog_post","delete","folio_rss_weblog_delete");
    listen_for_event("weblog_post","publish","folio_rss_weblog_post");
    listen_for_event("weblog_post","update","folio_rss_weblog_post");
    listen_for_event("weblog_comment","publish","folio_rss_weblog_comment_post");
	listen_for_event("weblog_comment","delete","folio_rss_weblog_comment_delete");
    listen_for_event("file","publish","folio_rss_file_post");
    listen_for_event("file","update","folio_rss_file_post");
	listen_for_event("file","delete","folio_rss_file_delete");
	
    // Test to see if there has been a log-on event
    $l = optional_param('username');
    $p = optional_param('password');

    if ( $FOLIO_CFG->track_logons == 'Y' OR $FOLIO_CFG->template_frontpage == 'Y' ) {
        // Test to see if a log-on event is going to occur thru cached credentials (or if they're already logged in, but
        //      the session variable with the replacement template code isn't being used).
        // Also test to see if a logon form has been submitted, in which case, re-test
        //    against authenticate & then record the logon event.
        if ( isloggedin() && !isset( $_SESSION['folio_template_frontpage'] ) ) {
            // Login via cached information.
            folio_record_logon();
        } else {
            // Test to see if a logon-form has been submitted.
            if (!empty($l) && !empty($p)) {
                if ( authenticate_account($l, $p) ) {
                    folio_record_logon();
                }
            }
        }
    }
}

/**
* Process stuff when a user first logs on
*
* Set $_SESSION variables
*   folio_template_frontpage: used later on to replace the frontpage_loggedin value to 
*       create a custom start page. Values replaced by folio_pagesetup.php
*   folio_lastlogon: used later for the inbox sidemenu w activity since last logon.
**/
function folio_record_logon() {
    global $_SESSION;
    global $FOLIO_CFG;

    // Include file with the frontpage content functions (as we're going to need to call them)
    require_once( 'frontpage/content_lib.php');

    // Generate the front-page text & save the generated html in a session variable
    //   This variable is then used later on to replace the $template['frontpage_loggedin'] var.
    $_SESSION['folio_template_frontpage'] = folio_template_frontpage();

    // Get last log-on from folio_history_logon table
    $logon = recordset_to_array( get_recordset_sql( "SELECT concat(user_ident, '.', created ) c1, " .
        "user_ident, created FROM " . $CFG->prefix . "folio_history_logon ".
        "WHERE user_ident = {$_SESSION['userid']} ORDER BY created DESC LIMIT 1" ) );

    if ( is_array( $logon ) ) {
        // User has logged on before
        $logon =  array_pop($logon);
        $_SESSION['folio_lastlogon'] = $logon->created;
    } else {
        // Hasn't logged on before.
        $_SESSION['folio_lastlogon'] = time();
    }

    $ul = new StdClass;
    $ul->user_ident = $_SESSION['USER']->ident;
    $ul->created = time();

    // Test to see if we're keeping a historical record of logons
    if ($FOLIO_CFG->track_logons == 'Y') {
        // Insert new record into db.
        insert_record('folio_history_logon',$ul);
    } else {
        // Delete previous
        delete_records('folio_history_logon', 'user_ident', $ul->user_ident);
        insert_record('folio_history_logon',$ul);
    }

}

/**
* Create the menu links used by the folio add-in, as well as setting the commands for the info / navigation menu
* on the side of the page.
*/
function folio_pagesetup() {
    global $profile_id;
    global $page_owner;
    global $PAGE;
    global $CFG;
    global $FOLIO_CFG;
    global $template;
    global $_SESSION;
    global $metatags;
    global $function;
    // These are defined by view.php, and are needed to properly setup the edit & history links for the submenu.
    global $page_ident;
    global $page_title;
    global $username; // this is who we're looking at, not the logged in user.
			// Depreciated in v.7?  Start referring to profile_id
	
  
    // This will change the log-on page to a custom session variable
    //  generated when the user first logged on.
    if ( $FOLIO_CFG->template_frontpage == 'Y' ) {
        // Don't show custom frontpge if the user isn't logged in.
        if ( isset( $_SESSION['folio_template_frontpage'] ) ) {
            $template['frontpage_loggedin'] = $_SESSION['folio_template_frontpage'];
        }
    }

    $currentusername = $_SESSION['username'];
	$currentuserident = $_SESSION['userid'];
	//var_dump( $_SESSION );
    
    // -----------------------------------------------
    // SETUP MENU
    //
    // Add Wiki Link
    if (isloggedin() & defined('context') ) {
        if ( substr(context, 0, 5) == "folio" & $page_owner == $currentuserident) {
                // Show selected
            $PAGE->menu[] = array( 'name' => 'folio', 
                'html' => '<li><a href="'.$CFG->wwwroot . $currentusername . '/page/" class="selected">Your Pages</a></li>');
		} else {
            // Show main menu unselected
            $PAGE->menu[] = array( 'name' => 'folio', 
                'html' => '<li><a href="'.$CFG->wwwroot . $currentusername . '/page/">Your Pages</a></li>');
        }
    } elseif ( isloggedin() ) {
        // Some pages don't have context defined.
        // Show link to folio.
        $PAGE->menu[] = array( 'name' => 'folio', 
            'html' => '<li><a href="'.$CFG->wwwroot . $currentusername . '/page/">Your Pages</a></li>');
    }

    // Add Activity Link (as first option)
    if (isloggedin() & defined('context') ) {
        if ( context == "feeds" & $page_owner == $currentuserident) {
            // Show selected
            array_splice( $PAGE->menu, 0,0, array( array( 'name' => 'folio_feeds', 
                'html' => '<li><a href="'.$CFG->wwwroot . 'activity/fac/' . $currentusername . '/summary/all/all/1" class="selected">Your Activity</a></li>')));
        } else {
            // Show main menu unselected
            array_splice( $PAGE->menu, 0,0, array( array( 'name' => 'folio_feeds', 
                'html' => '<li><a href="'.$CFG->wwwroot . 'activity/fac/' . $currentusername . '/summary/all/all/1" >Your Activity</a></li>')));
        
        }
    } elseif ( isloggedin() ) {
        // Some pages don't have context defined.
        // Show link to activity.
        array_splice( $PAGE->menu, 0, 0, array( array( 'name' => 'folio_feeds', 
            'html' => '<li><a href="'.$CFG->wwwroot . 'activity/fac/' . $currentusername . '/summary/all/all/1">Your Activity</a></li>')));
    }


    // -------------------------------------------------------------
    //     SETUP SIDE MENU
    //
    //    Modify side menu setup.  Remove the file & blog side menus in favor of a newly rebuilt & more compact
    //    version used in this mod.     
    
    folio_prunetopmenu();
    folio_prunesidemenu();

    // Insert 
    if ( defined('context') ) {
        switch (context) {
            case 'folio_page_view':
                // Viewing a page
                array_splice( $function['display:sidebar'], 2, 0, 
                    array(     $CFG->dirroot . "mod/folio/page_info_menu.php" ) );
                array_splice( $function['display:sidebar'], 3, 0, 
                    array(     $CFG->dirroot . "mod/folio/users_info_menu.php" ) );
                break;
            case 'folio_page_edit':
                // Editing a page.  Kill all sidebar entries.
                $function['display:sidebar'] = 
                    array( $CFG->dirroot . "mod/folio/page_edit_menu.php" );
                break;
            default:
                // Viewing normal areas of the website -- aka, not looking at pages.
                // Display link to the user's content.
                array_splice( $function['display:sidebar'], 2, 0, 
                    array( $CFG->dirroot . "mod/folio/users_info_menu.php" ) );
                break;
        }
    } else {
        // Viewing normal areas of the website -- aka, not looking at pages.
        // Display link to the user's content.
        array_splice( $function['display:sidebar'], 2, 0, 
            array( $CFG->dirroot . "mod/folio/users_info_menu.php" ) );
    
    }

}

/**
* Generic function used to remove an element from an array based off of the element's value (not key)
**/
function folio_delete_element( $array, $value ) {
    $i = 0;

    if ( !is_array( $array ) ) { return array(); }

    // Remove the element from the array.
    while ( !(($i = array_search( $value, $array )) === false) ) { 
        //THough not noted in documentation, array_splice has side effect of removing element from
        //      array as well as returning the removed section as a return value.
        array_splice( $array, $i, 1);
    }

    return $array;
}


/** 
* Used to generate a 'where' clause for a folio page retrieval operation. Used by tree.php.
* Note that by using this where clause, and running an innner join operation, you are opening the possiblity
*     of retrieving duplicate records.  To compensate, only retrieve the page values & slap a distinct on top
*    of the query.  It's not the most elegant way of handling dups, but should avoid triggering any bugs
*    from ADODB's keying of returned values by ID.
*
* @uses isloggedin 
* @uses $SESSION['userid']
* @param mixed $tableprefix The alias for the permission table being used.
* @param mixed $tableprefix The alias for the page table being used.
* @param string $level The level of permission that we're looking for...  Values (read, write, owner).
* @param int $profile_id The owner of the pages we're looking at.
* @return A where condition usable for getting content via joining folio_page and folio_page_security
*/
function folio_page_security_where( $tp_permission, $tp_page, $level, $profile_id ) {
    // TODO: Validate input.
    //$user_ident = required_param( $user_ident, int);
    global $CFG;
    $userid = array($_SESSION['userid']);
    
    // Build userid list, giving us a list of memberships.
    if ( isloggedin() ) {

        // Grab community IDs for friends.
        $groupid = recordset_to_array(
            get_recordset_sql( "SELECT * FROM " . $CFG->prefix . "friends f " .
                " INNER JOIN " . $CFG->prefix . "users u on f.friend = u.ident " .
                "WHERE f.owner = " . $_SESSION['userid'] . " AND u.user_type='community' ") );
                
        if ( !!$groupid ) {
            foreach ( $groupid as $id ) {
                $userid[] = $id->friend;
            }
        }
        
        // Grab community IDs for owned communities
        $groupid = recordset_to_array(
            get_recordset_sql( "SELECT * FROM " . $CFG->prefix . "users ".
                "WHERE user_type = 'community' AND owner = " . $_SESSION['userid'] ) );
        
        if ( !!$groupid ) {
            foreach ( $groupid as $id ) {
                $userid[] = $id->ident;
            }
        }
    }
    
    // Convert array userid list to a sql-able where clause.
    $userid = '(' . implode(', ', $userid ) . ')';
   
    // Validate
    if (!($level == 'read' | $level == 'write' | $level == 'owner' )) {
        error(' You can only pass the values read/write/owner to folio_page_security_where in mod\folio\lib.php ');
    }
    
    // Note: Use all upper-case for constants in sql.
    if (!isloggedin() && $level=='read') {
    
        return " ( $tp_permission.accesslevel = 'PUBLIC' " .
            " OR $tp_permission.accesslevel = 'MODERATED' ) ";
            
    } elseif (!isloggedin() && ($level=='write' || $level='owner') ) {
    
        return " ( $tp_permission.accesslevel = 'PUBLIC' ) "; 
        
    } elseif ($level == 'read') {
    
        return " ( $tp_permission.accesslevel = 'PUBLIC' " .
            " OR $tp_permission.accesslevel = 'MODERATED' " .
            " OR ( $tp_permission.accesslevel = 'PRIVATE' AND $tp_page.user_ident IN $userid ) ) ";

    } elseif ($level == 'write' | $level == 'owner') {
    
        return " ( $tp_permission.accesslevel = 'PUBLIC' " .
            "OR ( $tp_permission.accesslevel = 'MODERATED' AND $tp_page.user_ident IN $userid ) " .
            "OR ( $tp_permission.accesslevel = 'PRIVATE' AND $tp_page.user_ident IN $userid ) ) ";
            
    }

}

/**
* Translate the username title to an user record.
*    If the page isn't found, then return false
*
* @todo check that postgress where col = 'abc' is not case sensitive.
*
* @params string $user Username
* @return array MySQL Record.
**/
function folio_finduser( $user ) {
    global $CFG;
    
    // Make sure that incoming paramaters are clean 
    $user = folio_clean( trim( $user ) );

    $user = recordset_to_array(
        get_recordset_sql("SELECT * FROM " . $CFG->prefix . "users " .
        "WHERE LOWER(username) = '$user'")
        );            

    if ( $user ) {
        foreach ($user as $user) {
            // Return 
            return $user;
        }
    } else {
        // Since we didn't find a matching record, return false
        return false;
    }
}

/**
* Translate the page title to an ident.  Ignore security constraints, as this is a fairly safe operation that happens .htaccess rewriting urls.
*    If the page isn't found, then return -1
*
* @todo check that postgress where col = 'abc' is not case sensitive, as well as the LOWER and LIMIT function.
*
* @params string $user Username
* @params string $page The title of the page. Assumes that it has already been decoded by lib.php.
* @return int $page_ident If the page isn't found, then return -1
**/
function folio_page_translatetitle( $user, $page) {
    global $CFG;
    

    $pages = get_records_sql( "SELECT page_ident, p.title FROM " . $CFG->prefix . "folio_page p " .
        "INNER JOIN " . $CFG->prefix . "users u ON p.user_ident = u.ident " .
        "WHERE LOWER(p.title) = ? AND p.newest = 1 AND LOWER(u.username) = ? " . 
        ' LIMIT 1 ',
        array( $page, $user ) );
        
    if ( $pages ) {
        foreach ($pages as $page) {
            // Return page ident.
            return $page->page_ident;
        }
    } else {
        // Since we didn't find a matching record, return -1
        return -1;
    }
}

/**
* Remove any non alphanumeric characters in a string
* Legal Characters are a-z, A-Z, 0-9, -, _, and () and spaces
*
* @param string $input
* @param array $valid Valid characterrs, defaults to standard -_() and spaces.
* @return string Clean input
**/
function folio_clean( $input, $valid = array('(',')',' ','-','_',' ' )) {

    $input = trim( $input );
    
    // Test for an easy clean / validation.
		//Old Style::  str_replace( ' ', '', str_replace( ')', '', str_replace( '(', '', str_replace( '-', '', str_replace( '_', '', $input)) ) ) ) ) 
    if ( ctype_alnum( folio_clean_replace( $input, $valid ) ) ) {
        // Validated.  Return
        return $input;
    } else {
        // Step thru & clean each character.
        
        $retval = '';
        
        for ( $i = 0; $i < strlen($input); $i++ ) {
            $v = substr( $input, $i, 1 );
            
            if ( ctype_alnum( $v ) | in_array( $v, $valid ) ) {
                $retval .= $v;
            }
        }
        
        return $retval;
    }
}

/**
* Remove all of the valid_values in input  & return the value
**/
function folio_clean_replace( $input, $remove_values ) {
	foreach ( $remove_values as $value) {
		$input = str_replace( $value, '', $input );
	}
	return $input;
}


/**
* Same as folio_clean, except suitable for array_walk
**/
function folio_clean_array( &$result, $key ) {
    $result = folio_clean( $result);
}


/**
* Retrieve a page.  Ignores security restrictions, as these should be checked with the folio_page_permission function.
*
* @uses $_CFG
* @param int $page_ident
* @param int $created OPTIONAL, pass to pull up an older version of the page.
* @return array MYSQL record.
**/
function folio_page_select( $page_ident, $created = -1 ) {
    global $CFG;
    
    if ($created != -1) {
        // Get old page
        $pages = recordset_to_array(
            get_recordset_sql('SELECT * FROM ' . $CFG->prefix . 'folio_page ' .
                "WHERE page_ident = $page_ident AND created = $created LIMIT 1")
            );
            
    } else {
        // Get current page.
        $pages = recordset_to_array(
            get_recordset_sql('SELECT * FROM ' . $CFG->prefix . 'folio_page ' .
                "WHERE page_ident = $page_ident AND newest = 1 LIMIT 1")
            );
        
    }
    
    return $pages[$page_ident];
}
/**
* Retrieve a page's security records.
*
* @param int $page_ident
* @return array of MYSQL record in array form..
**/
function folio_page_security_select( $page_ident ) {
    global $CFG;
    
    // Create a fake first column to satisfy ADODB's requirement of a unique first field.
    $permissions = recordset_to_array(
        get_recordset_sql( "SELECT CONCAT(user_ident, '.', security_ident) jointkey, " .
            'user_ident, security_ident, accesslevel FROM ' . $CFG->prefix .
            'folio_page_security WHERE ' .
            "security_ident = $page_ident ORDER BY user_ident" ) );

    return $permissions;
}

/**
* Used to see the current user has permission to something.
*   May pull up community records to see if they have membership rights thru that.
*
* @uses $_SESSION[userid]
* @param array $page The page record being checked.  Should be from folio_page_select.  False if we're creating a new page.
* @param array $permissions An array of mysql records of permissions for the current page
* @param string $access The type of permission that we're asking about.  Valid options = read/write
* @param int OPTIONAL $opt_newpageowner An optional page location variable used for signaling where new pages will be located.  UserID of page's location
* @return bit Whether or not the given user has access.
**/
function folio_page_permission( $page, $permissions, $access, $opt_newpageowner = '') {
    global $CFG;
    global $_SESSION;
        
    // Test for non-logged in users
    if ( !isloggedin() ) {
    
        // See if there is a public permission level for write (or moderated for reader)
        if ( !$page & $access == 'write' ) {
            // Don't allow non-logged in users to create a new page.
            return false;
        } elseif ( $access == 'read' ) {
        if ( !!$permissions ) {
            foreach ($permissions as $permission) {
                if ( $permission->accesslevel == 'PUBLIC' |
                    $permission->accesslevel == 'MODERATED' ) {
                    return true;
                }
            }
        }
        } elseif ( $access == 'write' ) {
        if ( !!$permissions ) {
            foreach ($permissions as $permission) {
                if ( $permission->accesslevel == 'PUBLIC' ) {
                    return true;
                }
            }
            }
        } elseif ( $access == 'delete' ) {
                return false;
        }
        
        // Not found anything.  Return false - non-logged in user doesn't have access rights.
        return false;
    }
    
    // Continue, knowing that the user is logged in and we have a userid
    
    
    // I had problems directly accessing $_SESSION[userid] and found that
    //        assigning it to a variable solved my boolean evaluation problems
    //        with it.
    $userid = array( $_SESSION['userid'] );

    // Grab community IDs for friends.
    $groupid = recordset_to_array(
        get_recordset_sql( "SELECT * FROM " . $CFG->prefix . "friends f " .
            " INNER JOIN " . $CFG->prefix . "users u on f.friend = u.ident " .
            "WHERE f.owner = " . $_SESSION['userid'] . " AND u.user_type='community' ") );
            
    if ( !!$groupid ) {
        foreach ( $groupid as $id ) {
            $userid[] = $id->friend;
        }
    }
    
    // Grab community IDs for owned communities
    $groupid = recordset_to_array(
        get_recordset_sql( "SELECT * FROM " . $CFG->prefix . "users ".
            "WHERE user_type = 'community' AND owner = " . $_SESSION['userid'] ) );
    
    if ( !!$groupid ) {
        foreach ( $groupid as $id ) {
            $userid[] = $id->ident;
        }
    }

    // Look to see if the user has permission
    if ( !$page ) {
        // New page.  See if it in located in a place where the user has permission to access it.
        return ( in_array( $opt_newpageowner, $userid ) );
    } elseif ( $access == 'read' ) {
        // Page exists, see if we can read it.
        foreach ($permissions as $permission) {
            if ( $permission->accesslevel == 'PUBLIC' OR
                $permission->accesslevel == 'MODERATED' OR
                ( in_array( $page->user_ident, $userid)
                )
                ) {
                return true;
            }
        }
    } elseif ( $access == 'write' ) {
        // Page exists, see if we can edit it.
        foreach ($permissions as $permission) {
            if ( $permission->accesslevel == 'PUBLIC' OR
                ( in_array( $page->user_ident, $userid) )
                ) {
                return true;
            }
        }
    
    } elseif ( $access == 'delete' ) {
        // See if the user is a member / owner.
        return ( in_array( $page->user_ident, $userid ) );

    } else {
        error( 'Invalid ' . $access . ' passed to folio_page_permission');
    }
    // Didn't catcth anywhere, say no access.
    return false;
}



/**
* Translates [[Page Title]] to <a href=$url$username/page/$pagetitle/>>$pagetitle</a>.
*     Note that we don't have a decode, as this should only be called by the view procedure.
*    Encoded urls should not be stored in the database.
*
* Automatically removes invalid characters.
*
* Creates links to other user's wikis through [[username:page titles]]
*
* Adds in YOUTUBE links.  Should turn into an array instead of hardcoded, but this is
*   just a quick fix.
*
* @param string $username The username of the owner of the page.
* @param string $body The body that we're translating.
* @return string The revised body.
**/
function folio_page_makelinks( $username, $body ) {
    $i = true;
    $url = url;

    while ( $i === true ) {
        $i = stripos( $body, '[[' );
        $end = stripos( $body, ']]' );	
	$namespace = $username;
	$nametag = '';

        // Test find.
        if (!( $i === false | $end === false )) {
            // Translate title.

            $link = substr( $body, $i+2, $end -$i -2) ; 
            $link = folio_clean( $link, array( '(',')','-','_',':',' ') );
            // Find namespace for link.            
            if ( $temp = stripos( $link, ':') ) {
                $namespace = substr( $link, 0, $temp );
                $link = substr( $link, $temp+1, strlen($link) - $temp );
		$nametag = $namespace  . ':';
            }

            $body = substr( $body, 0, $i ) .
                "<a href=\"{$url}{$namespace}/page/" .
                folio_page_encodetitle($link) . 
                "\">{$nametag}{$link}</a>" . 
                substr( $body, $end+2 );
            
            $i = true;
        }
    }

    $i = true;
    while ( $i === true ) {
        $i = stripos( $body, '{{' );
        $end = stripos( $body, '}}' );
        
        // Test find.
        if (!( $i === false | $end === false )) {
            // Translate title.
            $link = substr( $body, $i+10, $end -$i -10);
            $body = substr( $body, 0, $i ) .
                '<object width="425" height="350"><param name="movie" value="http://www.youtube.com/v/' .
                ($link) . '"></param><param name="wmode" value="transparent"></param>' .
                '<embed src="http://www.youtube.com/v/' . ($link) . '" type="application/x-shockwave-flash"' .
                'wmode="transparent" width="425" height="350"></embed></object>' . 
                substr( $body, $end+2 );
            
            $i = true;
        }
    }
    
    return $body; 
    
}

/**
* Used to encode a page title.  Strips out illegal characters to make it suitable for use in linking.
*
* @param string Original title
* @return string Encoded title
**/
function folio_page_encodetitle( $title ) {
    //return str_replace( ' ', '_', trim($title) );
    return urlencode( $title );
}

/**
* Used to decode a page title.  
*
* @param string Encoded title
* @return string Original title
**/
function folio_page_decodetitle( $title ) {
    //return str_replace( '_', ' ', $title );
    return urldecode( $title );
}



/**
* Retrieve the homepage for the passed user.
*    If one hasn't been created, will return false
*
* @param string $username.  Assume that it is in normal form (it has not been encoded for html addressing).
* @return array of MYSQL record in array form.  FALSE if not found.
**/
function folio_homepage( $username ) {
    global $CFG;

    $pages = recordset_to_array(
            get_recordset_sql('SELECT p.*, u.username FROM ' . $CFG->prefix . 'folio_page p inner join ' .
                $CFG->prefix . 'users u on p.user_ident = u.ident ' .
                "WHERE p.page_ident = p.parentpage_ident AND u.username = '$username' AND newest = 1 LIMIT 1")
            );

    if ( !$pages ) {
        return false;
    } else {
        foreach ($pages as $page) {
            return $page;
        }
    }
}

/**
* Test to see if the current page is a homepage.
*
* @param $page 
* @return bool TRUE if homepage,  FALSE if not found.
**/
function folio_page_is_homepage( $page ) {

    if ( $page->page_ident == $page->parentpage_ident ) {
        return true;
    } else {
        return false;
    }
}

/**
* Called to create the standard templated homepage for a user. 
*    Will simply return the existing record if it already exists.
*
* @param string $username The user.
* @return int The id for the new record.
**/
function folio_homepage_initialize( $username ) {
    global $CFG;
    global $FOLIO_CFG;
    $url = url;
    
    // Transate the username to an ID
    $user = recordset_to_array(
            get_recordset_sql('SELECT username, ident FROM ' . $CFG->prefix . 'users ' .
                "WHERE username = '$username' LIMIT 1")
            );
    $user = $user[$username];
            
    if ( !$user ) {
        error("Unable to find $username user in folio_homepage_initialize");
    } else {
        $user_ident = $user->ident;
    }
    
    // Check to make sure that the record hasn't already been created in the past.
    $page = recordset_to_array(
            get_recordset_sql('SELECT * FROM ' . $CFG->prefix . 'folio_page ' .
                "WHERE page_ident = parentpage_ident AND user_ident = $user_ident LIMIT 1")
            );            
    
    if ( $page ) {
        $page = array_pop( $page );
        return $page->page_ident;
    } else {
    
        // Generate ID
        $page_ident = rand(1,999999999);

        // Create security record
        $security = new StdClass;
        $security->security_ident = $page_ident;
        $security->user_ident = $user_ident;
        $security->accesslevel = $FOLIO_CFG->page_defaultpermission;
        // Insert
        insert_record('folio_page_security',$security);

        // Create page record.
        $page = new StdClass;
        $page->title = 'Home Page';
        $page->body = "You can use this part of Elgg as a wiki, to collaboratively work on projects, " . 
                "or to create an online portfolio.\n\nYou can create links to new pages by simply " .
                "enclosing the page title using double bracket characters ([ and ]).  Here's an example " .
                "of a link to this [[Home Page]].  ";
        $page->page_ident = $page_ident;
        $page->security_ident = $page_ident;
        $page->parentpage_ident = $page_ident;
        $page->newest = 1;
        $page->created = time();
        $page->user_ident = $user_ident;
        // Insert new record into db.
        $insert_id = insert_record('folio_page',$page);
    }
    return $page_ident;

}

/**
* Check for a duplicate (or problemtatic) page name
*
* @param int $user_ident The user to whom the page belongs
* @param string $title The title of the page
* @param int $page_ident The id value of the page, -1 if it is a new page
* @param OUTPUT boolean True if there is a dup, false if not.
**/
function folio_duplicate_pagename( $user_ident, $title, $page_ident ) {
    // Clean title just to make sure.
    global $CFG;
    
    $title = folio_clean( $title );

    // Test to see another page exists with the same title.
    $page = recordset_to_array(
        get_recordset_sql('SELECT * FROM ' . $CFG->prefix . 'folio_page ' .
            "WHERE title = '$title' and newest = 1 and user_ident = $user_ident and page_ident <> $page_ident")
        );
    if ( !!$page ) {
        return true;
    } else {
        return false;     
    }
    

}

/**
* Retrieve the username for the passed userid
* @param int $user_ident
**/
function folio_getUsername( $user_ident ) {
    global $name_cache;

    if (!defined('profileinit')) {
        run("profile:init");
    }
   
    if (!isset($username_cache[$user_ident]) 
        || (time() - $username_cache[$user_ident]->created > 60)) {

        $username_cache[$user_ident]->created = time();
        $username_cache[$user_ident]->data = get_field('users','username','ident',$user_ident);
    }

    return $username_cache[$user_ident]->data;
}

/**
* Publish something to an RSS feed
*
* 
* @param int owner_ident (int for the object owner)
* @param string owner_username (url-able string name for the object owner)
* @param int user_ident (int for the person doing the action)
* @param string user_username (url-able string name for the user who performed the action)
* @param string user_name (the pretty name of the user who performed the action)
* @param string Type (url-able string, etc: weblog, file, wikipage)
* @param int Type_Ident (string: can be used for update/delete.  Should be unique
    with respect to the type, aka File2, wiki2939, blog234.  I like this
    way of keeping track of unique items (as opposed to the link or some
    other attribute) because all of the add-ins and native data types have
    an ident value in the database to store their values.  It would
    probably make sense to allow this field to be null, in which case, the
    rss item could not be updated/deleted in the future.).
* @param array Tags (an array of string, eg. array("wiki", "elgg", "notes") ).
* @param string Title (string title for the RSS item).
* @param string Body (full string for the message to be syndicated.)
* @param string Link (the http address to view/comment on/edit/whatever the item).
* @param string access (the permission level for the rss item, def=public).
**/
function rss_additem( $owner_ident, $owner_username, $user_ident, $user_name, $user_username, $type, $type_ident, $tags, $title, $body, $link, $access='PUBLIC', $created = -1 ) {

	if ($created == -1 ) {
		$created = time();
	}
	
    $rss = new StdClass;
    $rss->owner_ident = intval($owner_ident);
    $rss->owner_username = $owner_username;
    $rss->user_ident = intval($user_ident);
    $rss->user_name = $user_name;
    $rss->user_username = $user_username;
    $rss->type = $type;
    $rss->type_ident = $type_ident;
    $rss->title = $title;
    $rss->body = $body;
    $rss->link = $link;
    $rss->created = $created;
    $rss->access = $access;
    
    // Insert new record into db.
    insert_record('folio_rss',$rss);
}


/**
* Deletes all RSS feed entries with the given key and type.
*	Called to prevent duplicate entries for rapidly edited documents.
*	If calling with the user_ident and timespan, need to use both.
* 
* @param string Type (url-able string, etc: weblog, file, wikipage)
* @param int Type_Ident 
* @param int User_Ident  (can be used to limit the deletion of the item to only the current user)
* @param int timespan (how far back to delete)
**/
function rss_deleteitem( $type, $type_ident, $user_ident = -1, $timespan = -1 ) {
    global $db, $CFG;
	
	if ( $user_ident == -1 AND $timespan == -1 ) {
		delete_records('folio_rss', 'type_ident', $type_ident, 'type', $type);
	} else {
		$stmt = $db->Prepare("DELETE FROM {$CFG->prefix}folio_rss where type = '{$type}' AND " .
							 "type_ident = {$type_ident} AND user_ident = {$user_ident} AND created > " .
							 (time() - $timespan ));
		$returnvalue = $db->Execute($stmt);
	}
}


/**
* Retrieve records from an rss feed.
* 
* @param string $where (There where clause for the query)
* @param int $from (Where to start showing results from, def = 0
* @param int $perpage How many results to show after the start, def = 10
* @param array OUTPUT (The returned records in an array of arrays corresponding to the mysql records.)
**/
function rss_getitems( $where, $from=0, $perpage=10 ) {
    global $CFG;

   $results = recordset_to_array(get_recordset_sql( "SELECT CONCAT(type_ident, '.', type, '.', created) masterkey, " .
        $CFG->prefix . "folio_rss.* FROM " . $CFG->prefix . "folio_rss" .
        " WHERE $where" .
        " ORDER BY created DESC " .
        " LIMIT $from, $perpage " ));
    return $results;
}

/**
* Retrieve the number of records available in an RSS feed.
*
* @param string $where ( the where clause for the query)
* @param array OUTPUT an Integer value.
**/
function rss_getcount( $where ) {
    global $CFG;
    // Clean incoming variables.

   $results = recordset_to_array(get_recordset_sql( "SELECT '1' as ident, count(*) as records " .
        " FROM " . $CFG->prefix . "folio_rss" .
        " WHERE $where " ));

    return intval($results[1]->records);

}

/**
* Retrieve all of the allowable permission types for the passed userid
* @param int $userid -1 is a valid option, just sets to public permissions.
**/
function rss_permissionlist( $userid ) {
    global $CFG;
        
    if ( $userid == -1 ) {
        return array('PUBLIC', 'MODERATED');
    }
    
    // Default permissions already availabled to logged-in users.
    $list = array('PUBLIC', 'LOGGED_IN', 'MODERATED');
    
    // Find values for joined communities.
    $communities = recordset_to_array(get_recordset_sql( 
        "SELECT distinct u.ident as i, u.ident FROM " . $CFG->prefix . "friends e inner join " . 
            $CFG->prefix . "users u on e.friend = u.ident where e.owner=$userid and user_type = 'community'"));
    
    if ( $communities) {
        foreach ( $communities as $community) {
            $list[] = 'community' . $community->ident;
        }
    }
    
    // Find values for owned communities
    $communities = recordset_to_array(get_recordset_sql( 
        "SELECT DISTINCT ident as username, ident FROM " . $CFG->prefix . "users where owner = $userid"));
    
    if ( $communities ) {
        foreach ( $communities as $community ) {
            $list[] = 'community' . $community->username;
        }
    }
    
    // Find values for joined groups.
    $groups = recordset_to_array(get_recordset_sql( 
        "SELECT distinct g.ident as i, g.ident FROM " . $CFG->prefix . "group_membership m inner join " . 
            $CFG->prefix . "groups g on m.group_id = g.ident where m.user_id=$userid "));
    
    if ( $groups) {
        foreach ( $groups as $group) {
            $list[] = 'group' . $group->ident;
        }
    }
    
    // Find values for owned groups
    $groups = recordset_to_array(get_recordset_sql( 
        "SELECT DISTINCT ident as i, ident FROM " . $CFG->prefix . "groups where owner = $userid"));
    
    if ( $groups ) {
        foreach ( $groups as $group ) {
            $list[] = 'group' . $group->ident;
        }
    }
    
    // Add user{$userid} for stuff set to private for the specified user.
    $list[] = 'user' . $userid;
    
    
    return $list;
}  

/**
* Post the new weblog post to the RSS table.  Also called for weblog edits.
**/
function folio_rss_weblog_post($object_type, $event, $object) {
    global $db, $CFG;
    
    $title = $object->title;
    $body = $object->body;
        $access = $object->access;

    // Owner is where the blog is located, aka, the community.
    $owner_id = $object->weblog;
    $owner_username = user_info("username", $owner_id);

    // Get user info from db.
    $user_id = $object->owner;
    $user_name = user_info("name", $user_id);
    $user_username = user_info("username", $user_id);
    
    $type =  'weblog';

    // Ugly hack to grab tags.  Only works for new posts, not for edits.  Need to fix.
	// TODO: Fix to work with edits.
    $tags = explode(',', trim(optional_param('new_weblog_keywords')));

    $type_ident = $object->ident;

    $link = $CFG->wwwroot . $owner_username . '/weblog/' . $type_ident . '.html';
	$created = $object->posted;
	
    //generate rss
	rss_deleteitem( $type, $type_ident );
    rss_additem( $owner_id, $owner_username, $user_id, $user_name, $user_username, $type, $type_ident, $tags, $title, $body, $link, $access, $created);

    // Update RSS in comments to match the same permission level.
    $stmt = $db->Prepare("UPDATE {$CFG->prefix}folio_rss SET access = '{$access}' " .
                         "WHERE type_ident IN ( SELECT ident FROM {$CFG->prefix}weblog_comments " .
                         "WHERE post_id = {$type_ident} ) AND type = 'weblog_comment'" );
    $returnvalue = $db->Execute($stmt);
        
    // return results
    return $object;
}


/**
* Deletes the post from the RSS table.
**/
function folio_rss_weblog_delete($object_type, $event, $object) {
    global $db, $CFG;
  
	$stmt = $db->Prepare( "DELETE FROM {$CFG->prefix}folio_rss 
			WHERE type_ident = {$object->ident} AND type='weblog'");
	$returnvalue = $db->Execute($stmt); 

    // return results
    return $object;
}


/**
* Deletes the post from the RSS table.
**/
function folio_rss_weblog_comment_delete($object_type, $event, $object) {
    global $db, $CFG;
	$stmt = $db->Prepare( "DELETE FROM {$CFG->prefix}folio_rss 
			WHERE type_ident = {$object->ident} AND type='weblog_comment'");
	$returnvalue = $db->Execute($stmt); 

    // return results
    return $object;
}

/**
* Post the new weblog comment to the RSS table
**/
function folio_rss_weblog_comment_post($object_type, $event, $object) {
    global $CFG;

    $post = get_record_select('weblog_posts','ident = ' . $object->post_id);

    if (!$post) {
        trigger_error("Invalid weblog ident {$comment->post_id} passed to /mod/folio/lib.php ");
    }

    // Owner is where the blog is located, aka, the community.
    $owner_id = $post->weblog;
    $owner_username = user_info("username", $owner_id);
	$access = $post->access;

    // Get user info from db.
    $user_id = $object->owner;

    if ( $user_id == 0 ) {
        // Guest
        $user_id = -1;
        $user_name = $object->postedname;
        $user_username = '';
    } else {
        // Logged in users
        $user_name = $object->postedname;
        $user_username = user_info("username", $user_id);
    }

    $title = 'Comment on "' . $post->title . '"';
    $link = url . $owner_username. "/weblog/" . $post->ident . ".html";
	$created = $object->posted;
	
    //generate rss
    rss_additem( $owner_id, $owner_username, 
        $user_id, $user_name, $user_username, 
        'weblog_comment', $object->ident, '',
        $title, $object->body, $link, $access, $created );
        
    return $object;
}

/**
* Record a file upload or edit
**/
function folio_rss_file_post($object_type, $event, $object) {

    global $CFG;
    
    // Set type
    $type = 'file';

    // Grab tags from the post variables.
    $tags = explode(',', trim(optional_param('new_file_keywords')));

    // Pull the poster information from session
    if ( isloggedin() ) {
        // Get current logged in user
        $user_ident = $_SESSION['userid'];
        $user_name = $_SESSION['name'];
        $user_username = $_SESSION['username'];
    } else {
        // Build variables for a guest uploader.  Not sure if possible,
        //    but just in case...
        $user_ident = -1;
        $user_name = 'Guest';
        $user_username = '';
    }
    
    // Build variables from $object
    $type_ident = $object->ident;
    $owner_ident = $object->files_owner;
	
	if ( $event == 'update' ) {
		// Owner isn't passed, look it up
	    $file = get_record_select('files','ident = ' . $type_ident );
	    if (!$file) {
	        trigger_error("Invalid file ident {$type_ident} passed to /mod/folio/lib.php ");
	    }
	    // Owner is where the blog is located, aka, the community.
	    $owner_ident = $file->files_owner;
	}
	$owner_username = run('users:id_to_name', $owner_ident);	

    if ( strlen( $object->title ) > 0 ) {
        $title = "File $event: {$object->title}";
    } else {
        $title = "File $event: {$object->originalname}";
    }
    $body = $object->description;
    $link = url . $owner_username . '/files/' . $object->folder . '/' . $type_ident . '/filename' ;
    $access = $object->access;
	$created = $object->time_uploaded;
    
    //generate rss
	rss_deleteitem( $type, $type_ident );
    rss_additem( $owner_ident, $owner_username, $user_ident, $user_name, $user_username, $type, $type_ident, $tags, $title, $body, $link, $access, $created );

    return $object;
}

/**
* Deletes the file from the RSS table.
**/
function folio_rss_file_delete($object_type, $event, $object) {
    global $db, $CFG;
	$stmt = $db->Prepare( "DELETE FROM {$CFG->prefix}folio_rss 
			WHERE type_ident = {$object->ident} AND type='file'");
	$returnvalue = $db->Execute($stmt); 

    // return results
    return $object;
}


/**
* Return the ident for the user reading this page
*   If no validation, then return -1
**/    
function folio_decodehash( $readerhash ) {

    // To view subscribe records, validate hash.  If fail, return false.
    if ( $readerhash == '' ) {
        return -1;
    } else {
        // Calculate MD5 hash
        $i = strpos( $readerhash, ':' );
        
        // Separate variables
        $reader_name = substr( $readerhash, 0, $i );
        $reader_pwd = substr( $readerhash, $i+1, strlen($readerhash) - $i -1);
        // Grab ID
        $rec = get_record('users','username',$reader_name);
        
        if ( !$rec ) {
            // No matching user
            return -1;
        } elseif ( $rec->password == $reader_pwd ) {
            // Matched pwd
            return $rec->ident;
        } else {
            // Pwd doesn't match
            return -1;
        }
        
    }
}

/**
* Return the hash values for a userident
**/    
function folio_createhash( $user_ident ) {
    if ( isloggedin() ) {
        $rec = get_record('users','ident',$user_ident);
        return $rec->username . ':' . $rec->password;
    }
}
    
/**
* Update the page setup
*
* Run this in the top menus.  Can't run this in the lib.php init setup, as
*    not all of the add-ins have been loaded.
**/
function folio_prunetopmenu() {
    global $PAGE;
    global $FOLIO_CFG;

    $menu = $PAGE->menu;
    $PAGE->menu = array();

    // Top Menu
    foreach ( $menu as $m ) {
        if ( !in_array( $m['name'], $FOLIO_CFG->disabled_topmenuitems ) ) {
            $PAGE->menu[] = $m;
        } 
    }
}

/**
* Update the page setup
*
* Run this to remove extra side menus. 
**/
function folio_prunesidemenu() {
    global $FOLIO_CFG;
    global $function;
    global $CFG;
    
    // Side Menu
    // Debug print
    if ($FOLIO_CFG->debug_print_sidemenu_includes) { var_dump( $function['display:sidebar'] ); }

    // Set
    $menu = $function['display:sidebar'];
    $function['display:sidebar'] = array();

    // Top Menu
    foreach ( $menu as $m ) {
        if ( !in_array( $m, $FOLIO_CFG->disabled_sidemenuitems ) ) {
            $function['display:sidebar'][] = $m;
        } 
    }
}


?>