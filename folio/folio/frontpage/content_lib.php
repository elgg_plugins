<?php
global $CFG;
require_once(  $CFG->dirroot . 'mod/folio/control/feeds.php');

/**
* Create the html code for the revised frontpage template
**/
function folio_template_frontpage() {
    global $CFG;
    global $FOLIO_CFG;

    define("context", "main");
 
    $userid = $_SESSION['userid'];
    $username = $_SESSION['username'];
    //$portfolio = $CFG->wwwroot . $_SESSION['username'];
    //$activity = intval( $_SESSION['folio_events_fac']); //FAC = friends & Communities
    $events = intval( $_SESSION['folio_events_all']);

    $run_result =  str_replace( '{wwwroot}', $CFG->wwwroot, 
        str_replace( "{username}", $username, $FOLIO_CFG->template_frontpage_text));

    $run_result .= /* subscribe_view_friends_html( $userid, $username ) . */ 
        "<h1>Recent Activity</h1>" .
        subscribe_view_all_html( $userid, $username ) ;
    
    return $run_result;

}

function subscribe_view_friends_html( $userid, $username ) {
    //   Find the root page information.
    global $CFG;
    $url = url;

    // Display a header
    $run_result = "All blog entries, pages, files, and comments for your communities and friends are shown here. "  .
        "<a href='{$CFG->wwwroot}{$_SESSION['username']}/activity/html/'>View more...</a><br/><br/>" .
        "<table>";

    // Get a list of friends & joined communities.
    $friendlist = subscribe_friendslist( $userid);
    
    // Transform array into a where clause.
    if ( count( $friendlist ) > 0 ) {
       $where = 'owner_ident in (' . implode( ',', $friendlist) . ')';
    } else {
        // If no friends or joined communities, don't show anything.
        $where = ' false ';
    }
    
    // Get a list of allowable permission settings
    $permissions = rss_permissionlist( $userid );
    $where .= ' AND access in ("' . implode( '","', $permissions) . '") AND user_ident <> ' . $userid;
    
    $items = rss_getitems( $where, 0, 5);

    if ( is_array( $items ) ) {
        array_walk($items, 'folio_control_summaryfeed_item', &$run_result);
    }
    
    $run_result .= '</table><br/>';
    
    return trim($run_result);
}

function subscribe_view_all_html( $userid, $username ) {
    //   Find the root page information.
    global $CFG;
    $url = url;

    $run_result = "<table>";

    // Get a list of allowable permission settings
    $permissions = rss_permissionlist( $userid );
    
    // Transform array into a where clause.
    if ( count( $permissions ) > 0 ) {
       $where = 'access in ("' . implode( '","', $permissions) . '") AND user_ident <> ' . $userid;
    }
    
    $items = rss_getitems( $where, 0, 7);

    if ( is_array( $items ) ) {
        array_walk($items, 'folio_control_summaryfeed_item', &$run_result);
    }
    
    $run_result .= "</table><br/><a href='{$CFG->wwwroot}activity/inbox/{$username}/html/all/all/1'>More...</a><br/> <br/> <br/> <br/>";
    
    return trim($run_result);
}


function subscribe_friendslist( $userid ) {
    global $CFG;
    // FIND FRIENDS, OWNED,  AND JOINED COMMUNITIES.
        
    // Find key values for friends & joined communities.
    $friendlist = array();
    $friends = recordset_to_array(get_recordset_sql( 
        "SELECT DISTINCT friend as i, friend FROM " . $CFG->prefix . "friends where owner = $userid"));
    
    if ( $friends ) {
        foreach ( $friends as $friend ) {
            $friendlist[] = $friend->friend;
        }
    }
    
    // Find key values for owned communities
    $friends = recordset_to_array(get_recordset_sql( 
        "SELECT DISTINCT ident as i, ident FROM " . $CFG->prefix . "users where owner = $userid"));
    
    if ( $friends ) {
        foreach ( $friends as $friend ) {
            $friendlist[] = $friend->ident;
        }
    }
    
    return $friendlist;
}    


/* This has been replaced by feeds.php folio_control_summaryfeed_item
function subscribe_view_html_detail( $result, $key, &$run_result ) {
    $url = url;
    $icon = run("icons:get", $result->user_ident);
    $date = gmdate("D M d",intval($result->created));
    $teaser = strip_tags( substr( $result->body, 0, 100 ) );
    if ( strlen($result->body) > 100 ) { $teaser .= "<a href='{$result->link}'>...</a>"; }
    $type = str_replace( 'weblog', 'blog', str_replace( 'page', 'wiki page', str_replace( '_', ' ', $result->type )));
   
    $run_result .= <<< END
        <tr>
            <td width="70" valign="top">
                <a href='{$url}{$result->user_username}'>
                    <img alt="" src="{$url}{$result->user_username}/icons/{$icon}/h/67/w/67" border="0"/>
                </a>
            </td>
            <td valign='top'>
                <b><a href='{$result->link}'>{$result->title}</a></b><br/>
                <a href='{$url}{$result->owner_username}'>{$result->owner_username}</a> | $type | $date<br/>
                $teaser <b><a href='{$result->link}'>more...</a></i>
            </td>
        </tr>
END;

}
*/
?>