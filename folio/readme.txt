----------------------------------------------------------------------------------
                                Elgg Folio Add-in
                                Updated 10/14/2007
----------------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////////
// OVERVIEW
/////////////////////////////////////////////////////////////////////////////////

This add-in provides basic wiki functionality for the Elgg social networking
software, as well as global RSS & activity / notification pages.

Please view the installation section to see version compatability.  Because this add-in 
changes some of the default Elgg files, it has to be loaded for the appropriate
version.

This add-in modifies some of the way that Elgg operates.  It rewrite the side-menu
to include pages.  It includes a unique key searchable on Google with the add-in
version (see users_info_menu.php).

There are a wide variety of options that can be set in the mod/folio/config.php file.  These
include modifying the options available on the side menu, top menu, and a custom frontpage.
Please review this file for more information.

Please note that the Google svn repository contains files that are not used by this add-in.
The only files needed to install this folio add-in are found in mod/folio and
the .htaccess file.

/////////////////////////////////////////////////////////////////////////////////
// TEST INSTALLATIONS
/////////////////////////////////////////////////////////////////////////////////

This add-in has been tested on a number of different systems.  It is developed on a
Windows XP box running Apache2Triad.  It is run on Linux RHE system.  

At this point, it does not work on any database other than MySQL.

At this point, it is not internationalized with gettext.

If you experience any problems, please contact Nathan Garrett at nathan.garrett@cgu.edu

/////////////////////////////////////////////////////////////////////////////////
// INSTALLATION or UPGRADING
/////////////////////////////////////////////////////////////////////////////////

To install this software, do the following:

1. Install the v0.7 or v.08 version of Elgg.  Make sure that Elgg is working
	before going any further. This add-in is not supported
        with any other version.
2. If you are upgrading, remove any previous installations of the wiki add-in by 
	deleting all of the files in  _folio (for pre v.5 folio version) and mod/folio.
	Remove the extra lines in .htaccess marked as belonging to this add-in.
3. Copy the mod/folio folder to	{root folder}mod/folio.
4. Copy the section marked as belonging to this add-in in the htaccess file in this distribution
	to your .htaccess file.  
5. Log into Elgg as news.
6. Go the {root folder}mod/folio/html/setupdb.php.  This file will update the database. You have
	to be logged in as news to be able to run this file.  If you do not complete this
	step, the add-in will not work properly.  Running this file will not remove
	any of your existing data.  It simply updates your database to the most recent version.
	Running this multiple times will not cause any problems.  It should just tell you
	that your database is up to date.
7. There are a number of options in /mod/folio/config.php that can be set.  For example,
        you can set the frontpage text, enable or disable certain top menu options, etc...
	You do not have to move or rename this file for the options to work, just edit it as is.

You should now have a fully-working version of the Elgg Wiki Add-in


/////////////////////////////////////////////////////////////////////////////////
// OTHER MODULES
/////////////////////////////////////////////////////////////////////////////////

Research Advisors:
	Dr. Lorne Olfman (CGU, Social Learning Lab. Dean, IS&T)
	Dr. Terry Ryan (CGU, Social Learning Lab)
Main Programmer: 
	Nathan Garrett (blog at elgg.net/garrettn)
Custom Templating: 
	Brian Thoms 
Financial Support:
	CGU Transdisciplinary Grant
		Sponsor: Dr. Wendy Martin (CGU George and Ronya Kozmetsky Chair)

	
/////////////////////////////////////////////////////////////////////////////////
// Change Log & Schedule
/////////////////////////////////////////////////////////////////////////////////

Anticipated:
	2007.09.?? v0.7/8 Additional options for frontpage, custom community joining 

Occurred:
	2007.10.08 v0.64 Bug fix with file update.
	2007.07.01 v0.63 Fixed small bugs, made a number of minor enhancements.
	2007.06.25 v0.62 Fixed bugs.  Updated DB Scheme
	2007.06.06 v0.61 Fixed bugs.  Updated DB Scheme
	2007.06.06 v0.6 Fixed a number of bugs.  Added options via config.php for 
			different display options.
	2007.02.25 v0.5 Rewrote activity module, greatly enhancing functionality.
			Got rid of _folio directory, moving to /mod/folio/html
			Added tags
	2006.11.11 v0.4 Fixed some problems with the beta.
	2006.10.23 Beta v0.4 Finished getting the subscribe/activity functionality working.
	2006.10.04 v0.31 Added history & delete functionality. Minor bug fixes.
	2006.08.28 v0.3 
	2006.07.26 Add-in installed at claremontconversations.org/tcourse
	2006.06.26 v0.2 (Alpha 2). Compat with Elgg v.6
	2006.06.16 SVN setup at sl2.cgu.edu/svn/elgg.  Public read-only access granted.
	2006.05.?? Awarded CGU T-Course Grant 
	2006.02.26 v0.1 (Alpha 1). Compat with Elgg v.4.
	2006.01.01 Started Work

/////////////////////////////////////////////////////////////////////////////////
// Libraries
/////////////////////////////////////////////////////////////////////////////////    
    
This work was partially built off of a number of pubically available and open-sourced
php libraries.

Yahoo Control Library - Treeview control
Cross-Browser.com - Drop-down window
Yav - Javascript Validation    
YShout - Chatbox (optional, separate download)

/////////////////////////////////////////////////////////////////////////////////
// License
/////////////////////////////////////////////////////////////////////////////////

    This file is part of Elgg Folio Addin.

    Elgg Folio Addin is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    Elgg Folio Addin is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Elgg Folio Addin; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA