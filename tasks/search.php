<?php

    require("../../includes.php");
    global $page_owner;
    
    
    $page_owner = optional_param('tasklist', $_SESSION['userid'], PARAM_INT);
    $search_tag = optional_param('tag', '');
    
    if (empty($search_tag)) {
        header("Location: index.php?owner=".$page_owner);
        exit;
    }
    
    
    define("context","tasks");
    templates_page_setup();
    
    
    $body = "";
    
    $body .= tasks_display($page_owner,$_SESSION['userid'],gettext("Your tasks tagged with $search_tag on this tasklist"),false,$search_tag);
    $body .= tasks_display(0 - $page_owner,$_SESSION['userid'],gettext("Your tasks tagged with $search_tag elsewhere"),false,$search_tag);
    $body .= tasks_display($page_owner,-1,gettext("Other peoples' tasks tagged with $search_tag on this tasklist"),false,$search_tag);
    $body .= tasks_display(0 - $page_owner,-1,gettext("Other peoples' tasks tagged with $search_tag elsewhere"),false,$search_tag);
    
    $body .= "<h2>". gettext("Related resources") . "</h2>";
    $body .= "<p><a href=\"" . $CFG->wwwroot . "search/all.php?tag=$search_tag&=all&=search\">" . gettext("Click here for related resources.") . "</a></p>";
    
    $title = user_info("name",$page_owner) . " :: " . gettext("Tasks tagged with $search_tag");
    
    $body  = templates_draw( array(
                               'context' => 'contentholder',
                               'title' => $title,
                               'body' => $body
                               ));

    echo templates_page_draw(array($title, $body));

?>