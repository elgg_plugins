CREATE TABLE `prefix_tasks` (
  `ident` int(11) NOT NULL auto_increment,
  `owner` int(11) NOT NULL,
  `tasklist` int(11) NOT NULL,
  `title` text  NOT NULL,
  `description` text  NOT NULL,
  `posted` int(11) NOT NULL,
  `deadline` int(11) NOT NULL,
  `access` varchar(128)  NOT NULL,
  PRIMARY KEY  (`ident`),
  KEY `user_id` (`owner`),
  KEY `posted` (`posted`),
  KEY `deadline` (`deadline`),
  KEY `tasklist` (`tasklist`),
  KEY `access` (`access`)
) ENGINE=MyISAM;
