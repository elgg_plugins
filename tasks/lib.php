<?php

    function tasks_pagesetup() {
        // register links -- 
        global $tasks_id;
        global $PAGE;
        global $CFG;
    
        if (isloggedin()) {
            if (defined("context") && context == "tasks" && page_owner() == $_SESSION['userid']) {
                $PAGE->menu[] = array( 'name' => 'tasks', 
                                       'html' => '<li><a href="'.$CFG->wwwroot.'mod/tasks/" class="selected">'.gettext("Your Tasks").'</a></li>');
            } else {
                $PAGE->menu[] = array( 'name' => 'tasks',
                                       'html' => '<li><a href="'.$CFG->wwwroot.'mod/tasks/">'.gettext("Your Tasks").'</a></li>');
            }
        }
    }
    
    function tasks_init() {
        
        global $function;
        global $CFG;
        global $db;
        
        $function['init'][] = $CFG->dirroot . "mod/tasks/init.php";
        $function['display:sidebar'][] = $CFG->dirroot . "mod/tasks/sidebarlink.php";
        
        $tables = $db->Metatables();
        if (!in_array($CFG->prefix . "tasks",$tables)) {
            modify_database($CFG->dirroot . "mod/tasks/table.sql");
            print_continue("index.php");
            exit;
        }
        
    }
    
    function tasks_permissions_check ($object) {
        global page_owner();
        
        if ($object === "tasks" && (page_owner() == $_SESSION['userid'] || user_flag_get("admin", $_SESSION['userid']))) {
            return true;
        }
        return false;
    }
    
    function tasks_display ($tasklist_id, $owner_id, $title, $assigned_to = false, $keyword = null) {
    
        global $CFG;
        $return = "";
        
        $where = run("users:access_level_sql_where",$_SESSION['userid']);
        if ($tasklist_id > 0) {
            $tasklist_where = " and tasklist = $tasklist_id ";
        } else if ($tasklist_id < 0) {
            $tasklist_where = " and tasklist != " . (0 - $tasklist_id);
        } else {
            $tasklist_where = "";
        }
        
        if ($keyword == null) {
            if ($owner_id != -1) {
                $sql = "select * from ".$CFG->prefix."tasks where ($where) and owner = ".$owner_id." $tasklist_where order by deadline asc";
            } else {
                $sql = "select * from ".$CFG->prefix."tasks where ($where) and owner != ".$_SESSION['userid']." $tasklist_where order by deadline asc";
            }
        } else {
            $where = str_replace("owner","ts.owner",$where);
            $where = str_replace("access","ts.access",$where);
            $keyword = mysql_real_escape_string($keyword);
            if ($owner_id != -1) {
                $sql = "select ts.* from ".$CFG->prefix."tags tg left join ".$CFG->prefix."tasks ts on ts.ident = tg.ref where ($where) and tg.tag = '$keyword' and ts.owner = ".$owner_id." $tasklist_where order by deadline asc";
            } else {
                $sql = "select ts.* from ".$CFG->prefix."tags tg left join ".$CFG->prefix."tasks ts on ts.ident = tg.ref where ($where) and tg.tag = '$keyword' and ts.owner != ".$_SESSION['userid']." $tasklist_where order by deadline asc";
            }
        }
        
        $tasks = get_records_sql($sql);

        if ($tasklist_id < 0) {
            $tasklist_id = 0 - $tasklist_id;
        }
        
        if (is_array($tasks) && !empty($tasks)) {
            
            $return = "<h2>" . $title . "</h2>";
            foreach($tasks as $task) {
                if (run("users:access_level_check", $task->access)) {

                    $return .= "<table width=\"100%\" border=\"0\" class=\"task\">";
                                        
                    $cells = 1;
                    
                    if ($task->deadline < time() && $task->deadline > 0) {
                        $class = "task_late";
                    } else if ($task->deadline < (time() + 86400) && $task->deadline > 0) {
                        $class = "task_expiring";
                    } else {
                        $class = "task_normal";
                    }
                    $return .= "<tr class=\"$class\"><td class=\"task_title\">";

                    if ($task->tasklist != page_owner()) {
                        $return .= "<a href=\"".$CFG->wwwroot."mod/tasks/?owner=".$task->tasklist."\">";
                    }
                    
                    $return .= $task->title;
                    
                    if ($task->tasklist != page_owner()) {
                        $return .= " @ " . user_info("name",$task->tasklist) . "</a>";
                    }
                    
                    if ($task->deadline > 0) {
                        $cells++;
                        $return .= "</td><td class=\"task_deadline\">" . gettext("Due: ") . strftime("%B %d, %Y %H:%M",$task->deadline);
                    }
                    $return .= "</td>";
                    if ($task->owner == $_SESSION['userid'] || run("permissions:check", "profile")) {
                        $cells++;
                        $return .= "<td class=\"task_buttons\">Edit | <a href=\"".$CFG->wwwroot."mod/tasks/?owner=".$task->tasklist."&action=task:delete&task_id=".$task->ident."\">Delete</a></td>";
                    }
                    if ($assigned_to) {
                        $return .= "<tr class=\"$class\"><td colspan=\"$cells\" class=\"task_assigned\">";
                        $return .= gettext("Assigned to: ");
                        
                        if (run("permissions:check", "weblog") && logged_on) {
                            
                            $sql = "select u.ident, u.name from ".$CFG->prefix."friends f join ".$CFG->prefix."users u on u.ident = f.owner where f.friend = ".$task->tasklist;
                            $users = get_records_sql($sql);

                            $return .= "<form action=\"\" method=\"post\">";
                            
                            if (is_array($users) && !empty($users)) {
                                
                                $return .= "<select name=\"task_assigned\">";
                                $haduser = false;
                                foreach($users as $user) {
                                    if ($user->ident == $task->owner) {
                                        $selected = "selected = \"selected\"";
                                        $haduser = true;
                                    } else {
                                        $selected = "";
                                    }
                                    $return .= "<option value=\"".$user->ident."\" $selected>" . $user->name . "</option>";
                                }
                                if (!$haduser) {
                                    $user = get_record_sql("select * from ".$CFG->prefix."users where ident = ".$task->owner);
                                    $return .= "<option value=\"".$user->ident."\" selected=\"selected\">" . $user->name . "</option>";
                                }
                                $return .= "</select> ";
                                $return .= "<input type=\"hidden\" name=\"action\" value=\"task:reassign\" />";
                                $return .= "<input type=\"hidden\" name=\"task_id\" value=\"".$task->ident."\" />";
                                $return .= "<input type=\"submit\" value=\"&gt;\" />";
                                
                            }
                            
                            $return .= "</form>";

                        } else {
                            $return .= user_info("name",$task->owner);
                        }
                        
                        $return .= "</td></tr>";
                    }
                    $return .= "<tr><td colspan=\"$cells\" class=\"task_description\">" . nl2br($task->description) . "</td></tr>";
                    
                    $tags = "";
                    $db_tags = get_records_sql("select * from ".$CFG->prefix."tags where tagtype = 'task' and ref = ".$task->ident);
                    
                    if (is_array($db_tags) && !empty($db_tags)) {
                        $i=0;
                        foreach($db_tags as $tag) {
                            $tags .= "<a href=\"" . $CFG->wwwroot . "mod/tasks/search.php?tag=".urlencode($tag->tag)."&amp;tasklist=$tasklist_id\">";
                            $tags .= $tag->tag;
                            $tags .= "</a>";
                            if ($i < sizeof($db_tags) - 1) {
                                $tags .= ", ";
                            }
                            $i++;
                        }
                    }
                    
                    $return .= "<tr><td colspan=\"$cells\" class=\"task_tags\">" . gettext("Tags:") . " " . $tags . "</td>";
                    $return .= "</tr>\n";
                    
                    $return .= "</table>";
                    
                }
            }
            
        }
        
        return $return;
    
    }

    function tasks_form($tasklist_id) {
        
        $return = "";
        
        if (run("permissions:check", "weblog") && logged_on) {
            
            if (user_type($tasklist_id) == "person") {
                $access = "user" . $tasklist_id;
            } else if (user_type($tasklist_id) == "community") {
                $access = "community" . $tasklist_id;
            } else {
                $access = "PRIVATE";
            }
            
            $return .= "<h2>" . gettext("Add new task") . "</h2>";
            $return .= "<form action=\"\" method=\"post\">";
            $return .= "<p>" . gettext("Task title") . "<br /><input type=\"text\" name=\"task_title\" value=\"\" /></p>";
            $return .= "<p>" . gettext("Task description") . "<br /><textarea name=\"task_description\" ></textarea></p>";
            $return .= "<p>" . gettext("Task tags") . "<br /><input type=\"text\" name=\"task_tags\" value=\"\" /></p>";
            $return .= "<p>" . gettext("Task expiry time if any<br />(eg, 'next thursday 15:00', 'december 1st 2007', '+1 week', etc)") . "<br /><input type=\"text\" name=\"task_deadline\" value=\"\"></textarea></p>";
            $return .= "<p>" . gettext("Task access restriction") . "<br />". run("display:access_level_select",array("task_access",$access)) ."</p>";
            $return .= "<p><input type=\"hidden\" name=\"action\" value=\"task:add\" /><input type=\"hidden\" name=\"tasklist_id\" value=\"$tasklist_id\" /><input type=\"submit\" value=\"" . gettext("Add new task") . "\" /></p>";
            $return .= "</form>";
            
        }
        
        return $return;
        
    }
    
    function tasks_actions() {
        
        global $CFG;
        global $messages;
        
        $redirect = false;
        
        $action = optional_param('action', "");
        
        if (logged_on && run("permissions:check", "weblog") && !empty($action)) {
            
            switch($action) {
                
                case "task:add":
                    $task = new stdClass;
                    
                    $task->title = optional_param('task_title',"");
                    $task->description = optional_param('task_description',"");
                    $task->access = optional_param('task_access',"");
                    $task->tasklist = optional_param('tasklist_id',page_owner(),PARAM_INT);
                    $task->owner = $_SESSION['userid'];
                    $task->posted = time();
        
                    $deadline = optional_param('task_deadline',"");
                    if (empty($deadline)) {
                        $task->deadline = 0;
                    } else {
                        $task->deadline = strtotime($deadline);
                    }
                                
                    $insert_id = insert_record('tasks',$task);
                    
                    $tags = optional_param('task_tags',"");
                    insert_tags_from_string (trim($tags), 'task', $insert_id, $task->access, $task->owner);
                    
                    $messages[] = gettext("Task '". $task->title ."' was added to the tasklist.");
                    $redirect = true;
                    break;
                    
                case "task:delete":
                
                    $task_id = optional_param('task_id',-1,PARAM_INT);
                    if ($task_id != -1) {
                        $task = get_record_sql("select * from ".$CFG->prefix."tasks where ident = $task_id");
                        if ($task->owner == $_SESSION['userid'] || run("permissions:check", "weblog")) {
                            
                            delete_records('tasks','ident',$task_id);
                            delete_records('tags','tagtype','task','ref',$task_id);
                            $messages[] = gettext("Task '".$task->title."' was deleted.");
                            
                        }
                    }
                
                    $redirect = true;
                    break;
               case "task:reassign":

                    $task_id = optional_param('task_id',-1,PARAM_INT);
                    $task_owner = optional_param('task_assigned',-1,PARAM_INT);
                    if ($task_id != -1 && $task_owner != -1) {
                        $task = get_record_sql("select * from ".$CFG->prefix."tasks where ident = $task_id");
                        if ($task->owner == $_SESSION['userid'] || run("permissions:check", "weblog")) {
                            
                            $task->owner = $task_owner;
                            update_record('tasks',$task);
                            $messages[] = gettext("Task '".$task->title."' was reassigned.");
                            
                        }
                    }
                    $redirect = true;
                    break;
                    
            }
            
            if ($redirect) {
                define('redirect_url',$CFG->wwwroot . "mod/tasks/?owner=" . $task->tasklist);
                $_SESSION['messages'] = $messages;
                header("Location: " . redirect_url);
                exit;
            }
            
        }

    }

?>