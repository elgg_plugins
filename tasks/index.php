<?php

    require("../../includes.php");
    
    global $page_owner;

    $page_owner = optional_param('owner', $_SESSION['userid'], PARAM_INT);

    tasks_actions();
            
    define("context","tasks");
    templates_page_setup();
    
    $body = "";
    
    if (user_type($page_owner) == "community") {
        $assignedto = true;
    } else {
        $assignedto = false;
    }
    
    $yourtasks = tasks_display($page_owner,$_SESSION['userid'],gettext("Your tasks"),$assignedto);
    
    if (!empty($yourtasks)) {
        $body .= "<div class=\"tasks_yours\">".$yourtasks."</div>";
    }
    
    $body .= tasks_display($page_owner,-1,gettext("All tasks"),$assignedto);
    
    if ($page_owner == $_SESSION['userid']) {
        $body .= tasks_display(0 - $_SESSION['userid'],$_SESSION['userid'],gettext("Your tasks elsewhere"),$assignedto);
    }
    
    $body .= tasks_form($page_owner);
    
    $title = user_info("name",$page_owner) . " :: " . gettext("Tasks");
    
    $body  = templates_draw( array(
                               'context' => 'contentholder',
                               'title' => $title,
                               'body' => $body
                               ));

    echo templates_page_draw(array($title, $body));

?>