Forum Plugin for Elgg
GPL v3, yada yada yada...

***REQUIRES ELGG v0.8***

To install:
-copy the forum folder to your <elgg directory>/mod/ folder
-login as "news" (executes a minor database upgrade)
-edit mod/forum/config.php to change config (optional)
-visit a community blog that has a few posts and comments to those posts
-on the community blog, find the link that says "View as Forum"
 
Add the following lines to the end of your .htaccess file (just above the </ifmodule>):

#FORUM PLUGIN REWRITE ELEMENTSRewriteRule ^([A-Za-z0-9]+)\/forum\/?$ mod/forum/forum.php?weblog=$1RewriteRule ^([A-Za-z0-9]+)\/forum\/skip=([0-9]+)$ mod/forum/forum.php?weblog=$1&weblog_offset=$2RewriteRule ^[A-Za-z0-9]+\/forum\/([0-9]+)(\.html)?$ mod/forum/forum_view_thread.php?post=$1



To do:
-test for potential conflicts with other plugins (reported problems with "Folio" and v0.2 of FORUM)



Big thanks to Ben and Dave for some guidance on this, as well as "going for it" by installing on elgg.net/elgg.org

History:

v0.3 (July 2007)
-major cleanup of code/sloppiness
-fixed issues with Elgg v0.8
-lots of contribution to this revision by Rho (http://elgg.org/rho)
-updated GPL to v3 (take that, Microsoft! ;-)

v0.2 (March 10, 2007)
-added sorting based on last updated threads
-added better handling of default behaviour (forum vs. community blog)

v0.1 (Feb 8, 2007)
-finally usable!
-now has a more master/detail view
-moved to tables for layout rather than pure CSS
-uses post_icon to display image for each thread
-uses list_for_event API to return to approapriate view after adding, editing, deleting a post or comment

v0.000000000001 (Jan, 2007)
-actually getting a bit useful!
-added linking/posting new comments
-fixed bug where sidebar showed current user rather than the community profile
-now displays posters name, tooltip of comment content