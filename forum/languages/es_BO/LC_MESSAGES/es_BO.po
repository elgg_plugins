# Español translation of PACKAGE.
# Copyright (C) 2007 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Rolando Espinoza La Fuente <darkrho@gmail.com>, 2007.
# , fuzzy
# 
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-07-04 16:56-0400\n"
"PO-Revision-Date: 2007-07-04 17:35-0400\n"
"Last-Translator: Rolando Espinoza La Fuente <darkrho@gmail.com>\n"
"Language-Team: Español <es@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit"

#: forum_user_info_menu.php:10
msgid "Recent Activity"
msgstr ""

#: forum_user_info_menu.php:12
msgid "View your activity"
msgstr ""

#: forum_user_info_menu.php:29
msgid "Forum & Blogs"
msgstr "Foros & Blogs"

#: forum_user_info_menu.php:36
msgid "Personal blog"
msgstr ""

#: forum_user_info_menu.php:41
msgid "Community Forum"
msgstr "Foro de la comunidad"

#: forum_user_info_menu.php:49
msgid "Archive"
msgstr ""

#: forum_user_info_menu.php:50
msgid "Members blog"
msgstr "Blogs de miembros"

#: forum_view_thread.php:34
msgid "Access denied or post not found"
msgstr ""

#: forum_view_thread.php:37
msgid ""
"Either this blog post doesn't exist or you don't currently have access "
"privileges to view it."
msgstr ""

#: forum_view_thread.php:53
msgid "Return to Forum"
msgstr "Regresar al foro"

#: forum_view_thread.php:57
msgid "Add a comment..."
msgstr "Añadir comentario..."

#: forum_view_thread.php:57
msgid "Add new comment"
msgstr "Añadir nuevo comentario"

#: forum_view_thread.php:67
msgid "From"
msgstr "De"

#: forum_view_thread.php:75 forum_view_thread.php:108 forum.php:125
msgid "View profile..."
msgstr "Ver perfil..."

#: forum_view_thread.php:80
msgid "Edit"
msgstr "Editar"

#: forum_view_thread.php:81
msgid "Are you sure you want to permanently delete this forum post?"
msgstr "Estas seguro de querer borrar esta entrada del foro?"

#: forum_view_thread.php:82 forum_view_thread.php:113
msgid "Delete"
msgstr "Borrar"

#: forum_view_thread.php:112
msgid "Are you sure you want to permanently delete this forum comment?"
msgstr "Estas seguro de borrar este comentario del foro?"

#: forum_view_thread.php:128
msgid "Add new comment..."
msgstr "Añadir nuevo comentario..."

#: forum_view_thread.php:140 forum.php:164 forum.php:171
msgid "Forum"
msgstr "Foro"

#: lib.php:15
msgid "View as Forum"
msgstr "Ver como foro"

#: userdetails_actions.php:15
msgid "Your forum view preferences have been saved"
msgstr "Se ha guardado tu configuració de vista como foro"

#: userdetails_actions.php:18
msgid "Your forum view preferences have been changed"
msgstr "Se ha cambiado tu configuración de vista como foro"

#: userdetails_actions.php:20
msgid "Your forum view preferences could not be changed"
msgstr "No se pudo cambiar tu configuración de vista como foro"

#: userdetails_edit.php:11
msgid "Blog/Forum View:"
msgstr "Blog/Foro vista:"

#: userdetails_edit.php:12
msgid ""
"Set this to 'yes' if you would like to default this user/community blog to a "
"'forum' view."
msgstr "Selecciona 'Si' si quieres que el blog se vea como foro"

#: userdetails_edit.php:33 userdetails_edit.php:41
msgid "Default Blog View to 'Forum' type: "
msgstr "Ver blog como foro por defecto:"

#: userdetails_edit.php:34 userdetails_edit.php:42
msgid "Yes"
msgstr "Si"

#: userdetails_edit.php:34 userdetails_edit.php:42
msgid "No"
msgstr "No"

#: forum.php:40 forum.php:136
msgid "Add New Item"
msgstr "Añadir entrada"

#: forum.php:45
msgid "View as Blog"
msgstr "Ver como blog"

#: forum.php:89
msgid "Discussion Topic"
msgstr "Tema de discusión"

#: forum.php:90
msgid "Started by"
msgstr "Empezado por"

#: forum.php:91
msgid "Comments"
msgstr "Comentarios"

#: forum.php:124
msgid "View full discussion..."
msgstr "Ver discusión completa..."

#: forum.php:126
msgid "View comments..."
msgstr "Ver comentarios..."

#: forum.php:142
msgid "Back"
msgstr ""

#: forum.php:154
msgid "Next"
msgstr ""

#: forum.php:169
msgid "This forum doesn't exists."
msgstr "El foro no existe"

