<?php

    /*
     *  Community invite plugin
     *  Allows people to invite their mutual friends to communities
     */

     // Set up menu items etc
        function communityinvite_pagesetup() {
            
            // register links --
            global $profile_id;
            global $PAGE;
            global $CFG;
            global $USER;
            global $metatags;

            $page_owner = $profile_id;
            $usertype = user_type($page_owner);
            $username= user_info('username', $page_owner);
        
            if ($usertype == "community") {
        
                if (defined("context") && context == "profile") {
        
                    if (run("permissions:check", "profile")) {
        
                        // Add submenu link
                        $PAGE->menu_sub[] = array( 'name' => 'community:edit',
                                                   'html' => a_href("{$CFG->wwwroot}mod/communityinvite/index.php?profile_id=$page_owner" ,
                                                                     __gettext("Invite friends to community")));
                    }
        
                }
                
            }
        }

     // Get IDs of community members as a string
        function communityinvite_notin($userid) {
            global $CFG;
            $notin = "-1";
            if ($friends = get_records_sql("select ident from {$CFG->friends} where friend = {$userid}")) {
                foreach($friends as $friend) {
                    $notin .= "," . $friend->ident;
                }
            }
            return $notin;
        }
        
     // Get usernames of mutual friends
        function communityinvite_mutualfriends($userid = -2, $notin = "-1") {
            global $CFG, $messages;
            if ($userid == -2) {
                $userid = $_SESSION['userid'];
            }
            if ($notin == "-1") {
                $notin = communityinvite_notin($userid);
            }
            if ($userid > 0) {
                $mutualfriends = get_records_sql("select * from {$CFG->prefix}users where ident not in ({$notin}) and ident in (select owner as ident from {$CFG->prefix}friends where friend = {$userid} and owner in (select friend as owner from {$CFG->prefix}friends where owner = {$userid}))");
                return $mutualfriends;
                
            }
            return false;
        }

     // Check actions
        function communityinvite_actions() {
            global $CFG, $messages, $page_owner;
            
            $action = optional_param("action");
            if (isloggedin() && run("permissions:check", "profile") && !empty($action)) {
                
                switch($action) {
                
                    case "community:friends:invite":
                                                        $people = optional_param("people");
                                                        if (!empty($people)) {
                                                            communityinvite_doinvite($page_owner, $people);
                                                        } else {
                                                            $messages[] = __gettext("Sorry - we couldn't find anyone to invite.");
                                                        }
                                                        $_SESSION['messages'] = $messages;
                                                        header("Location: {$CFG->wwwroot}mod/communityinvite/index.php?profile_id={$page_owner}");
                                                        exit;
                                                        break;
                    
                }
                
            }

        }
        
     // Inviter function
        function communityinvite_doinvite($community, $people) {
            global $CFG,$messages;
            if (isloggedin()) {
                
                if ($mutualfriends = communityinvite_mutualfriends($_SESSION['userid'])) {
                    
                    $emailarray = array();
                    foreach($mutualfriends as $mutual) {
                        $emailarray[trim(strtolower($mutual->username))] = $mutual->ident;
                    }
                    
                    $communityrow = get_record_sql("select * from {$CFG->prefix}users where ident = {$community}");
                    $peoplearray = explode(",",$people);
                    
                    $title = sprintf(__gettext("%s has invited you to join a community"),$_SESSION['name']);
                    $message = sprintf(__gettext("%s has invited you to join the following community: %s. To do so, visit the following profile:\n\n\t%s\n\nYou can join the community from this screen."),$_SESSION['name'],$communityrow->name,$CFG->wwwroot . $communityrow->username);
                    
                    if (!empty($peoplearray)) {
                        foreach($peoplearray as $person) {
                            $person = trim(strtolower($person));
                            if (!empty($emailarray[$person])) {
                                notify_user($emailarray[$person], $title, $message);
                            }
                        }
                        $messages[] = sprintf(__gettext("Your friends were invited to join the %s community."),$communityrow->name);
                    }
                    
                }                
                
            }
        }

?>