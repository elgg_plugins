<?php

    //    ELGG manage community memberships page

    // Run includes
        require_once(dirname(dirname(__FILE__))."/../includes.php");

    // Initialise functions for user details, icon management and profile management
        run("userdetails:init");
        run("profile:init");
        run("friends:init");
        run("communities:init");

        $context = "profile";

        define("context", $context);
        templates_page_setup();

        global $CFG, $page_owner;

        $title = run("profile:display:name") . " :: " . __gettext("Invite friends");

    // Hmm, we don't have permission to invite people to this community!        
        if (!run("permissions:check", "profile")) {
            header("Location: {$CFG->wwwroot}");
            exit;
        }
        
    // From here on in, we know we have permission to add people.
    
    // Let's do some actions, shall we?
        communityinvite_actions();
    
    // Initialise body
        $body = "";
    
    // Get mutual friends.
        $mutualfriends = communityinvite_mutualfriends($_SESSION['userid']);
        if (!empty($mutualfriends)) {
            
            $body .= "<form action=\"\" method=\"post\">";
            $body .= "<p>" . __gettext("To add mutual friends to this community, select their names from the pulldown list below to add their usernames to the list, and press the 'Invite friends' button:") . "</p>";
            $body .= <<< END
            
                        <script>
                            function addFriend(pulldown,boxid)
                            {
                                var team;
                                var boxobj;
                                var pulldownobj;
                            	boxobj = document.getElementById(boxid);
                            	pulldownobj = document.getElementById(pulldown);
                            	if (boxobj.value != "") {
                                	boxobj.value = boxobj.value + ", ";
                            	}
                            	boxobj.value = boxobj.value + pulldownobj.options[pulldownobj.selectedIndex].value;
                            }
                        </script>
END;
            $body .= "<select id=\"mutualfriends\" onChange=\"addFriend('mutualfriends','people')\" ><option value=\"\"></option>";
            foreach($mutualfriends as $mutualfriend) {
                
                $body .= "<option value=\"{$mutualfriend->username}\">{$mutualfriend->name}</option>";
                
            }
            $body .= "</select>";
            $body .= "<textarea name=\"people\" id=\"people\" style=\"width: 95%; height: 100px\"></textarea>";
            $body .= "<input type=\"submit\" value=\"" . __gettext("Invite friends") . "\" />";
            $body .= "<input type=\"hidden\" name=\"action\" value=\"community:friends:invite\" />";
            $body .= "</form>";
            
        }
        
        $body  = templates_draw( array(
                               'context' => 'contentholder',
                               'title' => $title,
                               'body' => $body
                               ));
        
        echo templates_page_draw( array(
                    $title, $body
                )
                );

?>