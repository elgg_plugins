<?php

    function visaviz_pagesetup()
    {
        global $CFG, $PAGE, $page_owner;
    
        if (context == "network"  && run("users:type:get", $page_owner) == "person")
        {
            // Add to the submenu
            $num = count($PAGE->menu_sub) + 1;

            $PAGE->menu_sub[$num]['name'] = "friend:explore"; 
            $PAGE->menu_sub[$num]['html'] = '<a href="'.$CFG->wwwroot . 'mod/visaviz/visaviz_applet.php?owner='.$page_owner.'">Explore</a>';
        }

    }
?>
