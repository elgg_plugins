<?php
	
	// Run includes
		require_once("../../includes.php");

        global $CFG, $page_owner;

        // Uses xmlrpc library for now
        $user = run('users:instance', array('user_id' => $page_owner));

        $name = $user->getName();
        $username = $user->getUserName();
        $root = substr($CFG->wwwroot,0,-1);
        $url = $root . "/_rpc/RPC2.php";

        define("context", "network");
        
        templates_page_setup();
        
    // Applet
        $body = <<< END
	        <p style="width:80%;padding:8px;border:1px solid #e3e3e3;font-size:.8em;margin:20px;background-color:#f5f5f5;">
	        &raquo; Double-click to expand a node<br/>
	        &raquo; Left click to move the display area<br/>
            &raquo; Right click to zoom</a><br/>
	        <br/>
	        The visualization applet is experimental and may take some time to load.
	        </p>

            <object
                classid = "clsid:8AD9C840-044E-11D1-B3E9-00805F499D93"
                codebase = "http://java.sun.com/update/1.5.0/jinstall-1_5-windows-i586.cab#Version=5,0,0,5"
                WIDTH = "500" HEIGHT = "500" />
                <PARAM NAME = CODE VALUE = "org/elgg/services/visual/ElggFriendsApplet.class" />
                <PARAM NAME = ARCHIVE VALUE = "$root/mod/visaviz/lib/prefuse.jar,$root/mod/visaviz/lib/prefusex.jar,$root/mod/visaviz/lib/xmlrpc-2.0-beta.jar,$root/mod/visaviz/lib/commons-codec.jar,$root/mod/visaviz/lib/elgg.jar" />
                <param name = "type" value = "application/x-java-applet;version=1.5">
                <param name = "scriptable" value = "false"/>
                <PARAM NAME = "textField" VALUE="label"/>
                <PARAM NAME = "user_id" VALUE="$username"/>
                <PARAM NAME = "name" VALUE="$name"/>
                <PARAM NAME = "url" VALUE="$url"/>
                <PARAM NAME = "display_icon" VALUE="true"/>

            <comment>
        	<embed
                    type = "application/x-java-applet;version=1.5" \
                    CODE = "org/elgg/services/visual/ElggFriendsApplet.class" \
                    ARCHIVE = "$root/mod/visaviz/lib/prefuse.jar,$root/mod/visaviz/lib/prefusex.jar,$root/mod/visaviz/lib/xmlrpc-2.0-beta.jar,$root/mod/visaviz/lib/commons-codec.jar,$root/mod/visaviz/lib/elgg.jar" \
                    WIDTH = "500" \
                    HEIGHT = "500" \
                    textField ="label"/ \
                    user_id ="$username"/ \
                    name ="$name"/ \
                    url ="$url"/ \
                    display_icon ="true"/
	                scriptable = false
	                pluginspage = "http://java.sun.com/products/plugin/index.html#download">
	               <noembed>
                        If you can read this text, the applet is not working. Perhaps you don\'t	have the Java 1.4 (or later) web plug-in installed?
                   </noembed>
	       </embed>
           </comment>
           </object>
END;

    // Draw page
        $title = "Explore ".$name;

        $body = templates_draw(array(
                        'context' => 'contentholder',
                        'title' => $title,
                        'body' => $body
                    )
                    );

        echo templates_page_draw( array(
                        $title, $body
                    )
                    );

		
?>
