<?php

	global $run_result;
	
	$run_result .= "<div class=\"infoholder\"><div class=\"fieldname\"><h3 style=\"cursor:hand; cursor:pointer\" onclick=\"trackback_showhide('trackback_post')\">". __gettext("Trackbacks") . " &gt;&gt;</h3></div><div id=\"trackback_post\" style=\"display: none\">" . __gettext("Elgg will attempt to mine trackback urls from your post. Specify any additional urls in a comma separated list below.") . "<br /><textarea style=\"width: 95%; height: 100px\" name=\"trackbackurls\"></textarea><br /><label><input name=\"sendtrackbacks\" type=\"checkbox\"/> ".__gettext("Look for trackbacks in post")."</label></div></div>";

?>