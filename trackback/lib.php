<?php
	/*
    	 Trackback plugin for Elgg
    	 Initial author: Marcus Povey <marcus@dushka.co.uk>
	*/

	/** Initialise the trackback code & hook into framework */
	function trackback_init() {

		global $function, $CFG, $metatags;

		// Register javascript
		$metatags .= "<script type=\"text/javascript\" src=\"{$CFG->wwwroot}mod/trackback/trackback.js\"><!-- trackback js --></script>";
		
		$function['weblog:post:links'][] = $CFG->dirroot . "mod/trackback/lib/weblog_post_links.php"; // Add trackback link.
		$function['display:content:toolbar'][] = $CFG->dirroot . "mod/trackback/lib/weblog_post_trackback.php"; // Add trackback options to post
		
		listen_for_event("weblog_post","publish","trackback_parsefortrackbacks"); 
	}

	function trackback_pagesetup() {
	}

	
	/**
	 * Truncate some text to a given number of words.
	 * This function truncates some text to a given number of words.
	 */
	function trackback_TruncateToWords($text, $maxwords)
	{
	 	$output = "";
		$cnt = 0;
		
	 	for ($n = 0; $n < strlen($text); $n++)
		{
			if ($text[$n]==" ")
				$cnt++;
				
			if ($cnt>$maxwords)
				return $output;
			
			$output .= $text[$n];
		}
		
		return $output;
	 }

	/**
	 * This function sends some data as a post request to a server url.
	 */
	function trackback_PostSend($server, $query, $contenttype = "application/x-www-form-urlencoded") {

		if (trim($server) == "")
			return FALSE;

	        // Construct data
	        $data_o = $query;
		$serverdetails = parse_url($server);

	        $host = $serverdetails['host'];
		$port = 80;

	        if ($serverdetails['scheme'] == "https") {
	                $host = "ssl://" . $host;
			$port = 443;
	        }

		if (($serverdetails['port']!="") && ($serverdetails['port']!=$port)) $port = $serverdetails['port'];

		// Open socket
	        $socket = @fsockopen($host, $port,$errno, $errstr);

	        if ($socket==FALSE)
			return FALSE;

		// Construct URI to query
		if (isset($serverdetails['query'])) {
			$uri = $serverdetails['path'] . "?" . $serverdetails['query'];
		} else {
			$uri = $serverdetails['path'];
		}

	        // Construct query
	        $header_o = "POST " . $uri. " HTTP/1.0\r\n";
		$header_o .= "Host: ".$serverdetails['host']."\r\n";
	        $header_o .= "Content-Length: " . strlen($data_o) . "\r\n";
	        $header_o .= "Content-type: $contenttype\r\n";
	        $header_o .= "User-Agent: Elgg trackback pinger\r\n";
		$header_o .= "Connection: close\r\n";

	        // Construct buffer
	        $buff = $header_o . "\n" . $data_o;

	        // Write query
	        fputs($socket, $buff, strlen($buff));
		
		// Read response
	        $blob = "";
	        while (!feof($socket)) {
	                $blob .= fgets($socket);
	        }
	        
		// Close socket
	        fclose($socket);

	        // Parse out session variables
	        $blobs = explode("\r\n\r\n", $blob);
	        $headers = $blobs[0];
	        $body = $blobs[1];

	        $r_data = $body;

	        // Sanity check data
	        if (strlen(trim($r_data))==0) 
	                return FALSE;
	      
	        return $r_data;
	}


	function trackback_Ok()
	{
?>
<response>
<error>0</error>
</response>
<?php
	}
	 
	function trackback_Error($message)
	{
?>
<response>
<error>1</error>
<message><?php echo $message; ?></message>
</response>
<?php
		die();
	}

	/** 
	 * Send a trackback ping to a url.
	 * @returns false on error
	 */
	function trackback_sendtrackbackping($post, $url)
	{
		global $CFG;

		$url = trim($url);

		// Pull user details
		$username = urlencode(user_info("username", $post->weblog));
		$blog_name = urlencode(user_info("name",$post->weblog));

		$words = trackback_TruncateToWords(
						htmlentities(
							strip_tags(
								stripslashes($post->body)
							)
						)
						,70
				);
		
		$title = urlencode(strip_tags(stripslashes($post->title)));
		$excerpt = urlencode(
			trim(
				$words . "...",
				"\r\n\t \0."
			)
		);
		$src = urlencode($CFG->wwwroot . $username . "/weblog/" . $post->ident . ".html");
	
		$query = "title=$title&url=$src&excerpt=$excerpt&blog_name=$blog_name";

		$result = trackback_PostSend($url, $query);
		if (($result != FALSE) && (strpos($result, "<error>0")!==FALSE) )
			return true;
		
		return false;
	}
	
	/** 
	 * Mine the post for links and see if we can find trackbacks.
 	 */
	function trackback_parsefortrackbacks($object_type, $event, $object) 
	{
		global $messages;
		
		if ($event=="publish")
		{
			// Check options for trackbacks
			$SendTrackbacks = trim(optional_param('sendtrackbacks',''));
			$giventrackbacks = trim(optional_param('trackbackurls',''), " \r\n,");
			
			$TrackbackUrls = array();

			// Have we been provided a trackback urls?
			if ($giventrackbacks!="")
				$TrackbackUrls = explode(",", $giventrackbacks);
	
			// Do we need to search for trackbacks
			if ($SendTrackbacks!="")
			{
				// Find trackback pings
	
				// Find urls in post
				$rtn = preg_match_all("*href=\"([^\"]+)\"*", $object->body, $urlarray);
				//$rtn = preg_match_all("/href=\"(.*)\"/imsU", $object->body, $urlarray);  

				if ($rtn>0) {

					foreach ($urlarray[1] as $url)
					{
						// Fetch permalink
						$url = trim($url); 

						// attempt to fetch url
						$page = file_get_contents($url);
	
						// find trackback containing the permalink
						preg_match_all("/<rdf\:RDF(.*)<\/rdf\:RDF>/imsU", $page, $matches);

						foreach ($matches[1] as $rdf)
						{
							if (strpos($rdf, $url)!==FALSE)
							{
								// Found the correct trackback link
								$a = strpos($rdf, "trackback:ping=");
								$a+=16;
								
								$b = strpos($rdf, "\"", $a);

								if (($a!==FALSE) && ($b!==FALSE))
								{
									$TrackbackUrls[] = substr($rdf, $a, $b - $a);
								}

							}
						}

					}
				}
			}

			// Ok, now send trackbacks to each url we have been given or harvested.
			foreach (array_unique($TrackbackUrls) as $url)
			{
				if (!trackback_sendtrackbackping($object, $url))
					$messages[] = "Could not send trackback to $url";
				else
					$messages[] = "Trackback sent to $url";	
			}
			
		}

		return $object;
	}



?>