<?php
	/*
    	 Trackback plugin for Elgg
    	 Initial author: Marcus Povey <marcus@dushka.co.uk>
	*/

	/** @file trackback/index.php Trackback endpoint.
	 * This file receives trackback requests for entries and stores them in a database, based on the 
	 * code written for dushka.co.uk.
	 * @author Marcus Povey <marcus@dushka.co.uk>
	 */

 	// Run includes
        require_once("../../includes.php");
	global $messages;

	// Identity of the entry being pinged
	$id = optional_param('id','', PARAM_INT);
	
	// Trackback data
 	$url = optional_param('url','');
 	$title = optional_param('title','');
	$excerpt = optional_param('excerpt','');
	$blog_name = optional_param('blog_name','');

	// Output the header before anything else
	header("Content-Type: application/xml; charset=charset=iso-8859-15");

	// Sanity check input
	if ($url=="")
	{
		trackback_Error("URL has not been given!");
	}

	if ($id=="")
	{
		trackback_Error("ID of article not specified, trackback URL is incomplete");
	}

	// Construct and post the comment
	$comment = new stdClass;
	$comment->post_id = $id;
	$comment->postedname = $blog_name;
	//$comment->profile_url = $url; // This might be a nice future enhancement
	$comment->body = "<a href=\"$url\">$title</a><br />$excerpt";
	$comment->owner = -1;
	$comment->posted = time();

	// Post the comment
	$comment = plugin_hook("weblog_comment","create",$comment);
	if ( ($comment!==false) && (!empty($comment)) )
	{
		$insert_id = insert_record('weblog_comments',$comment);
		$comment->ident = $insert_id;
		$comment = plugin_hook("weblog_comment","publish",$comment);
	}

	// Any errors?
	if ($comment===false) 
	{
		if (count($messages)>0) 
			$message = $messages[count($messages)-1];
		else
			$message = "Could not post message.";

		$errormessage = $message; 
		trackback_Error($errormessage);
	}

	trackback_Ok();

?>