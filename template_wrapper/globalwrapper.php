<?php
/*
 * globalwrapper.php
 *
 * Created on Apr 19, 2007
 *
 * @author Diego Andrés Ramírez Aragón <diego@somosmas.org>
 * @copyright Corporación Somos más - 2007
 */
require_once(dirname(dirname(__FILE__))."/../includes.php");
global $default_template_wrapper;
global $template_wrapper;
global $template;

$page = optional_param("page");

if(!empty($default_template_wrapper)){
  $template['pageshell'] = file_get_contents($default_template_wrapper);
}

if(array_key_exists($page,$template_wrapper)){
  if(array_key_exists('template',$template_wrapper[$page])){
    $template['pageshell'] = file_get_contents($template_wrapper[$page]['template']);
  }
  require_once $template_wrapper[$page]['file'];
}
?>
