<?php
/*
 * lib.php
 *
 * Created on Apr 23, 2007
 *
 * @author Diego Andrés Ramírez Aragón <diego@somosmas.org>
 * @copyright Corporación Somos más - 2007
 */
 global $template_wrapper;
 global $default_template_wrapper;

 $default_template_wrapper = null;
 $template_wrapper = array();

 function template_wrapper_pagesetup(){
 }
 
 function template_wrapper_init(){
  // Setup the default template_wrapper. It is usefull if you will share a wrapped template
  // for many pages
    //set_default_wrapper($CFG->dirroot."mod/template/templates/Default_Template/searchtemplate");
  // Add a template wrapper using the default wrapper  
    //add_template_wrapper("register",$CFG->dirroot."mod/invite/register.php");
  // Add a template wrapper using a specific template for it
    //add_template_wrapper("join",$CFG->dirroot."mod/invite/join.php",$CFG->dirroot."mod/template/templates/Default_Template/logintemplate");
 }

 /**
  * Allows to set the default wrapper.
  * @param string $path The complete path to the default wrapper
  * @return string The old default_template_wrapper
  */
 function set_default_wrapper($path){
   global $default_template_wrapper;
   $old_template = "";
   if(!empty($path) && file_exists($path)){
     $old_template = $default_template_wrapper;
     $default_template_wrapper = $path;
   }
   return $old_template;
 }


 /**
  * Allows to extensions add a new template wrapper
  *
  * @param string $page Page to be wrapped
  * @param string $file The complete path to the file to be wrapped (included)
  * @param string $wrapper (optional) The complet path to the template to be used.
  *                        If not specified the default_template_wrapper would be used.
  * @return mixed Two positions array where: the first positions returns the operation status (true|false)
  *               The second one the error message if one.
  */
 function add_template_wrapper($page,$file,$wrapper=null){
    global $template_wrapper;
    if(empty($page)  || empty($file)){
      return array(false,__gettext("No page or file was provided!"));
    }

    if(!file_exists($file)){
      return array(false,__gettext("The include file doesn't exists!"));
    }

    if(!array_key_exists($page,$template_wrapper)){
      $template_wrapper[$page] = array();
      $template_wrapper[$page]['file'] = $file;
      $status = true;
      $msg = "";
      if(!empty($template_wrapper)){
        if(file_exists($wrapper)){
          $template_wrapper[$page]['template'] = $wrapper;
        }
        else{
          $msg = __gettext("The wrapper file doesn't exists!");
        }
      }
      return array($status,$msg);
    }
    return array (false,__gettext("The $page template wrapper already exists: "). $template_wrapper[$page]['file']);
 }
?>
