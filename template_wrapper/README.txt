-----------------------------------------------------------------------
| TEMPLATE_WRAPPER MODULE: Configuration options and Extension points |
-----------------------------------------------------------------------

The Elgg behavior its mainly controlled by the 'pageshell' script from the
current theme. It the page layout for 'all' the pages from the site.

But in some cases you need that some pages have an special layout, something
like the elgg.org home page.

Here is where this module would be useful for you. It basically allows to you
specify another pageshell for an specific page.

How to use it?
==============

To use the 'template_wrapper' you must to follow the following steps:

- If you will be 'wrap' many pages you should configure an template wrapper for it.
  To do that you must to specify the default template wrapper through
  the 'set_default_wrapper' function in the 'template_wrapper_init' at 'template_wrapper/lib.php'.
  For example:
    set_default_wrapper($CFG->dirroot."mod/template/templates/Default_Template/searchtemplate");

- Add a template_wrapper for each page that you want to have an special layout.
  Add it in the 'template_wrapper_init' at 'template_wrapper/lib.php' 
  If you configure a default template wrapper an you want to use it you could ommit the third argument.
  For example:
    add_template_wrapper("register",$CFG->dirroot."mod/invite/register.php");
    add_template_wrapper("join",$CFG->dirroot."mod/invite/join.php",$CFG->dirroot."mod/template/templates/Default_Template/logintemplate");

- Finally you must to configure in your .htaccess file the redirection.
  Note: the 'page' parameter ist the SAME name that you configure through 
        the 'add_template_wrapper' function.
  For example:
    ReWriteRule ^register$ mod/template_wrapper/globalwrapper.php?page=register
  	ReWriteRule ^invite\/join\/([A-Za-z0-9]+)$ mod/template_wrapper/globalwrapper.php?page=join&invitecode=$1

Notes: 
  - Personaly, I suggest to add your wrapper templates with your Elgg theme.
  - Note that in some cases to make it work you need to change some elgg code for generate
    the links or form actions in the redirect form that you are configuring.
