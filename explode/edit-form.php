<?php 

/*
* Form to include if the user wants to edit an existing service
* @author Curverider <dave@curverider.co.uk>
*/
  
//API function to get the service id from the user selection
$get_service = services_findservicesbyname($service_name);


//iterate through the results
foreach ($get_service->result as $res) {
	$serviceID = $res->ident; // get the named service's ID
}

//query to get the current service record ident
$get_record = get_record_sql("SELECT * FROM {$CFG->prefix}explode WHERE owner=$page_owner and service=$serviceID and service_name='$service_name'");

//check the record exists and set the record ident to pass back
if($get_record) {
	$ident = $get_record->ident;
}

$body .= '<div class=\"presentation-contentslist\">';
			$body .= "<div id=\"edit-service-$service_name\"><a href=javascript:toggleVisibility('edit_a_service_$service_name')>Edit</a></div>";

			$body .= <<<END
				<div id="edit_a_service_$service_name"><br />
				<form action="" method="POST">
					<p><select name="explode_serviceName">
END;
						if($service_name == 'flickr') {
							$body .= "<option value=\"flickr\" SELECTED>Flickr</option>";
						} else {
							$body .= "<option value=\"flickr\">Flickr</option>";
						}

						if($service_name == 'rucku') {
							$body .= "<option value=\"rucku\" SELECTED>Rucku</option>";
						} else {
							$body .= "<option value=\"rucku\">Rucku</option>";
						}
						
						if($service_name == 'youtube') {
							$body .= "<option value=\"youtube\" SELECTED>Youtube</option>";
						} else {
							$body .= "<option value=\"youtube\">Youtube</option>";
						}

						if($service_name == 'tribe') {
							$body .= "<option value=\"tribe\" SELECTED>Tribe</option>";
						} else {
							$body .= "<option value=\"tribe\">Tribe</option>";
						}

						if($service_name == '43things') {
							$body .= "<option value=\"43things\" SELECTED>43Things</option>";
						} else {
							$body .= "<option value=\"43things\">43Things</option>";
						}

						if($service_name == 'livejournal') {
							$body .= "<option value=\"livejournal\" SELECTED>Livejournal</option>";
						} else {
							$body .= "<option value=\"livejournal\">Livejournal</option>";
						}
						
						if($service_name == 'vox') {
							$body .= "<option value=\"vox\" SELECTED>Vox</option>";
						} else {
							$body .= "<option value=\"vox\">Vox</option>";
						}

						if($service_name == 'twitter') {
							$body .= "<option value=\"twitter\" SELECTED>Twitter</option>";
						} else {
							$body .= "<option value=\"twitter\">Twitter</option>";
						}

						if($service_name == 'jaiku') {
							$body .= "<option value=\"jaiku\" SELECTED>Jaiku</option>";
						} else {
							$body .= "<option value=\"jaiku\">Jaiku</option>";
						}

						if($service_name == 'explode') {
							$body .= "<option value=\"explode\" SELECTED>Explode</option>";
						} else {
							$body .= "<option value=\"explode\">Explode</option>";
						}

					$body .= <<<END
						</select></p>
					<p>Username:<br /><input type="textbox" name="explode_service_username" value="$username" /></p>
					<p><select name="explode_numberToDisplay">
END;
						if($user_count == '2') {
							$body .= "<option value='2' SELECTED>2</option>";
						} else {
							$body .= "<option value='2'>2</option>";
						}

						if($user_count == '4') {
							$body .= "<option value='4' SELECTED>4</option>";
						} else {
							$body .= "<option value='4'>4</option>";
						}

						if($user_count == '6') {
							$body .= "<option value='6' SELECTED>6</option>";
						} else {
							$body .= "<option value='6'>6</option>";
						}

						if($user_count == '8') {
							$body .= "<option value='8' SELECTED>8</option>";
						} else {
							$body .= "<option value='8'>8</option>";
						}

						if($user_count == '10') {
							$body .= "<option value='10' SELECTED>10</option>";
						} else {
							$body .= "<option value='10'>10</option>";
						}

						if($user_count == '12') {
							$body .= "<option value='12' SELECTED>12</option>";
						} else {
							$body .= "<option value='12'>12</option>";
						}
						
						if($user_count == '24') {
							$body .= "<option value='24' SELECTED>24</option>";
						} else {
							$body .= "<option value='24'>24</option>";
						}

					$body .= <<<END
						</select></p>
					<p><input type="hidden" name="explode:edit" value="true">
					<!-- hidden field to pass back the record ident for edit -->
					<input type="hidden" name="explode_record_ident" value="$ident">
					<input type="submit" value="submit" name="submit"></p>
				</form>
				</div>
				<script type="text/javascript">
					<!--
						toggleVisibility('edit_a_service_$service_name');
					 -->
				</script>
END;
			$body .= "</div>";
?>