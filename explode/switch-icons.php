<?php

/*
* Code to select which icon to display
* @author Curverider <dave@curverider.co.uk>
*/

// get the service icon to display
switch ($service_id) {
	case 8:
		$icon = 'icon_flickr.gif';
		break;
	case 7:
		$icon = 'icon_tribe.gif';
		break;
	case 9:
		$icon = 'icon_youtube.gif';
		break;
	case 3:
		$icon = 'icon_livejournal.gif';
		break;
	case 2:
		$icon = 'icon_explode.gif';
		break;
	case 4:
		$icon = 'icon_vox.gif';
		break;
	case 10:
		$icon = 'icon_43things.gif';
		break;
	case 11:
		$icon = 'icon_twitter.gif';
		break;
	case 12:
		$icon = 'icon_jaiku.gif';
		break;
	case 1858:
		$icon = 'icon_rucku.gif';
		break;
	default:
		$icon = '';
		break;
}

?>