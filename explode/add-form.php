<?php 

/*
* Form to include if the user wants to add a new service
* @author Curverider <dave@curverider.co.uk>
*/


$body .= '<div class=\"presentation-contentslist\">';
			$body .= '<div id="add-service"><a href=javascript:toggleVisibility(\'add_a_service\')>Add a service:</a></div>';

			$body .= <<<END
				<div id="add_a_service"><br />
				<form action="" method="POST">
					<!-- <p>Add a service:</p> -->
					<p><select name="explode_serviceName">
							<option value="flickr">Flickr</option>
							<option value="youtube">Youtube</option>
							<option value="livejournal">Livejournal</option>
							<option value="vox">Vox</option>
							<option value="twitter">Twitter</option>
							<option value="jaiku">Jaiku</option>
							<option value="explode">Explode</option>
							<option value="43things">43things</option>
							<option value="rucku">Rucku</option>
							<option value="tribe">Tribe</option>
						</select></p>
					<p>Your username:<br /><input type="textbox" name="explode_service_username" value="" /></p>
					<p><select name="explode_numberToDisplay">
							<option value="2">2</option>
							<option value="4">4</option>
							<option value="6">6</option>
							<option value="8">8</option>
							<option value="10">10</option>
							<option value="12">12</option>
							<option value="24">24</option>
						</select></p>
					<p><input type="hidden" name="explode:add" value="true">
					<input type="submit" value="submit" name="submit"></p>
				</form>
				</div>
				<script type="text/javascript">
					<!--
						toggleVisibility('add_a_service');
					// -->
				</script>
END;

?>