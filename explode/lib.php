<?php
	/**
	 * @file lib.php Explode plugin
	 * This plugin is a test Explode sidebar widget 
	 * @author Curverider <dave@curverider.co.uk>
	 */

	 include('explode_api_client.php');

	 /**
	 * Explode page set up page setup
	 */
	function explode_pagesetup() {
		
	}
	
	/**
	 * Explode module initialisation.
	 */
	function explode_init() {
		global $CFG, $db,$function, $metatags, $template;

		// Template keywords
		$CFG->templates->variables_substitute['explodesidebar'][]= "explode_sidebar";

		//define some template items
		$template['css'] .= file_get_contents($CFG->dirroot . "mod/explode/css");

		// Add meta tags 
		$metatags .= "<script type=\"text/javascript\" src=\"{$CFG->wwwroot}mod/explode/js/edit.js\"><!-- status js --></script>";

		// Set up the database
		$tables = $db->Metatables();
		if (!in_array($CFG->prefix . "explode", $tables))
		{
			if (file_exists($CFG->dirroot . "mod/explode/$CFG->dbtype.sql"))
			{
				modify_database($CFG->dirroot . "mod/explode/$CFG->dbtype.sql");
			}
			else
			{
				error("Error: Your database ($CFG->dbtype) is not yet fully supported by the Elgg explode functionality. See the mod/explode directory.");
			}
	
			print_continue($CFG->wwwroot);
			exit;
		}

		// USER ACTIONS
		
		// if a user edits the number of friends they want - capture the result
		if(optional_param("explode:add")) {
			explode_sidebar_add(optional_param("explode_serviceName"), optional_param("explode_service_username"), optional_param("explode_numberToDisplay"));
		}

		// if a user want to delete a service
		if(optional_param("explode:delete")) {
			explode_sidebar_delete(optional_param("explode:service",0,PARAM_INT), optional_param("explode:owner"), optional_param("explode:service_username"));
		}

		// if a user want to edit a service
		if(optional_param("explode:edit")) {
			explode_sidebar_edit(optional_param("explode_serviceName"), optional_param("explode_service_username"), optional_param("explode_numberToDisplay"), optional_param("explode_record_ident",0,PARAM_INT));
		}

		
	}



	/**
	 * This function handle the display and add service functionality
	 * it produces a template keyword that sits in the sidebar portion
	 * of the template/
	 */
	
	function explode_sidebar() {
		global $CFG, $page_owner;
		$user = $page_owner;
		$service = '';
		$username = '';

		$body = "<li id=\"sidebar_external_friends\">";

		// this adds the JS to collapse the friends sidebar widget if desired. 
		// would be nice to remember the setting but for now it just persist for the duration
		// the user is on the page

		$body .= "<a href=\"javascript:changeVisibility('friendsExternal', '/mod/explode/icons/', '')\" >";
     
	    // Displays the open/close icon beside the widget header

		$body .= "<h2><img id=\"imgfriendsExternal\" src=\"/mod/explode/icons/minus.gif\" border=\"0\" name=\"close\"> " . __gettext("Friends elsewhere") . "</h2></a>";

		//this is the div that is required by the colapse JS

		$body .= "<div id=\"elementfriendsExternal\" style=\"display:block\">";


		// get services
		$get_results = get_records_sql("SELECT * FROM {$CFG->prefix}explode WHERE owner=$user");
		
		if($get_results) {
			
			//loop through each result returned for a particular user's services
			foreach ($get_results as $returned) {
					
					//create varibles
					$service_id = $returned->service; // the name of the external service selected
					$username = $returned->service_username; //username on that service
					$user_count = $returned->number_to_display; // the number of friends to display
					$service_name = $returned->service_name;
		
					//use the Explode API function to get the users ident in explode
					$results = users_viewbyusername($username, $service_id);
					
					//loop through returned 'viewbyusername' result
					foreach($results as $res) {

						// check that the ident is not empty
						if($res->ident != '') {
					
							// API function to get all friends of a user
							$friends = users_getfriends($res->ident);
							
							//var_export($friends);
				
							//counters
							$i = 0;
							
							// code which selects the correct icon to display
							include('switch-icons.php');
								
							//display the relevant icon for a service
							$body .= "<div class=\"service\"><img src=\"/mod/explode/images/". $icon ."\"/></div>";

							$body .= "<div class=\"external-friends\">";
                            				
							//iterate through the array and pull out relevant data
							foreach($friends->result as $friend_res) {
    							
    							if ($i == $user_count) {
									break; // break out according to the users choice for display amount.
								}

								$z = 0; // counter to end the display div after two cycles
					
								//check there are some details 
								if($friend_res->user->ident != '') {
    								
    								//itterate through the second array called user_property
									foreach($friend_res->user_property as $url){
    									$friend_url = $url->val;    									
									}

									$body .= "<a href=\"$friend_url\"><img src=" . $friend_res->user->iconurl . " width=\"48\" height=\"48\"/></a>";
									
									// this if statement just close the div and creates a new one 
									// after two icons are displayed
									if($z == 4) {
										$body .= "</div><div class=\"external-friends\">";
									}

									$z++; // counter to break the table cells - ATM it displays two to a row
									
								}
								
								// increase counter
								$i++;

							}//end foreach friends

							//if logged in and the owner
							if($page_owner == $_SESSION['userid']) {

								//include the edit form
								include('edit-form.php');

								//form to delete a service
								$body .=<<<END
								<form action="" method="POST">
									<input type="hidden" name="explode:service" value="$service_id" />
									<input type="hidden" name="explode:owner" value="$page_owner" />
									<input type="hidden" name="explode:service_username" value="$username" />
									<input type="hidden" name="explode:delete" value="true" />
									<input type="submit" name="submit" value="delete" id="delete-button"/>
								</form>
END;
							}

							$body .= "</div>"; //end external friends div

						}//end of if statement
					}//end foreach results
			}//end foreach get results
		}// end if get results

		if($page_owner == $_SESSION['userid']) {
			include('add-form.php');
        }

		$body .= "</div>"; // end the collapsable div

		$body .= "<div id=\"explode-logo\"><a href=\"http://ex.plode.us\"><img src=\"{$CFG->prefix}mod/explode/images/logo_blue.gif\"/></a></div>";
		$body .= "</li>";

		return $body;
	} //function end



	/**
	 * This function handles a users edits to the services they
	 * wish to display
	 */
	
	function explode_sidebar_add($serviceName, $serviceUsername, $numberToDisplay) {
		
		global $CFG, $page_owner;


		// check all the correct variable are present
		if (!empty($serviceName) && !empty($serviceUsername) && !empty($numberToDisplay)) {

			//API function to get the service id from the user selection
			$get_service = services_findservicesbyname($serviceName);


			//itterate through the results
			foreach ($get_service->result as $res) {
				$serviceID = $res->ident; // get the named service's ID
			}

			// update the db
			// check the page owner is in fact the one logged in
			if($page_owner == $_SESSION['userid']) {

				
				//query used to check if this user has already got the service running
				$check_results = get_records_sql("SELECT * FROM {$CFG->prefix}explode WHERE owner=$page_owner and service=$serviceID and service_username='$serviceUsername'");

				//check the record doesn't already exists
				if(!$check_results) {

					$row_to_insert = new stdClass;
					$row_to_insert->owner = $page_owner;
					$row_to_insert->service = $serviceID;
					$row_to_insert->service_username = $serviceUsername;
					$row_to_insert->service_name = $serviceName;
					$row_to_insert->number_to_display = $numberToDisplay;

					$insertRecords = insert_record('explode',$row_to_insert);

				} else {

					$insertRecords ='';
				
				}
			}

			// if the record updates the DB great, if not send back an error message
			if($insertRecords) {
				
			} else {
				return 'Error, we were unable to save your select. Try again or contact the administrator.';
			}

		} else {
			return 'An error occurred. Please try again or contact your site admin.';
		}

	}//end function

	/**
	 * Function that deletes a service
	 */
	
	function explode_sidebar_delete($service, $owner, $service_username) {

		global $CFG, $page_owner;
	 
		// check the page owner is in fact the one logged in
		if($owner == $_SESSION['userid']) {

			//query used to check the record exists and get its ident for deletion
			$get_ident = get_records_sql("SELECT * FROM {$CFG->prefix}explode WHERE owner=$owner and service=$service and service_username='$service_username'");

			//check the record exists
			if($get_ident) {
				
				foreach($get_ident as $results) {
					$ident_delete = $results->ident;
				}
				
				//delete the appropriate record
				$deleteRecords = delete_records('explode','ident', $ident_delete);

			}
		}
	
	}//end function

	function explode_sidebar_edit($service_name, $service_username, $number_to_display, $record_ident) {

		global $CFG, $page_owner;
	 
		// check the page owner is in fact the one logged in
		if($page_owner == $_SESSION['userid']) {

			//API function to get the service id from the service_name
			$get_service = services_findservicesbyname($service_name);

			//iterate through the results
			foreach ($get_service->result as $res) {
				$serviceID = $res->ident; // get the named service's ID
			}

			//check the record exists
			if($get_row_to_edit = get_record_sql("SELECT * FROM {$CFG->prefix}explode WHERE ident=$record_ident")){
				
				$get_row_to_edit->service = $serviceID;
				$get_row_to_edit->service_username = $service_username;
				$get_row_to_edit->service_name = $service_name;
				$get_row_to_edit->number_to_display = $number_to_display;

				$updateRecords = update_record('explode',$get_row_to_edit);

			}
		}
	
	}//end function

?>