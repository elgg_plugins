CREATE TABLE IF NOT EXISTS `prefix_explode` (
  `ident` int(11) NOT NULL auto_increment,
  `owner` int(11) NOT NULL COMMENT '-> pageowner',
  `service` int(11) NOT NULL COMMENT '-> This is the external service to pull friends from',
  `service_username` text NOT NULL COMMENT '-> The Elgg user username on that service',
  `service_name` text NOT NULL COMMENT '-> The actual name of the service',
  `number_to_display` int(11) NOT NULL COMMENT '-> The number of results to display',
   PRIMARY KEY  (`ident`)
);