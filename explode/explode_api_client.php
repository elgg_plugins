<?php
	/** 
	 * @file explodelib.php Explode API interface library.
	 * 
	 * This is a library / reference implementation for interfacing with the explode API.
	 * It should be noted that the Explode API is still under development and is subject to 
	 * change. 
	 * 
	 * I will try to keep this sample API up to date, but the website documentation should
	 * be taken as the canonical source.
	 * 
	 * It is compatible with both PHP4 & 5.
	 * 
	 * @author Marcus Povey <marcus@dushka.co.uk>
	 * @version 0.2
	 */
	

	/** API endpoint.
	 * You should not ever have to change this, but this could be useful to developers.  
	 */
	$xmlurl = "http://ex.plode.us/mod/explodeapi/";

	/**
	 * Encode a raw rest call.
	 * @param $method array An array of 'key' => 'value' of your api parameters.
	 * @return object Query result as a php object.
	 */
	function explode_rawcall($method)
	{
		// Get the endpoint
		global $xmlurl; 

		// Declare an array
		$encoded_params = array();

		// Hard code the format - we're using PHP, so lets use PHP serialisation.
		$method['format'] = "php";
		
		// URL encode all the parameters
		foreach ($method as $k => $v){
			$encoded_params[] = urlencode($k).'='.urlencode($v);
		}
		
		// Put together the query string
		$url = $xmlurl."?".implode('&', $encoded_params);

		// Send the query and get the result and decode.
		$rsp = file_get_contents($url);
		return unserialize($rsp);
	}

	/**
	 * Get a list of users.
	 * 
	 * @param $limit int The number of users to return, default 25.
	 * @param $offset int Offset in the search.
	 */
	function users_index($limit = 25, $offset = 0)
	{
		$param = array();
		
		$param['limit'] = $limit;
		$param['offset'] = $offset;
		$param['method'] = "users.index";

		return explode_rawcall($param);
	}
	
	/**
	 * Get some information about a user.
	 *
	 * @param $userid int The user id to display
	 */
	function users_view($userid)
	{
		$param = array();
		
		$param['userid'] = $userid;
		$param['method'] = "users.view";

		return explode_rawcall($param);
	}
	
	/**
	 * Get some information about a user from a username and service.
	 *
	 * @param string $username The user to look for.
	 * @param int $service The service id.
	 */
	function users_viewbyusername($username, $service)
	{
		$param = array();
		
		$param['username'] = $username;
		$param['service'] = $service;
		$param['method'] = "users.viewbyusername";

		return explode_rawcall($param);
	}

	/**
	 * Get the user's friends.
	 *
	 * @param $userid int The user id to display
	 */
	function users_getfriends($userid)
	{
		$param = array();
		
		$param['userid'] = $userid;
		$param['method'] = "users.getfriends";

		return explode_rawcall($param);
	}

	/**
	 * Get the users who have made this user their friend.
	 *
	 * @param $userid int The user id to display
	 */
	function users_getfriendsof($userid)
	{
		$param = array();
		
		$param['userid'] = $userid;
		$param['method'] = "users.getfriendsof";

		return explode_rawcall($param);
	}

	/**
	 * Get a list of users by interest.
	 *
	 * @param $tag string The tag. Multiple tags can be specified by using "and" eg. "music and chocolate".
	 * @param $limit int The number of users to return, default 25.
	 * @param $offset int Offset in the search.
	 */
	function users_findusersbytag($tag, $limit = 25, $offset = 0)
	{
		$param = array();
		
		$param['method'] = "users.findusersbytag";
		$param['tag'] = $tag;
		$param['limit'] = $limit;
		$param['offset'] = $offset;

		return explode_rawcall($param);
	}

	/**
	 * Search for users by (user)name or name, essentially executing an explode name search.
	 *
	 * @param $username string Username to look for. 
	 * @param $limit int The number of users to return, default 25.
	 * @param $offset int Offset in the search.
	 * @param $servicelist string Optional service list.
	 */
	function users_findusersbyname($username, $limit = 25, $offset = 0, $servicelist = "")
	{
		$param = array();
		
		$param['username'] = $username;
		$param['limit'] = $limit;
		$param['offset'] = $offset;
		$param['servicelist'] = $servicelist;
		$param['method'] = "users.findusersbyname";

		return explode_rawcall($param);
	}

	/**
	 * Get a list of services.
	 * 
	 * @param $limit int The number of users to return, default 25.
	 * @param $offset int Offset in the search.
	 */
	function services_index($limit = 25, $offset = 0)
	{
		$param = array();
		
		$param['limit'] = $limit;
		$param['offset'] = $offset;
		$param['method'] = "services.index";

		return explode_rawcall($param);
	}

	/** 
	 * Get some information about a service.
	 *
	 * @param $serviceid int The id of the service
	 */
	function services_view($serviceid)
	{
		$param = array();
		
		$param['serviceid'] = $serviceid;
		$param['method'] = "services.view";

		return explode_rawcall($param);
	}

	/**
	 * Search for services by service by either the full or short name.
	 * 
	 * @param $name string The name of the service
	 */
	function services_findservicesbyname($name)
	{
		$param = array();
		
		$param['name'] = $name;		
		$param['method'] = "services.findservicesbyname";

		return explode_rawcall($param);
	}

	/**
	 * Get a list of comments (public comment wall).
	 *
	 * @param $limit int The number of users to return, default 25.
	 * @param $offset int Offset in the search.
	 */
	function comments_index($limit = 25, $offset = 0)
	{
		$param = array();
		
		$param['limit'] = $limit;
		$param['offset'] = $offset;
		$param['method'] = "comments.index";

		return explode_rawcall($param);
	}

	/**
	 * Get information about a comment.
	 *
	 * @param $commentid int The comment id
	 */
	function comments_view($commentid)
	{
		$param = array();
		
		$param['method'] = "comments.view";
		$param['commentid'] = $commentid;

		return explode_rawcall($param);
	}

	/**
	 * Get the user's comments (comment wall).
	 *
	 * @param $userid int The user id of the user who's comment wall will be retrieved.
	 * @param $limit int The number of users to return, default 25.
	 * @param $offset int Offset in the search.
	 */
	function comments_user($userid, $limit = 25, $offset = 0)
	{
		$param = array();
		
		$param['userid'] = $userid;
		$param['limit'] = $limit;
		$param['offset'] = $offset;
		$param['method'] = "comments.user";

		return explode_rawcall($param);
	}

?>